package chado.loader.service;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import chado.loader.AppContext;
import chado.loader.model.VariantVariantset;
import chado.loader.model.Variantset;

public class VariantVariantSetService {

	private EntityManager entityManager;

	public VariantVariantSetService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Variantset> findByvariantVariantSetId(Integer id) {
		return entityManager.createNamedQuery("VariantVariantset.findByvariantVariantSetId")
				.setParameter("variantVariantsetId", id).getResultList();
	}

	public List<VariantVariantset> findByvariantVariantSetId() {
		return entityManager.createNamedQuery("VariantVariantset.findAll").getResultList();
	}

	public void insertRecord(VariantVariantset variantVariantSet) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(variantVariantSet);

		entityManager.getTransaction().commit();
	}

	public BigInteger getVariantVariantSetCurrentSeqNumber() {
		BigInteger max = null;
		BigInteger seq = null;

		List max_result = entityManager.createNativeQuery("select max(variant_variantset_id) from variant_variantset")
				.getResultList();
		List seq_result = entityManager
				.createNativeQuery("SELECT last_value from variant_variantset_variant_variantset_id_seq")
				.getResultList();

		if (max_result.get(0) instanceof Integer)
			max = BigInteger.valueOf((Integer) max_result.get(0));
		else
			max = (BigInteger) max_result.get(0);

		if (seq_result.get(0) instanceof Integer)
			seq = BigInteger.valueOf((Integer) seq_result.get(0));
		else
			seq = (BigInteger) seq_result.get(0);

		if (max != null && seq != null) {

			int compare = max.compareTo(seq);
			if (compare == 1) {
				seq = max;
				entityManager.createNativeQuery("Select setval('variantset_variantset_id_seq', " + seq + ", true)");
			}
		}

		return seq;

	}

}

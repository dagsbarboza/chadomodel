package chado.loader.service;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import chado.loader.AppContext;
import chado.loader.model.SnpFeature;
import chado.loader.model.Variantset;

public class SnpFeatureService {

	private EntityManager entityManager;

	public SnpFeatureService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<SnpFeature> getAllSnpFeature() {
		return entityManager.createNamedQuery("SnpFeature.findAll").getResultList();
	}

	public List<SnpFeature> getSnpFeatureById(Integer id) {
		return entityManager.createNamedQuery("SnpFeature.findBySnpFeatureId").setParameter("snpFeatureId", id)
				.getResultList();
	}

	public List<SnpFeature> getSnpFeatureByVariantSetId(Variantset variantset) {
		return entityManager.createNamedQuery("SnpFeature.findByVariantsetId").setParameter("variantsetId", variantset)
				.getResultList();
	}

	public void insertRecord(SnpFeature snpfeature) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();

		entityManager.persist(snpfeature);
		entityManager.getTransaction().commit();
	}

	public List<SnpFeature> getSnpFeatureByIdAndVariantSet(Integer id, Variantset vset) {
		return entityManager.createNamedQuery("SnpFeature.findBySnpFeatureIdAndVariantSet")
				.setParameter("snpFeatureId", id).setParameter("variantSet", vset).getResultList();
	}

	public BigInteger getSnpFeatureCurrentSeqNumber() {

		BigInteger max = null;
		BigInteger seq = null;

		List max_result = entityManager.createNativeQuery("select max(snp_feature_id) from snp_feature")
				.getResultList();
		List seq_result = entityManager.createNativeQuery("SELECT last_value from snp_feature_snp_feature_id_seq")
				.getResultList();

		if (max_result.get(0) instanceof Integer)
			max = BigInteger.valueOf((Integer) max_result.get(0));
		else
			max = (BigInteger) max_result.get(0);

		if (seq_result.get(0) instanceof Integer)
			seq = BigInteger.valueOf((Integer) seq_result.get(0));
		else
			seq = (BigInteger) seq_result.get(0);
		

		if (max != null && seq != null) {

			int compare = max.compareTo(seq);

			if (compare == 1) {
				seq = max;
				entityManager.createNativeQuery("Select setval('snp_feature_snp_feature_id_seq', " + seq + ", true)");
			}
		}

		return seq;
	}

}

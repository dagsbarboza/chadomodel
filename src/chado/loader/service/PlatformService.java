package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chado.loader.AppContext;
import chado.loader.model.Platform;
import chado.loader.model.Stock;

public class PlatformService {

	private EntityManager entityManager;

	public PlatformService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Stock> finStockyId(Integer id) {
		return entityManager.createNamedQuery("Stock.findByPlatformId").setParameter("platformId", id).getResultList();
	}

	public void insertRecord(Platform platform) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(platform);

		entityManager.getTransaction().commit();
	}

}

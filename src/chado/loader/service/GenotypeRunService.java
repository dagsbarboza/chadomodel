package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chado.loader.AppContext;
import chado.loader.model.GenotypeRun;
import chado.loader.model.Stock;

public class GenotypeRunService {

	private EntityManager entityManager;

	public GenotypeRunService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<GenotypeRun> findByGenotypeRunId(Integer id) {
		return entityManager.createNamedQuery("GenotypeRun.findByGenotypeRunId").setParameter("genotypeRunId", id)
				.getResultList();
	}

	public void insertRecord(GenotypeRun genotypeRun) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();

		entityManager.persist(genotypeRun);
		entityManager.getTransaction().commit();
	}

}

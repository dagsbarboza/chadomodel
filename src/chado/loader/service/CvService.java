package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;

import chado.loader.AppContext;
import chado.loader.model.Cv;

public class CvService {
	private EntityManager entityManager;

	public CvService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Cv> findAll() {
		return entityManager.createNamedQuery("Cv.findAll").getResultList();
	}
	
	public List<Cv> findAllName() {
		return entityManager.createNamedQuery("Cv.findAllNames").getResultList();
	}

	public List<Cv> findbyCvId(Integer id) {
		return entityManager.createNamedQuery("Cv.findById").setParameter("CvId", id).getResultList();
	}

	public List<Cv> findByCvName(String name) {
		return entityManager.createNamedQuery("Cv.findByName").setParameter("name", name).getResultList();
	}

	public void insertRecord(Cv Cv) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();

		entityManager.persist(Cv);
		entityManager.getTransaction().commit();
	}

}

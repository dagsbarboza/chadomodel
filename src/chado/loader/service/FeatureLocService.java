package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Feature;
import chado.loader.model.Featureloc;
import chado.loader.model.Organism;

@Service
public class FeatureLocService {

	private EntityManager em;

	public FeatureLocService() {
		em = AppContext.getEntityManager();
	}

	public Featureloc getFeatureLocById(Integer id) {

		List<Featureloc> result = em.createNamedQuery("Feature.findFeatureById").setParameter("FeatureId", id)
				.getResultList();

		if (result.isEmpty())
			return null;
		return result.get(0);

	}

	public List<Featureloc> getAllFeatureloc() {
		return em.createNamedQuery("Featureloc.findAll").getResultList();
	}

	public Featureloc insertRecord(Featureloc featureloc) {
		if (!em.getTransaction().isActive())
			em.getTransaction().begin();
		em.persist(featureloc);
		em.getTransaction().commit();
		
		return featureloc;

	}

	

}

package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Feature;
import chado.loader.model.Organism;

@Service
public class FeatureService {

	private EntityManager em;

	public FeatureService() {
		em = AppContext.getEntityManager();
	}

	public Feature getFeatureById(Integer id) {

		List<Feature> result = em.createNamedQuery("Feature.findFeatureId").setParameter("featureId", id)
				.getResultList();

		if (result.isEmpty())
			return null;
		return result.get(0);

	}

	public List<Feature> getFeatureByName(String name) {
		return em.createNamedQuery("Feature.findFeatureyName").setParameter("name", name).getResultList();
	}

	public List<Feature> getFeatureByNameTypeOrganism(String name, Cvterm cvTerm, Organism organism) {
		Query q = em.createNamedQuery("Feature.findByNameOrganismType").setParameter("name", name)
				.setParameter("cvterm", cvTerm).setParameter("organism", organism);

		return q.getResultList();
	}

	public List<Feature> getAllFeature() {
		return em.createNamedQuery("Feature.findAll").getResultList();
	}

	public Feature insertRecord(Feature feature) {
		if (!em.getTransaction().isActive())
			em.getTransaction().begin();
		em.persist(feature);
		em.getTransaction().commit();
		
		return feature;

	}

	

}

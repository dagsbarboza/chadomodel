package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chado.loader.AppContext;
import chado.loader.model.Db;
import chado.loader.model.Organism;

public class OrganismService {

	private EntityManager entityManager;

	public OrganismService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Organism> getAllOrganism() {
		return entityManager.createNamedQuery("Organism.findAll").getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Organism> findOrganismId(Integer id) {
		return entityManager.createNamedQuery("Organism.findOrganismById").setParameter("organismId", id)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Organism> findOrganismByCommonName(String commonName) {
		return entityManager.createNamedQuery("Organism.findOrganismByCommonName")
				.setParameter("commonName", commonName).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Organism> findOrganismByGenus(String genus) {
		return entityManager.createNamedQuery("Organism.findOrganismByGenus").setParameter("genus", genus)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Organism> findOrganismBySpecies(String species) {
		return entityManager.createNamedQuery("Organism.findOrganismBySpecies").setParameter("species", species)
				.getResultList();
	}

	public void insertRecord(Organism organism) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(organism);

		entityManager.getTransaction().commit();
	}

	public List<String> getAllOrganismName() {
		return entityManager.createNamedQuery("Organism.findAllNames").getResultList();
	}

	

}

package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import chado.loader.AppContext;
import chado.loader.model.Db;
import chado.loader.model.Dbxref;

public class DbXrefService {

	private EntityManager entityManager;

	public DbXrefService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Dbxref> findByAll() {
		return entityManager.createNamedQuery("Dbxref.findAll").getResultList();
	}

	public List<Dbxref> findByAllAccession() {
		return entityManager.createNamedQuery("Dbxref.findAllAccession").getResultList();
	}

	public List<Dbxref> findById(Integer id) {
		return entityManager.createNamedQuery("Dbxref.findByDbXrefIdId").setParameter("dbxrefId", id).getResultList();
	}

	public List<Dbxref> findByAccession(String accession) {
		return entityManager.createNamedQuery("Dbxref.findByAccession").setParameter("accession", accession)
				.getResultList();
	}

	public Dbxref insertRecord(Dbxref dbxref) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(dbxref);

		entityManager.getTransaction().commit();
		
		return dbxref;
	}

	public List<Dbxref> findByDbAndAccession(String accession, Db db) {

		Query q = entityManager.createNamedQuery("Dbxref.findByAccessionAndDb").setParameter("accession", accession)
				.setParameter("db", db);

		return q.getResultList();

	}

}

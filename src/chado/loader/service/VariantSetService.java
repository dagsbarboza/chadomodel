package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;

import chado.loader.AppContext;
import chado.loader.model.Variantset;

public class VariantSetService {

	private EntityManager entityManager;

	public VariantSetService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Variantset> getAllVariantSet() {
		return entityManager.createNamedQuery("Variantset.findAll").getResultList();
	}

	public List<Variantset> getVariantSetById(Integer id) {
		return entityManager.createNamedQuery("Variantset.findById").setParameter("variantsetId", id)
				.getResultList();
	}

	public List<Variantset> getVariantSetByName(String name) {
		return entityManager.createNamedQuery("Variantset.findByName").setParameter("name", name)
				.getResultList();
	}

	public void insertRecord(Variantset variantSet) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(variantSet);

		entityManager.getTransaction().commit();
	}

	public List<String> getAllVariantSetName() {
		return entityManager.createNamedQuery("Variantset.findVarientSetName").getResultList();

	}

}

package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Organism;
import chado.loader.model.Stock;

public class StockService {

	private EntityManager entityManager;

	public StockService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Stock> findAll() {
		return entityManager.createNamedQuery("Stock.findAll").getResultList();
	}

	public List<Stock> findByStockyId(Integer id) {
		return entityManager.createNamedQuery("Stock.findById").setParameter("stockId", id).getResultList();
	}

	public Object insertRecord(Stock stock) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(stock);

		entityManager.getTransaction().commit();
		return stock;

	}

	public List<Stock> findByStockyByOrganismTypeName(Cvterm cvterm, Organism organism, String name) {
		Query q = entityManager.createNamedQuery("Stock.findByOrganismTypeName").setParameter("cvterm", cvterm)
				.setParameter("uniquename", name).setParameter("organism", organism);

		return q.getResultList();

	}
	
	public List<Stock> findByStockyByOrganism(Organism organism) {
		Query q = entityManager.createNamedQuery("Stock.findByOrganism").setParameter("organism", organism);

		return q.getResultList();

	}

}

package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;

public class CvTermService {

	private EntityManager entityManager;

	public CvTermService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Cvterm> findAll() {
		return entityManager.createNamedQuery("Cvterm.findAll").getResultList();
	}

	public List<Cvterm> findCvtermyId(Integer id) {
		return entityManager.createNamedQuery("Cvterm.findById").setParameter("cvtermId", id).getResultList();
	}

	public List<Cvterm> findCvtermyName(String name) {
		return entityManager.createNamedQuery("Cvterm.findByName").setParameter("name", name).getResultList();
	}

	public void insertRecord(Cvterm cvterm) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();

		entityManager.persist(cvterm);
		entityManager.getTransaction().commit();
	}

	public List<String> getAllCvTermName() {
		return entityManager.createNamedQuery("Cvterm.findAllNames").getResultList();

	}

	public List<Cvterm> getFeatureType(String cvtermName, String cvname) {
		return entityManager.createNamedQuery("Cvterm.findFeatureType").setParameter("ctName", cvtermName)
				.setParameter("cvName", cvname).getResultList();

	}

}

package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chado.loader.AppContext;
import chado.loader.model.SnpFeature;
import chado.loader.model.SnpFeatureloc;

public class SnpFeatureLocService {

	private EntityManager entityManager;

	public SnpFeatureLocService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<SnpFeatureloc> findBySnpFeatureLocById(Integer id) {
		return entityManager.createNamedQuery("SnpFeature.findBySnpFeatureLocId").setParameter("snpFeaturelocId", id)
				.getResultList();
	}

	public List<SnpFeatureloc> findBySnpFeatureByPosAndFeatureName(Integer position, String name) {
		return entityManager.createNamedQuery("SnpFeature.findBySnpFeaturebyPosAndFeature").setParameter("name", name)
				.setParameter("position", position).getResultList();
	}

	public void insertRecord(SnpFeatureloc snpfeatureLoc) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(snpfeatureLoc);

		entityManager.getTransaction().commit();
	}

}

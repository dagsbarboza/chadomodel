package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Organism;
import chado.loader.model.Stock;
import chado.loader.model.StockSample;

public class StockSampleService {

	private EntityManager entityManager;

	public StockSampleService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Stock> findAll() {
		return entityManager.createNamedQuery("StockSample.findAll").getResultList();
	}

	public List<Stock> findByStockyId(Integer id) {
		return entityManager.createNamedQuery("StockSample.findById").setParameter("stockId", id).getResultList();
	}

	public Object insertRecord(StockSample stockSample) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(stockSample);

		entityManager.getTransaction().commit();
		return stockSample;

	}

//	public List<StockSample> findByStockyByOrganismTypeName(Cvterm cvterm, Organism organism, String name) {
//		Query q = entityManager.createNamedQuery("Stock.findByOrganismTypeName").setParameter("cvterm", cvterm)
//				.setParameter("uniquename", name).setParameter("organism", organism);
//
//		return q.getResultList();
//
//	}

}

package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chado.loader.AppContext;
import chado.loader.model.SnpFeatureprop;

public class SnpFeaturePropService {

	private EntityManager entityManager;

	public SnpFeaturePropService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<SnpFeatureprop> findSnpFeatureId(Integer id) {
		return entityManager.createNamedQuery("SnpFeature.findBysnpFeaturePropId").setParameter("snpFeaturepropId", id)
				.getResultList();
	}

	public void insertRecord(SnpFeatureprop snpfeatureProp) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(snpfeatureProp);

		entityManager.getTransaction().commit();
	}

}

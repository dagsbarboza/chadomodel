package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Organism;
import chado.loader.model.Stockprop;

public class StockPropService {

	private EntityManager entityManager;

	public StockPropService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<Stockprop> findAll() {
		return entityManager.createNamedQuery("Stockprop.findAll").getResultList();
	}

	public List<Stockprop> findByStockyId(Integer id) {
		return entityManager.createNamedQuery("Stockprop.findById").setParameter("stockpropId", id).getResultList();
	}

	public Object insertRecord(Stockprop stockprop) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(stockprop);

		entityManager.getTransaction().commit();
		return stockprop;

	}

	public List<Stockprop> findByStockyByOrganism(Organism organism) {
		Query q = entityManager.createNamedQuery("Stockprop.findByOrganism").setParameter("organism", organism);

		return q.getResultList();

	}

}

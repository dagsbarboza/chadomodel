package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Organism;
import chado.loader.model.SampleVarietyset;
import chado.loader.model.Stock;
import chado.loader.model.StockSample;

public class SampleVarietySetService {

	private EntityManager entityManager;

	public SampleVarietySetService() {
		entityManager = AppContext.getEntityManager();
	}

	public List<SampleVarietyset> findAll() {
		return entityManager.createNamedQuery("SampleVarietyset.findAll").getResultList();
	}

	public Object insertRecord(SampleVarietyset sampleVarietySet) {
		if (!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(sampleVarietySet);

		entityManager.getTransaction().commit();
		return sampleVarietySet;

	}

//	public List<StockSample> findByStockyByOrganismTypeName(Cvterm cvterm, Organism organism, String name) {
//		Query q = entityManager.createNamedQuery("Stock.findByOrganismTypeName").setParameter("cvterm", cvterm)
//				.setParameter("uniquename", name).setParameter("organism", organism);
//
//		return q.getResultList();
//
//	}

}

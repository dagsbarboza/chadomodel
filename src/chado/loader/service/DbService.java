package chado.loader.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import chado.loader.AppContext;
import chado.loader.model.Db;

@Service
public class DbService {

	private EntityManager em;

	public DbService() {
		em = AppContext.getEntityManager();
	}

	public Db getDbById(Integer id) {

		List<Db> result = em.createNamedQuery("Db.findById").setParameter("dbId", id).getResultList();

		if (result.isEmpty())
			return null;
		return result.get(0);

	}

	public List<Db> getDbByName(String name) {
		return em.createNamedQuery("Db.findByName").setParameter("name", name).getResultList();
	}

	public List<String> getDbNameByName(String name) {
		return em.createNamedQuery("Db.findNameByName").setParameter("name", name).getResultList();
	}

	public List<Db> getAllDb() {
		return em.createNamedQuery("Db.findAll").getResultList();
	}

	public Object insertRecord(Db db) {
		if (!em.getTransaction().isActive())
			em.getTransaction().begin();
		em.persist(db);
		em.getTransaction().commit();
		return db;

	}

	public List<String> getAllDbName() {
		return em.createNamedQuery("Db.findAllName").getResultList();
	}

}

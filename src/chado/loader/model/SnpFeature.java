package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the snp_feature database table.
 * 
 */
@Entity
@Table(name = "snp_feature")
@NamedQueries({ @NamedQuery(name = "SnpFeature.findAll", query = "SELECT s FROM SnpFeature s"),
		@NamedQuery(name = "SnpFeature.findBySnpFeatureId", query = "SELECT s FROM SnpFeature s where s.snpFeatureId = :snpFeatureId"),
		@NamedQuery(name = "SnpFeature.findByVariantsetId", query = "SELECT s FROM SnpFeature s where s.variantset = :variantsetId order by s.snpFeatureId" ),
		@NamedQuery(name = "SnpFeature.findBySnpFeatureIdAndVariantSet", query = "SELECT s FROM SnpFeature s where s.snpFeatureId = :snpFeatureId and s.variantset=:variantSet")
})

public class SnpFeature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "snp_feature_id")
	private Integer snpFeatureId;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "snpFeature")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs;

	// bi-directional many-to-one association to Variantset
	@ManyToOne
	@JoinColumn(name = "variantset_id")
	private Variantset variantset;

	// bi-directional many-to-one association to SnpFeatureloc
	@OneToMany(mappedBy = "snpFeature")
	private List<SnpFeatureloc> snpFeaturelocs;

	// bi-directional many-to-one association to SnpFeatureprop
	@OneToMany(mappedBy = "snpFeature")
	private List<SnpFeatureprop> snpFeatureprops;

	public SnpFeature() {
	}

	public Integer getSnpFeatureId() {
		return this.snpFeatureId;
	}

	public void setSnpFeatureId(Integer snpFeatureId) {
		this.snpFeatureId = snpFeatureId;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs() {
		return this.mvConvertposNb2allrefs;
	}

	public void setMvConvertposNb2allrefs(List<MvConvertposNb2allref> mvConvertposNb2allrefs) {
		this.mvConvertposNb2allrefs = mvConvertposNb2allrefs;
	}

	public MvConvertposNb2allref addMvConvertposNb2allref(MvConvertposNb2allref mvConvertposNb2allref) {
		getMvConvertposNb2allrefs().add(mvConvertposNb2allref);
		mvConvertposNb2allref.setSnpFeature(this);

		return mvConvertposNb2allref;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allref(MvConvertposNb2allref mvConvertposNb2allref) {
		getMvConvertposNb2allrefs().remove(mvConvertposNb2allref);
		mvConvertposNb2allref.setSnpFeature(null);

		return mvConvertposNb2allref;
	}

	public Variantset getVariantset() {
		return this.variantset;
	}

	public void setVariantset(Variantset variantset) {
		this.variantset = variantset;
	}

	public List<SnpFeatureloc> getSnpFeaturelocs() {
		return this.snpFeaturelocs;
	}

	public void setSnpFeaturelocs(List<SnpFeatureloc> snpFeaturelocs) {
		this.snpFeaturelocs = snpFeaturelocs;
	}

	public SnpFeatureloc addSnpFeatureloc(SnpFeatureloc snpFeatureloc) {
		getSnpFeaturelocs().add(snpFeatureloc);
		snpFeatureloc.setSnpFeature(this);

		return snpFeatureloc;
	}

	public SnpFeatureloc removeSnpFeatureloc(SnpFeatureloc snpFeatureloc) {
		getSnpFeaturelocs().remove(snpFeatureloc);
		snpFeatureloc.setSnpFeature(null);

		return snpFeatureloc;
	}

	public List<SnpFeatureprop> getSnpFeatureprops() {
		return this.snpFeatureprops;
	}

	public void setSnpFeatureprops(List<SnpFeatureprop> snpFeatureprops) {
		this.snpFeatureprops = snpFeatureprops;
	}

	public SnpFeatureprop addSnpFeatureprop(SnpFeatureprop snpFeatureprop) {
		getSnpFeatureprops().add(snpFeatureprop);
		snpFeatureprop.setSnpFeature(this);

		return snpFeatureprop;
	}

	public SnpFeatureprop removeSnpFeatureprop(SnpFeatureprop snpFeatureprop) {
		getSnpFeatureprops().remove(snpFeatureprop);
		snpFeatureprop.setSnpFeature(null);

		return snpFeatureprop;
	}

}
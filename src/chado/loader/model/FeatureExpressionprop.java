package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_expressionprop database table.
 * 
 */
@Entity
@Table(name="feature_expressionprop")
@NamedQuery(name="FeatureExpressionprop.findAll", query="SELECT f FROM FeatureExpressionprop f")
public class FeatureExpressionprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_expressionprop_id")
	private Integer featureExpressionpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to FeatureExpression
	@ManyToOne
	@JoinColumn(name="feature_expression_id")
	private FeatureExpression featureExpression;

	public FeatureExpressionprop() {
	}

	public Integer getFeatureExpressionpropId() {
		return this.featureExpressionpropId;
	}

	public void setFeatureExpressionpropId(Integer featureExpressionpropId) {
		this.featureExpressionpropId = featureExpressionpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public FeatureExpression getFeatureExpression() {
		return this.featureExpression;
	}

	public void setFeatureExpression(FeatureExpression featureExpression) {
		this.featureExpression = featureExpression;
	}

}
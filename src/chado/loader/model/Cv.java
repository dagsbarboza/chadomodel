package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the cv database table.
 * 
 */
@Entity
@NamedQueries({ 
	@NamedQuery(name = "Cv.findAll", query = "SELECT c FROM Cv c"),
	@NamedQuery(name = "Cv.findAllNames", query = "SELECT c.name FROM Cv c"),
		@NamedQuery(name = "Cv.findByName", query = "SELECT c FROM Cv c where c.name=:name"),
		@NamedQuery(name = "Cv.findById", query = "SELECT c FROM Cv c where c.cvId=:cvId") })

public class Cv implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cv_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer cvId;

	private String definition;

	private String name;

	// bi-directional many-to-one association to Cvprop
	@OneToMany(mappedBy = "cv")
	private List<Cvprop> cvprops;

	// bi-directional many-to-one association to Cvterm
	@OneToMany(mappedBy = "cv")
	private List<Cvterm> cvterms;

	// bi-directional many-to-one association to Cvtermpath
	@OneToMany(mappedBy = "cv")
	private List<Cvtermpath> cvtermpaths;

	public Cv() {
	}

	public Integer getCvId() {
		return this.cvId;
	}

	public void setCvId(Integer cvId) {
		this.cvId = cvId;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Cvprop> getCvprops() {
		return this.cvprops;
	}

	public void setCvprops(List<Cvprop> cvprops) {
		this.cvprops = cvprops;
	}

	public Cvprop addCvprop(Cvprop cvprop) {
		getCvprops().add(cvprop);
		cvprop.setCv(this);

		return cvprop;
	}

	public Cvprop removeCvprop(Cvprop cvprop) {
		getCvprops().remove(cvprop);
		cvprop.setCv(null);

		return cvprop;
	}

	public List<Cvterm> getCvterms() {
		return this.cvterms;
	}

	public void setCvterms(List<Cvterm> cvterms) {
		this.cvterms = cvterms;
	}

	public Cvterm addCvterm(Cvterm cvterm) {
		getCvterms().add(cvterm);
		cvterm.setCv(this);

		return cvterm;
	}

	public Cvterm removeCvterm(Cvterm cvterm) {
		getCvterms().remove(cvterm);
		cvterm.setCv(null);

		return cvterm;
	}

	public List<Cvtermpath> getCvtermpaths() {
		return this.cvtermpaths;
	}

	public void setCvtermpaths(List<Cvtermpath> cvtermpaths) {
		this.cvtermpaths = cvtermpaths;
	}

	public Cvtermpath addCvtermpath(Cvtermpath cvtermpath) {
		getCvtermpaths().add(cvtermpath);
		cvtermpath.setCv(this);

		return cvtermpath;
	}

	public Cvtermpath removeCvtermpath(Cvtermpath cvtermpath) {
		getCvtermpaths().remove(cvtermpath);
		cvtermpath.setCv(null);

		return cvtermpath;
	}

}
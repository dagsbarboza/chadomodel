package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the gwas_run database table.
 * 
 */
@Entity
@Table(name="gwas_run")
@NamedQuery(name="GwasRun.findAll", query="SELECT g FROM GwasRun g")
public class GwasRun implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="gwas_run_id")
	private Integer gwasRunId;

	//bi-directional many-to-one association to GwasSubpopulation
	@ManyToOne
	@JoinColumn(name="subpopulation_id")
	private GwasSubpopulation gwasSubpopulation;

	//bi-directional many-to-one association to GwasTrait
	@ManyToOne
	@JoinColumn(name="trait_id")
	private GwasTrait gwasTrait;

	public GwasRun() {
	}

	public Integer getGwasRunId() {
		return this.gwasRunId;
	}

	public void setGwasRunId(Integer gwasRunId) {
		this.gwasRunId = gwasRunId;
	}

	public GwasSubpopulation getGwasSubpopulation() {
		return this.gwasSubpopulation;
	}

	public void setGwasSubpopulation(GwasSubpopulation gwasSubpopulation) {
		this.gwasSubpopulation = gwasSubpopulation;
	}

	public GwasTrait getGwasTrait() {
		return this.gwasTrait;
	}

	public void setGwasTrait(GwasTrait gwasTrait) {
		this.gwasTrait = gwasTrait;
	}

}
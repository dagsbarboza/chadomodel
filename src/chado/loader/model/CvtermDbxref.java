package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvterm_dbxref database table.
 * 
 */
@Entity
@Table(name="cvterm_dbxref")
@NamedQuery(name="CvtermDbxref.findAll", query="SELECT c FROM CvtermDbxref c")
public class CvtermDbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvterm_dbxref_id")
	private Integer cvtermDbxrefId;

	@Column(name="is_for_definition")
	private Integer isForDefinition;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	public CvtermDbxref() {
	}

	public Integer getCvtermDbxrefId() {
		return this.cvtermDbxrefId;
	}

	public void setCvtermDbxrefId(Integer cvtermDbxrefId) {
		this.cvtermDbxrefId = cvtermDbxrefId;
	}

	public Integer getIsForDefinition() {
		return this.isForDefinition;
	}

	public void setIsForDefinition(Integer isForDefinition) {
		this.isForDefinition = isForDefinition;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

}
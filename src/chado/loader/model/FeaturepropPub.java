package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the featureprop_pub database table.
 * 
 */
@Entity
@Table(name="featureprop_pub")
@NamedQuery(name="FeaturepropPub.findAll", query="SELECT f FROM FeaturepropPub f")
public class FeaturepropPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="featureprop_pub_id")
	private Integer featurepropPubId;

	//bi-directional many-to-one association to Featureprop
	@ManyToOne
	@JoinColumn(name="featureprop_id")
	private Featureprop featureprop;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public FeaturepropPub() {
	}

	public Integer getFeaturepropPubId() {
		return this.featurepropPubId;
	}

	public void setFeaturepropPubId(Integer featurepropPubId) {
		this.featurepropPubId = featurepropPubId;
	}

	public Featureprop getFeatureprop() {
		return this.featureprop;
	}

	public void setFeatureprop(Featureprop featureprop) {
		this.featureprop = featureprop;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
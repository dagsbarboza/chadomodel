package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the genotype_run database table.
 * 
 */
@Entity
@Table(name="genotype_run")
@NamedQueries({
	@NamedQuery(name="GenotypeRun.findAll", query="SELECT g FROM GenotypeRun g"),
	@NamedQuery(name="GenotypeRun.findByGenotypeRunId", query="SELECT g FROM GenotypeRun g where g.genotypeRunId=:genotypeRunId")
})

public class GenotypeRun implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="genotype_run_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer genotypeRunId;

	@Column(name="data_location")
	private String dataLocation;

	@Temporal(TemporalType.DATE)
	@Column(name="date_performed")
	private Date datePerformed;

	@Column(name="platform_id")
	private Integer platformId;

	public GenotypeRun() {
	}

	public Integer getGenotypeRunId() {
		return this.genotypeRunId;
	}

	public void setGenotypeRunId(Integer genotypeRunId) {
		this.genotypeRunId = genotypeRunId;
	}

	public String getDataLocation() {
		return this.dataLocation;
	}

	public void setDataLocation(String dataLocation) {
		this.dataLocation = dataLocation;
	}

	public Date getDatePerformed() {
		return this.datePerformed;
	}

	public void setDatePerformed(Date datePerformed) {
		this.datePerformed = datePerformed;
	}

	public Integer getPlatformId() {
		return this.platformId;
	}

	public void setPlatformId(Integer platformId) {
		this.platformId = platformId;
	}

}
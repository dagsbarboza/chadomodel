package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the sample_varietyset database table.
 * 
 */
@Entity
@Table(name = "sample_varietyset")
@NamedQuery(name = "SampleVarietyset.findAll", query = "SELECT s FROM SampleVarietyset s")
public class SampleVarietyset implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "sample_varietyset_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer sampleVarietysetId;

	@Column(name = "db_id")
	private Integer dbId;

	@Column(name = "hdf5_index")
	private Integer hdf5Index;

	@Column(name = "stock_sample_id")
	private Integer stockSampleId;

	public SampleVarietyset() {
	}

	public Integer getSampleVarietysetId() {
		return this.sampleVarietysetId;
	}

	public void setSampleVarietysetId(Integer sampleVarietysetId) {
		this.sampleVarietysetId = sampleVarietysetId;
	}

	public Integer getDbId() {
		return this.dbId;
	}

	public void setDbId(Integer dbId) {
		this.dbId = dbId;
	}

	public Integer getHdf5Index() {
		return this.hdf5Index;
	}

	public void setHdf5Index(Integer hdf5Index) {
		this.hdf5Index = hdf5Index;
	}

	public Integer getStockSampleId() {
		return this.stockSampleId;
	}

	public void setStockSampleId(Integer stockSampleId) {
		this.stockSampleId = stockSampleId;
	}

}
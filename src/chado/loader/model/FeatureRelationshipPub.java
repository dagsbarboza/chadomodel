package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_relationship_pub database table.
 * 
 */
@Entity
@Table(name="feature_relationship_pub")
@NamedQuery(name="FeatureRelationshipPub.findAll", query="SELECT f FROM FeatureRelationshipPub f")
public class FeatureRelationshipPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_relationship_pub_id")
	private Integer featureRelationshipPubId;

	//bi-directional many-to-one association to FeatureRelationship
	@ManyToOne
	@JoinColumn(name="feature_relationship_id")
	private FeatureRelationship featureRelationship;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public FeatureRelationshipPub() {
	}

	public Integer getFeatureRelationshipPubId() {
		return this.featureRelationshipPubId;
	}

	public void setFeatureRelationshipPubId(Integer featureRelationshipPubId) {
		this.featureRelationshipPubId = featureRelationshipPubId;
	}

	public FeatureRelationship getFeatureRelationship() {
		return this.featureRelationship;
	}

	public void setFeatureRelationship(FeatureRelationship featureRelationship) {
		this.featureRelationship = featureRelationship;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
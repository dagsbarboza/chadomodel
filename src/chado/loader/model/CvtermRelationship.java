package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvterm_relationship database table.
 * 
 */
@Entity
@Table(name="cvterm_relationship")
@NamedQuery(name="CvtermRelationship.findAll", query="SELECT c FROM CvtermRelationship c")
public class CvtermRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvterm_relationship_id")
	private Integer cvtermRelationshipId;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="object_id")
	private Cvterm cvterm1;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Cvterm cvterm2;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm3;

	public CvtermRelationship() {
	}

	public Integer getCvtermRelationshipId() {
		return this.cvtermRelationshipId;
	}

	public void setCvtermRelationshipId(Integer cvtermRelationshipId) {
		this.cvtermRelationshipId = cvtermRelationshipId;
	}

	public Cvterm getCvterm1() {
		return this.cvterm1;
	}

	public void setCvterm1(Cvterm cvterm1) {
		this.cvterm1 = cvterm1;
	}

	public Cvterm getCvterm2() {
		return this.cvterm2;
	}

	public void setCvterm2(Cvterm cvterm2) {
		this.cvterm2 = cvterm2;
	}

	public Cvterm getCvterm3() {
		return this.cvterm3;
	}

	public void setCvterm3(Cvterm cvterm3) {
		this.cvterm3 = cvterm3;
	}

}
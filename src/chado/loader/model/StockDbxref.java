package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stock_dbxref database table.
 * 
 */
@Entity
@Table(name="stock_dbxref")
@NamedQuery(name="StockDbxref.findAll", query="SELECT s FROM StockDbxref s")
public class StockDbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_dbxref_id")
	private Integer stockDbxrefId;

	@Column(name="is_current")
	private Boolean isCurrent;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	//bi-directional many-to-one association to StockDbxrefprop
	@OneToMany(mappedBy="stockDbxref")
	private List<StockDbxrefprop> stockDbxrefprops;

	public StockDbxref() {
	}

	public Integer getStockDbxrefId() {
		return this.stockDbxrefId;
	}

	public void setStockDbxrefId(Integer stockDbxrefId) {
		this.stockDbxrefId = stockDbxrefId;
	}

	public Boolean getIsCurrent() {
		return this.isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public List<StockDbxrefprop> getStockDbxrefprops() {
		return this.stockDbxrefprops;
	}

	public void setStockDbxrefprops(List<StockDbxrefprop> stockDbxrefprops) {
		this.stockDbxrefprops = stockDbxrefprops;
	}

	public StockDbxrefprop addStockDbxrefprop(StockDbxrefprop stockDbxrefprop) {
		getStockDbxrefprops().add(stockDbxrefprop);
		stockDbxrefprop.setStockDbxref(this);

		return stockDbxrefprop;
	}

	public StockDbxrefprop removeStockDbxrefprop(StockDbxrefprop stockDbxrefprop) {
		getStockDbxrefprops().remove(stockDbxrefprop);
		stockDbxrefprop.setStockDbxref(null);

		return stockDbxrefprop;
	}

}
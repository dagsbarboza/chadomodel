package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the indel_featureloc database table.
 * 
 */
@Entity
@Table(name="indel_featureloc")
@NamedQuery(name="IndelFeatureloc.findAll", query="SELECT i FROM IndelFeatureloc i")
public class IndelFeatureloc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="indel_featureloc_id")
	private Integer indelFeaturelocId;

	private String altcall;

	@Column(name="max_delete_len")
	private Integer maxDeleteLen;

	@Column(name="max_insert_len")
	private Integer maxInsertLen;

	private Integer position;

	private String refcall;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="srcfeature_id")
	private Feature feature;

	//bi-directional many-to-one association to IndelFeature
	@ManyToOne
	@JoinColumn(name="indel_feature_id")
	private IndelFeature indelFeature;

	//bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name="organism_id")
	private Organism organism;

	public IndelFeatureloc() {
	}

	public Integer getIndelFeaturelocId() {
		return this.indelFeaturelocId;
	}

	public void setIndelFeaturelocId(Integer indelFeaturelocId) {
		this.indelFeaturelocId = indelFeaturelocId;
	}

	public String getAltcall() {
		return this.altcall;
	}

	public void setAltcall(String altcall) {
		this.altcall = altcall;
	}

	public Integer getMaxDeleteLen() {
		return this.maxDeleteLen;
	}

	public void setMaxDeleteLen(Integer maxDeleteLen) {
		this.maxDeleteLen = maxDeleteLen;
	}

	public Integer getMaxInsertLen() {
		return this.maxInsertLen;
	}

	public void setMaxInsertLen(Integer maxInsertLen) {
		this.maxInsertLen = maxInsertLen;
	}

	public Integer getPosition() {
		return this.position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getRefcall() {
		return this.refcall;
	}

	public void setRefcall(String refcall) {
		this.refcall = refcall;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public IndelFeature getIndelFeature() {
		return this.indelFeature;
	}

	public void setIndelFeature(IndelFeature indelFeature) {
		this.indelFeature = indelFeature;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

}
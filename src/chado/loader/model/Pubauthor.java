package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pubauthor database table.
 * 
 */
@Entity
@NamedQuery(name="Pubauthor.findAll", query="SELECT p FROM Pubauthor p")
public class Pubauthor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pubauthor_id")
	private Integer pubauthorId;

	private Boolean editor;

	private String givennames;

	private Integer rank;

	private String suffix;

	private String surname;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public Pubauthor() {
	}

	public Integer getPubauthorId() {
		return this.pubauthorId;
	}

	public void setPubauthorId(Integer pubauthorId) {
		this.pubauthorId = pubauthorId;
	}

	public Boolean getEditor() {
		return this.editor;
	}

	public void setEditor(Boolean editor) {
		this.editor = editor;
	}

	public String getGivennames() {
		return this.givennames;
	}

	public void setGivennames(String givennames) {
		this.givennames = givennames;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
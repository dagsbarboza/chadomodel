package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_cvterm_dbxref database table.
 * 
 */
@Entity
@Table(name="feature_cvterm_dbxref")
@NamedQuery(name="FeatureCvtermDbxref.findAll", query="SELECT f FROM FeatureCvtermDbxref f")
public class FeatureCvtermDbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_cvterm_dbxref_id")
	private Integer featureCvtermDbxrefId;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to FeatureCvterm
	@ManyToOne
	@JoinColumn(name="feature_cvterm_id")
	private FeatureCvterm featureCvterm;

	public FeatureCvtermDbxref() {
	}

	public Integer getFeatureCvtermDbxrefId() {
		return this.featureCvtermDbxrefId;
	}

	public void setFeatureCvtermDbxrefId(Integer featureCvtermDbxrefId) {
		this.featureCvtermDbxrefId = featureCvtermDbxrefId;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public FeatureCvterm getFeatureCvterm() {
		return this.featureCvterm;
	}

	public void setFeatureCvterm(FeatureCvterm featureCvterm) {
		this.featureCvterm = featureCvterm;
	}

}
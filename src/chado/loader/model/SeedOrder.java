package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the seed_order database table.
 * 
 */
@Entity
@Table(name="seed_order")
@NamedQuery(name="SeedOrder.findAll", query="SELECT s FROM SeedOrder s")
public class SeedOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="seed_order_id")
	private Integer seedOrderId;

	private String address1;

	private String address2;

	@Column(name="authorized_email")
	private String authorizedEmail;

	@Column(name="authorized_name")
	private String authorizedName;

	@Column(name="authorized_phone")
	private String authorizedPhone;

	@Column(name="authorized_position")
	private String authorizedPosition;

	private String country;

	@Column(name="country_category")
	private String countryCategory;

	@Temporal(TemporalType.DATE)
	@Column(name="date_ordered")
	private Date dateOrdered;

	private String institution;

	@Column(name="order_code")
	private String orderCode;

	@Column(name="postal_code")
	private String postalCode;

	@Column(name="requestor_email")
	private String requestorEmail;

	@Column(name="requestor_name")
	private String requestorName;

	@Column(name="requestor_phone")
	private String requestorPhone;

	@Column(name="smta_acceptance")
	private String smtaAcceptance;

	private String status;

	@Column(name="total_price")
	private BigDecimal totalPrice;

	@Column(name="user_category")
	private String userCategory;

	@Column(name="verify_code")
	private String verifyCode;

	public SeedOrder() {
	}

	public Integer getSeedOrderId() {
		return this.seedOrderId;
	}

	public void setSeedOrderId(Integer seedOrderId) {
		this.seedOrderId = seedOrderId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAuthorizedEmail() {
		return this.authorizedEmail;
	}

	public void setAuthorizedEmail(String authorizedEmail) {
		this.authorizedEmail = authorizedEmail;
	}

	public String getAuthorizedName() {
		return this.authorizedName;
	}

	public void setAuthorizedName(String authorizedName) {
		this.authorizedName = authorizedName;
	}

	public String getAuthorizedPhone() {
		return this.authorizedPhone;
	}

	public void setAuthorizedPhone(String authorizedPhone) {
		this.authorizedPhone = authorizedPhone;
	}

	public String getAuthorizedPosition() {
		return this.authorizedPosition;
	}

	public void setAuthorizedPosition(String authorizedPosition) {
		this.authorizedPosition = authorizedPosition;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryCategory() {
		return this.countryCategory;
	}

	public void setCountryCategory(String countryCategory) {
		this.countryCategory = countryCategory;
	}

	public Date getDateOrdered() {
		return this.dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public String getInstitution() {
		return this.institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getOrderCode() {
		return this.orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRequestorEmail() {
		return this.requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestorName() {
		return this.requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestorPhone() {
		return this.requestorPhone;
	}

	public void setRequestorPhone(String requestorPhone) {
		this.requestorPhone = requestorPhone;
	}

	public String getSmtaAcceptance() {
		return this.smtaAcceptance;
	}

	public void setSmtaAcceptance(String smtaAcceptance) {
		this.smtaAcceptance = smtaAcceptance;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getUserCategory() {
		return this.userCategory;
	}

	public void setUserCategory(String userCategory) {
		this.userCategory = userCategory;
	}

	public String getVerifyCode() {
		return this.verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

}
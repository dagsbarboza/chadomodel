package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_dbxref database table.
 * 
 */
@Entity
@Table(name="feature_dbxref")
@NamedQuery(name="FeatureDbxref.findAll", query="SELECT f FROM FeatureDbxref f")
public class FeatureDbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_dbxref_id")
	private Integer featureDbxrefId;

	@Column(name="is_current")
	private Boolean isCurrent;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	public FeatureDbxref() {
	}

	public Integer getFeatureDbxrefId() {
		return this.featureDbxrefId;
	}

	public void setFeatureDbxrefId(Integer featureDbxrefId) {
		this.featureDbxrefId = featureDbxrefId;
	}

	public Boolean getIsCurrent() {
		return this.isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

}
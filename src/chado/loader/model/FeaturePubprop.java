package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_pubprop database table.
 * 
 */
@Entity
@Table(name="feature_pubprop")
@NamedQuery(name="FeaturePubprop.findAll", query="SELECT f FROM FeaturePubprop f")
public class FeaturePubprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_pubprop_id")
	private Integer featurePubpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to FeaturePub
	@ManyToOne
	@JoinColumn(name="feature_pub_id")
	private FeaturePub featurePub;

	public FeaturePubprop() {
	}

	public Integer getFeaturePubpropId() {
		return this.featurePubpropId;
	}

	public void setFeaturePubpropId(Integer featurePubpropId) {
		this.featurePubpropId = featurePubpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public FeaturePub getFeaturePub() {
		return this.featurePub;
	}

	public void setFeaturePub(FeaturePub featurePub) {
		this.featurePub = featurePub;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the snp_featureloc database table.
 * 
 */
@Entity
@Table(name="snp_featureloc")
@NamedQueries({
	@NamedQuery(name="SnpFeatureloc.findAll", query="SELECT s FROM SnpFeatureloc s"),
	@NamedQuery(name="SnpFeatureloc.findBySnpFeatureLocId", query="SELECT s FROM SnpFeatureloc s where s.snpFeaturelocId = :snpFeaturelocId"),
	@NamedQuery(name="SnpFeatureloc.findBySnpFeaturebyPosAndFeature", query="SELECT sfl FROM SnpFeatureloc sfl, Feature f where sfl.position=:position and f.name=:name")
})

public class SnpFeatureloc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="snp_featureloc_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer snpFeaturelocId;

	private Integer position;

	private String refcall;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="srcfeature_id")
	private Feature feature;

	//bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name="organism_id")
	private Organism organism;

	//bi-directional many-to-one association to SnpFeature
	@ManyToOne
	@JoinColumn(name="snp_feature_id")
	private SnpFeature snpFeature;

	public SnpFeatureloc() {
	}

	public Integer getSnpFeaturelocId() {
		return this.snpFeaturelocId;
	}

	public void setSnpFeaturelocId(Integer snpFeaturelocId) {
		this.snpFeaturelocId = snpFeaturelocId;
	}

	public Integer getPosition() {
		return this.position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getRefcall() {
		return this.refcall;
	}

	public void setRefcall(String refcall) {
		this.refcall = refcall;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

	public SnpFeature getSnpFeature() {
		return this.snpFeature;
	}

	public void setSnpFeature(SnpFeature snpFeature) {
		this.snpFeature = snpFeature;
	}

}
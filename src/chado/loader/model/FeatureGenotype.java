package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_genotype database table.
 * 
 */
@Entity
@Table(name="feature_genotype")
@NamedQuery(name="FeatureGenotype.findAll", query="SELECT f FROM FeatureGenotype f")
public class FeatureGenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_genotype_id")
	private Integer featureGenotypeId;

	private Integer cgroup;

	@Column(name="genotype_id")
	private Integer genotypeId;

	private Integer rank;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="chromosome_id")
	private Feature feature1;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature2;

	public FeatureGenotype() {
	}

	public Integer getFeatureGenotypeId() {
		return this.featureGenotypeId;
	}

	public void setFeatureGenotypeId(Integer featureGenotypeId) {
		this.featureGenotypeId = featureGenotypeId;
	}

	public Integer getCgroup() {
		return this.cgroup;
	}

	public void setCgroup(Integer cgroup) {
		this.cgroup = cgroup;
	}

	public Integer getGenotypeId() {
		return this.genotypeId;
	}

	public void setGenotypeId(Integer genotypeId) {
		this.genotypeId = genotypeId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Feature getFeature1() {
		return this.feature1;
	}

	public void setFeature1(Feature feature1) {
		this.feature1 = feature1;
	}

	public Feature getFeature2() {
		return this.feature2;
	}

	public void setFeature2(Feature feature2) {
		this.feature2 = feature2;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the stock_phenotype database table.
 * 
 */
@Entity
@Table(name="stock_phenotype")
@NamedQuery(name="StockPhenotype.findAll", query="SELECT s FROM StockPhenotype s")
public class StockPhenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_phenotype_id")
	private Integer stockPhenotypeId;

	@Column(name="qual_value")
	private String qualValue;

	@Column(name="quan_value")
	private BigDecimal quanValue;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="stock_type_id")
	private Cvterm cvterm1;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm2;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	public StockPhenotype() {
	}

	public Integer getStockPhenotypeId() {
		return this.stockPhenotypeId;
	}

	public void setStockPhenotypeId(Integer stockPhenotypeId) {
		this.stockPhenotypeId = stockPhenotypeId;
	}

	public String getQualValue() {
		return this.qualValue;
	}

	public void setQualValue(String qualValue) {
		this.qualValue = qualValue;
	}

	public BigDecimal getQuanValue() {
		return this.quanValue;
	}

	public void setQuanValue(BigDecimal quanValue) {
		this.quanValue = quanValue;
	}

	public Cvterm getCvterm1() {
		return this.cvterm1;
	}

	public void setCvterm1(Cvterm cvterm1) {
		this.cvterm1 = cvterm1;
	}

	public Cvterm getCvterm2() {
		return this.cvterm2;
	}

	public void setCvterm2(Cvterm cvterm2) {
		this.cvterm2 = cvterm2;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
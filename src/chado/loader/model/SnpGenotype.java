package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the snp_genotype database table.
 * 
 */
@Entity
@Table(name = "snp_genotype")
@NamedQueries({ @NamedQuery(name = "SnpGenotype.findAll", query = "SELECT s FROM SnpGenotype s"), })

public class SnpGenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", insertable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	private String allele1;

	private String allele2;

	@Column(name = "genotype_run_id")
	private Integer genotypeRunId;

	@Column(name = "snp_feature_id")
	private Integer snpFeatureId;

	@Column(name = "stock_sample_id")
	private Integer stockSampleId;

	public SnpGenotype() {
	}

	public String getAllele1() {
		return this.allele1;
	}

	public void setAllele1(String allele1) {
		this.allele1 = allele1;
	}

	public String getAllele2() {
		return this.allele2;
	}

	public void setAllele2(String allele2) {
		this.allele2 = allele2;
	}

	public Integer getGenotypeRunId() {
		return this.genotypeRunId;
	}

	public void setGenotypeRunId(Integer genotypeRunId) {
		this.genotypeRunId = genotypeRunId;
	}

	public Integer getSnpFeatureId() {
		return this.snpFeatureId;
	}

	public void setSnpFeatureId(Integer snpFeatureId) {
		this.snpFeatureId = snpFeatureId;
	}

	public Integer getStockSampleId() {
		return this.stockSampleId;
	}

	public void setStockSampleId(Integer stockSampleId) {
		this.stockSampleId = stockSampleId;
	}

}
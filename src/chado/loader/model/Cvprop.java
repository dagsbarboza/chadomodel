package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvprop database table.
 * 
 */
@Entity
@NamedQuery(name="Cvprop.findAll", query="SELECT c FROM Cvprop c")
public class Cvprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvprop_id")
	private Integer cvpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cv
	@ManyToOne
	@JoinColumn(name="cv_id")
	private Cv cv;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	public Cvprop() {
	}

	public Integer getCvpropId() {
		return this.cvpropId;
	}

	public void setCvpropId(Integer cvpropId) {
		this.cvpropId = cvpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cv getCv() {
		return this.cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

}
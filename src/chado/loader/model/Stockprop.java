package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stockprop database table.
 * 
 */
@Entity
@NamedQuery(name="Stockprop.findAll", query="SELECT s FROM Stockprop s")
public class Stockprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="stockprop_id")
	private Integer stockpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	//bi-directional many-to-one association to StockpropPub
	@OneToMany(mappedBy="stockprop")
	private List<StockpropPub> stockpropPubs;

	public Stockprop() {
	}

	public Integer getStockpropId() {
		return this.stockpropId;
	}

	public void setStockpropId(Integer stockpropId) {
		this.stockpropId = stockpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public List<StockpropPub> getStockpropPubs() {
		return this.stockpropPubs;
	}

	public void setStockpropPubs(List<StockpropPub> stockpropPubs) {
		this.stockpropPubs = stockpropPubs;
	}

	public StockpropPub addStockpropPub(StockpropPub stockpropPub) {
		getStockpropPubs().add(stockpropPub);
		stockpropPub.setStockprop(this);

		return stockpropPub;
	}

	public StockpropPub removeStockpropPub(StockpropPub stockpropPub) {
		getStockpropPubs().remove(stockpropPub);
		stockpropPub.setStockprop(null);

		return stockpropPub;
	}

}
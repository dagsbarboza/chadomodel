package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the variant_variantset database table.
 * 
 */
@Entity
@Table(name="variant_variantset")
@NamedQueries({
	@NamedQuery(name="VariantVariantset.findAll", query="SELECT v FROM VariantVariantset v"),
	@NamedQuery(name="VariantVariantset.findByvariantVariantSetId", query="SELECT v FROM VariantVariantset v where v.variantVariantsetId = :variantVariantsetId")
})

public class VariantVariantset implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="variant_variantset_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer variantVariantsetId;

	@Column(name="hdf5_index")
	private Integer hdf5Index;

	@Column(name="variant_feature_id")
	private Integer variantFeatureId;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="variant_type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Variantset
	@ManyToOne
	@JoinColumn(name="variantset_id")
	private Variantset variantset;

	public VariantVariantset() {
	}

	public Integer getVariantVariantsetId() {
		return this.variantVariantsetId;
	}

	public void setVariantVariantsetId(Integer variantVariantsetId) {
		this.variantVariantsetId = variantVariantsetId;
	}

	public Integer getHdf5Index() {
		return this.hdf5Index;
	}

	public void setHdf5Index(Integer hdf5Index) {
		this.hdf5Index = hdf5Index;
	}

	public Integer getVariantFeatureId() {
		return this.variantFeatureId;
	}

	public void setVariantFeatureId(Integer variantFeatureId) {
		this.variantFeatureId = variantFeatureId;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Variantset getVariantset() {
		return this.variantset;
	}

	public void setVariantset(Variantset variantset) {
		this.variantset = variantset;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the organism database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Organism.findAll", query="SELECT o FROM Organism o"),
	@NamedQuery(name="Organism.findAllNames", query="SELECT o.commonName FROM Organism o order by o.commonName"),
	@NamedQuery(name="Organism.findOrganismById", query="SELECT o FROM Organism o where o.organismId= :organismId"),
	@NamedQuery(name="Organism.findOrganismByCommonName", query="SELECT o FROM Organism o where o.commonName= :commonName"),
	@NamedQuery(name="Organism.findOrganismByGenus", query="SELECT o FROM Organism o where o.genus= :genus"),
	@NamedQuery(name="Organism.findOrganismBySpecies", query="SELECT o FROM Organism o where o.species= :species"),
})

public class Organism implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="organism_id")
	private Integer organismId;

	private String abbreviation;

	private String comment;

	@Column(name="common_name")
	private String commonName;

	private String genus;

	private String species;

	//bi-directional many-to-one association to Feature
	@OneToMany(mappedBy="organism")
	private List<Feature> features;

	//bi-directional many-to-one association to IndelFeatureloc
	@OneToMany(mappedBy="organism")
	private List<IndelFeatureloc> indelFeaturelocs;

	//bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy="organism")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs;

	//bi-directional many-to-one association to OrganismDbxref
	@OneToMany(mappedBy="organism")
	private List<OrganismDbxref> organismDbxrefs;

	//bi-directional many-to-one association to Organismprop
	@OneToMany(mappedBy="organism")
	private List<Organismprop> organismprops;

	//bi-directional many-to-one association to SnpFeatureloc
	@OneToMany(mappedBy="organism")
	private List<SnpFeatureloc> snpFeaturelocs;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="organism")
	private List<Stock> stocks;

	public Organism() {
	}

	public Integer getOrganismId() {
		return this.organismId;
	}

	public void setOrganismId(Integer organismId) {
		this.organismId = organismId;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommonName() {
		return this.commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getGenus() {
		return this.genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public List<Feature> getFeatures() {
		return this.features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public Feature addFeature(Feature feature) {
		getFeatures().add(feature);
		feature.setOrganism(this);

		return feature;
	}

	public Feature removeFeature(Feature feature) {
		getFeatures().remove(feature);
		feature.setOrganism(null);

		return feature;
	}

	public List<IndelFeatureloc> getIndelFeaturelocs() {
		return this.indelFeaturelocs;
	}

	public void setIndelFeaturelocs(List<IndelFeatureloc> indelFeaturelocs) {
		this.indelFeaturelocs = indelFeaturelocs;
	}

	public IndelFeatureloc addIndelFeatureloc(IndelFeatureloc indelFeatureloc) {
		getIndelFeaturelocs().add(indelFeatureloc);
		indelFeatureloc.setOrganism(this);

		return indelFeatureloc;
	}

	public IndelFeatureloc removeIndelFeatureloc(IndelFeatureloc indelFeatureloc) {
		getIndelFeaturelocs().remove(indelFeatureloc);
		indelFeatureloc.setOrganism(null);

		return indelFeatureloc;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs() {
		return this.mvConvertposNb2allrefs;
	}

	public void setMvConvertposNb2allrefs(List<MvConvertposNb2allref> mvConvertposNb2allrefs) {
		this.mvConvertposNb2allrefs = mvConvertposNb2allrefs;
	}

	public MvConvertposNb2allref addMvConvertposNb2allref(MvConvertposNb2allref mvConvertposNb2allref) {
		getMvConvertposNb2allrefs().add(mvConvertposNb2allref);
		mvConvertposNb2allref.setOrganism(this);

		return mvConvertposNb2allref;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allref(MvConvertposNb2allref mvConvertposNb2allref) {
		getMvConvertposNb2allrefs().remove(mvConvertposNb2allref);
		mvConvertposNb2allref.setOrganism(null);

		return mvConvertposNb2allref;
	}

	public List<OrganismDbxref> getOrganismDbxrefs() {
		return this.organismDbxrefs;
	}

	public void setOrganismDbxrefs(List<OrganismDbxref> organismDbxrefs) {
		this.organismDbxrefs = organismDbxrefs;
	}

	public OrganismDbxref addOrganismDbxref(OrganismDbxref organismDbxref) {
		getOrganismDbxrefs().add(organismDbxref);
		organismDbxref.setOrganism(this);

		return organismDbxref;
	}

	public OrganismDbxref removeOrganismDbxref(OrganismDbxref organismDbxref) {
		getOrganismDbxrefs().remove(organismDbxref);
		organismDbxref.setOrganism(null);

		return organismDbxref;
	}

	public List<Organismprop> getOrganismprops() {
		return this.organismprops;
	}

	public void setOrganismprops(List<Organismprop> organismprops) {
		this.organismprops = organismprops;
	}

	public Organismprop addOrganismprop(Organismprop organismprop) {
		getOrganismprops().add(organismprop);
		organismprop.setOrganism(this);

		return organismprop;
	}

	public Organismprop removeOrganismprop(Organismprop organismprop) {
		getOrganismprops().remove(organismprop);
		organismprop.setOrganism(null);

		return organismprop;
	}

	public List<SnpFeatureloc> getSnpFeaturelocs() {
		return this.snpFeaturelocs;
	}

	public void setSnpFeaturelocs(List<SnpFeatureloc> snpFeaturelocs) {
		this.snpFeaturelocs = snpFeaturelocs;
	}

	public SnpFeatureloc addSnpFeatureloc(SnpFeatureloc snpFeatureloc) {
		getSnpFeaturelocs().add(snpFeatureloc);
		snpFeatureloc.setOrganism(this);

		return snpFeatureloc;
	}

	public SnpFeatureloc removeSnpFeatureloc(SnpFeatureloc snpFeatureloc) {
		getSnpFeaturelocs().remove(snpFeatureloc);
		snpFeatureloc.setOrganism(null);

		return snpFeatureloc;
	}

	public List<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public Stock addStock(Stock stock) {
		getStocks().add(stock);
		stock.setOrganism(this);

		return stock;
	}

	public Stock removeStock(Stock stock) {
		getStocks().remove(stock);
		stock.setOrganism(null);

		return stock;
	}

}
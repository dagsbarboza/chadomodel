package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pub database table.
 * 
 */
@Entity
@NamedQuery(name="Pub.findAll", query="SELECT p FROM Pub p")
public class Pub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pub_id")
	private Integer pubId;

	@Column(name="is_obsolete")
	private Boolean isObsolete;

	private String issue;

	private String miniref;

	private String pages;

	private String publisher;

	private String pubplace;

	private String pyear;

	@Column(name="series_name")
	private String seriesName;

	private String title;

	private String uniquename;

	private String volume;

	private String volumetitle;

	//bi-directional many-to-one association to FeatureCvterm
	@OneToMany(mappedBy="pub")
	private List<FeatureCvterm> featureCvterms;

	//bi-directional many-to-one association to FeatureCvtermPub
	@OneToMany(mappedBy="pub")
	private List<FeatureCvtermPub> featureCvtermPubs;

	//bi-directional many-to-one association to FeatureExpression
	@OneToMany(mappedBy="pub")
	private List<FeatureExpression> featureExpressions;

	//bi-directional many-to-one association to FeaturePub
	@OneToMany(mappedBy="pub")
	private List<FeaturePub> featurePubs;

	//bi-directional many-to-one association to FeatureRelationshipPub
	@OneToMany(mappedBy="pub")
	private List<FeatureRelationshipPub> featureRelationshipPubs;

	//bi-directional many-to-one association to FeatureRelationshippropPub
	@OneToMany(mappedBy="pub")
	private List<FeatureRelationshippropPub> featureRelationshippropPubs;

	//bi-directional many-to-one association to FeaturelocPub
	@OneToMany(mappedBy="pub")
	private List<FeaturelocPub> featurelocPubs;

	//bi-directional many-to-one association to FeaturepropPub
	@OneToMany(mappedBy="pub")
	private List<FeaturepropPub> featurepropPubs;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to PubDbxref
	@OneToMany(mappedBy="pub")
	private List<PubDbxref> pubDbxrefs;

	//bi-directional many-to-one association to PubRelationship
	@OneToMany(mappedBy="pub1")
	private List<PubRelationship> pubRelationships1;

	//bi-directional many-to-one association to PubRelationship
	@OneToMany(mappedBy="pub2")
	private List<PubRelationship> pubRelationships2;

	//bi-directional many-to-one association to Pubauthor
	@OneToMany(mappedBy="pub")
	private List<Pubauthor> pubauthors;

	//bi-directional many-to-one association to Pubprop
	@OneToMany(mappedBy="pub")
	private List<Pubprop> pubprops;

	//bi-directional many-to-one association to StockCvterm
	@OneToMany(mappedBy="pub")
	private List<StockCvterm> stockCvterms;

	//bi-directional many-to-one association to StockPub
	@OneToMany(mappedBy="pub")
	private List<StockPub> stockPubs;

	//bi-directional many-to-one association to StockRelationshipCvterm
	@OneToMany(mappedBy="pub")
	private List<StockRelationshipCvterm> stockRelationshipCvterms;

	//bi-directional many-to-one association to StockRelationshipPub
	@OneToMany(mappedBy="pub")
	private List<StockRelationshipPub> stockRelationshipPubs;

	//bi-directional many-to-one association to StockpropPub
	@OneToMany(mappedBy="pub")
	private List<StockpropPub> stockpropPubs;

	public Pub() {
	}

	public Integer getPubId() {
		return this.pubId;
	}

	public void setPubId(Integer pubId) {
		this.pubId = pubId;
	}

	public Boolean getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Boolean isObsolete) {
		this.isObsolete = isObsolete;
	}

	public String getIssue() {
		return this.issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getMiniref() {
		return this.miniref;
	}

	public void setMiniref(String miniref) {
		this.miniref = miniref;
	}

	public String getPages() {
		return this.pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public String getPublisher() {
		return this.publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPubplace() {
		return this.pubplace;
	}

	public void setPubplace(String pubplace) {
		this.pubplace = pubplace;
	}

	public String getPyear() {
		return this.pyear;
	}

	public void setPyear(String pyear) {
		this.pyear = pyear;
	}

	public String getSeriesName() {
		return this.seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUniquename() {
		return this.uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

	public String getVolume() {
		return this.volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getVolumetitle() {
		return this.volumetitle;
	}

	public void setVolumetitle(String volumetitle) {
		this.volumetitle = volumetitle;
	}

	public List<FeatureCvterm> getFeatureCvterms() {
		return this.featureCvterms;
	}

	public void setFeatureCvterms(List<FeatureCvterm> featureCvterms) {
		this.featureCvterms = featureCvterms;
	}

	public FeatureCvterm addFeatureCvterm(FeatureCvterm featureCvterm) {
		getFeatureCvterms().add(featureCvterm);
		featureCvterm.setPub(this);

		return featureCvterm;
	}

	public FeatureCvterm removeFeatureCvterm(FeatureCvterm featureCvterm) {
		getFeatureCvterms().remove(featureCvterm);
		featureCvterm.setPub(null);

		return featureCvterm;
	}

	public List<FeatureCvtermPub> getFeatureCvtermPubs() {
		return this.featureCvtermPubs;
	}

	public void setFeatureCvtermPubs(List<FeatureCvtermPub> featureCvtermPubs) {
		this.featureCvtermPubs = featureCvtermPubs;
	}

	public FeatureCvtermPub addFeatureCvtermPub(FeatureCvtermPub featureCvtermPub) {
		getFeatureCvtermPubs().add(featureCvtermPub);
		featureCvtermPub.setPub(this);

		return featureCvtermPub;
	}

	public FeatureCvtermPub removeFeatureCvtermPub(FeatureCvtermPub featureCvtermPub) {
		getFeatureCvtermPubs().remove(featureCvtermPub);
		featureCvtermPub.setPub(null);

		return featureCvtermPub;
	}

	public List<FeatureExpression> getFeatureExpressions() {
		return this.featureExpressions;
	}

	public void setFeatureExpressions(List<FeatureExpression> featureExpressions) {
		this.featureExpressions = featureExpressions;
	}

	public FeatureExpression addFeatureExpression(FeatureExpression featureExpression) {
		getFeatureExpressions().add(featureExpression);
		featureExpression.setPub(this);

		return featureExpression;
	}

	public FeatureExpression removeFeatureExpression(FeatureExpression featureExpression) {
		getFeatureExpressions().remove(featureExpression);
		featureExpression.setPub(null);

		return featureExpression;
	}

	public List<FeaturePub> getFeaturePubs() {
		return this.featurePubs;
	}

	public void setFeaturePubs(List<FeaturePub> featurePubs) {
		this.featurePubs = featurePubs;
	}

	public FeaturePub addFeaturePub(FeaturePub featurePub) {
		getFeaturePubs().add(featurePub);
		featurePub.setPub(this);

		return featurePub;
	}

	public FeaturePub removeFeaturePub(FeaturePub featurePub) {
		getFeaturePubs().remove(featurePub);
		featurePub.setPub(null);

		return featurePub;
	}

	public List<FeatureRelationshipPub> getFeatureRelationshipPubs() {
		return this.featureRelationshipPubs;
	}

	public void setFeatureRelationshipPubs(List<FeatureRelationshipPub> featureRelationshipPubs) {
		this.featureRelationshipPubs = featureRelationshipPubs;
	}

	public FeatureRelationshipPub addFeatureRelationshipPub(FeatureRelationshipPub featureRelationshipPub) {
		getFeatureRelationshipPubs().add(featureRelationshipPub);
		featureRelationshipPub.setPub(this);

		return featureRelationshipPub;
	}

	public FeatureRelationshipPub removeFeatureRelationshipPub(FeatureRelationshipPub featureRelationshipPub) {
		getFeatureRelationshipPubs().remove(featureRelationshipPub);
		featureRelationshipPub.setPub(null);

		return featureRelationshipPub;
	}

	public List<FeatureRelationshippropPub> getFeatureRelationshippropPubs() {
		return this.featureRelationshippropPubs;
	}

	public void setFeatureRelationshippropPubs(List<FeatureRelationshippropPub> featureRelationshippropPubs) {
		this.featureRelationshippropPubs = featureRelationshippropPubs;
	}

	public FeatureRelationshippropPub addFeatureRelationshippropPub(FeatureRelationshippropPub featureRelationshippropPub) {
		getFeatureRelationshippropPubs().add(featureRelationshippropPub);
		featureRelationshippropPub.setPub(this);

		return featureRelationshippropPub;
	}

	public FeatureRelationshippropPub removeFeatureRelationshippropPub(FeatureRelationshippropPub featureRelationshippropPub) {
		getFeatureRelationshippropPubs().remove(featureRelationshippropPub);
		featureRelationshippropPub.setPub(null);

		return featureRelationshippropPub;
	}

	public List<FeaturelocPub> getFeaturelocPubs() {
		return this.featurelocPubs;
	}

	public void setFeaturelocPubs(List<FeaturelocPub> featurelocPubs) {
		this.featurelocPubs = featurelocPubs;
	}

	public FeaturelocPub addFeaturelocPub(FeaturelocPub featurelocPub) {
		getFeaturelocPubs().add(featurelocPub);
		featurelocPub.setPub(this);

		return featurelocPub;
	}

	public FeaturelocPub removeFeaturelocPub(FeaturelocPub featurelocPub) {
		getFeaturelocPubs().remove(featurelocPub);
		featurelocPub.setPub(null);

		return featurelocPub;
	}

	public List<FeaturepropPub> getFeaturepropPubs() {
		return this.featurepropPubs;
	}

	public void setFeaturepropPubs(List<FeaturepropPub> featurepropPubs) {
		this.featurepropPubs = featurepropPubs;
	}

	public FeaturepropPub addFeaturepropPub(FeaturepropPub featurepropPub) {
		getFeaturepropPubs().add(featurepropPub);
		featurepropPub.setPub(this);

		return featurepropPub;
	}

	public FeaturepropPub removeFeaturepropPub(FeaturepropPub featurepropPub) {
		getFeaturepropPubs().remove(featurepropPub);
		featurepropPub.setPub(null);

		return featurepropPub;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public List<PubDbxref> getPubDbxrefs() {
		return this.pubDbxrefs;
	}

	public void setPubDbxrefs(List<PubDbxref> pubDbxrefs) {
		this.pubDbxrefs = pubDbxrefs;
	}

	public PubDbxref addPubDbxref(PubDbxref pubDbxref) {
		getPubDbxrefs().add(pubDbxref);
		pubDbxref.setPub(this);

		return pubDbxref;
	}

	public PubDbxref removePubDbxref(PubDbxref pubDbxref) {
		getPubDbxrefs().remove(pubDbxref);
		pubDbxref.setPub(null);

		return pubDbxref;
	}

	public List<PubRelationship> getPubRelationships1() {
		return this.pubRelationships1;
	}

	public void setPubRelationships1(List<PubRelationship> pubRelationships1) {
		this.pubRelationships1 = pubRelationships1;
	}

	public PubRelationship addPubRelationships1(PubRelationship pubRelationships1) {
		getPubRelationships1().add(pubRelationships1);
		pubRelationships1.setPub1(this);

		return pubRelationships1;
	}

	public PubRelationship removePubRelationships1(PubRelationship pubRelationships1) {
		getPubRelationships1().remove(pubRelationships1);
		pubRelationships1.setPub1(null);

		return pubRelationships1;
	}

	public List<PubRelationship> getPubRelationships2() {
		return this.pubRelationships2;
	}

	public void setPubRelationships2(List<PubRelationship> pubRelationships2) {
		this.pubRelationships2 = pubRelationships2;
	}

	public PubRelationship addPubRelationships2(PubRelationship pubRelationships2) {
		getPubRelationships2().add(pubRelationships2);
		pubRelationships2.setPub2(this);

		return pubRelationships2;
	}

	public PubRelationship removePubRelationships2(PubRelationship pubRelationships2) {
		getPubRelationships2().remove(pubRelationships2);
		pubRelationships2.setPub2(null);

		return pubRelationships2;
	}

	public List<Pubauthor> getPubauthors() {
		return this.pubauthors;
	}

	public void setPubauthors(List<Pubauthor> pubauthors) {
		this.pubauthors = pubauthors;
	}

	public Pubauthor addPubauthor(Pubauthor pubauthor) {
		getPubauthors().add(pubauthor);
		pubauthor.setPub(this);

		return pubauthor;
	}

	public Pubauthor removePubauthor(Pubauthor pubauthor) {
		getPubauthors().remove(pubauthor);
		pubauthor.setPub(null);

		return pubauthor;
	}

	public List<Pubprop> getPubprops() {
		return this.pubprops;
	}

	public void setPubprops(List<Pubprop> pubprops) {
		this.pubprops = pubprops;
	}

	public Pubprop addPubprop(Pubprop pubprop) {
		getPubprops().add(pubprop);
		pubprop.setPub(this);

		return pubprop;
	}

	public Pubprop removePubprop(Pubprop pubprop) {
		getPubprops().remove(pubprop);
		pubprop.setPub(null);

		return pubprop;
	}

	public List<StockCvterm> getStockCvterms() {
		return this.stockCvterms;
	}

	public void setStockCvterms(List<StockCvterm> stockCvterms) {
		this.stockCvterms = stockCvterms;
	}

	public StockCvterm addStockCvterm(StockCvterm stockCvterm) {
		getStockCvterms().add(stockCvterm);
		stockCvterm.setPub(this);

		return stockCvterm;
	}

	public StockCvterm removeStockCvterm(StockCvterm stockCvterm) {
		getStockCvterms().remove(stockCvterm);
		stockCvterm.setPub(null);

		return stockCvterm;
	}

	public List<StockPub> getStockPubs() {
		return this.stockPubs;
	}

	public void setStockPubs(List<StockPub> stockPubs) {
		this.stockPubs = stockPubs;
	}

	public StockPub addStockPub(StockPub stockPub) {
		getStockPubs().add(stockPub);
		stockPub.setPub(this);

		return stockPub;
	}

	public StockPub removeStockPub(StockPub stockPub) {
		getStockPubs().remove(stockPub);
		stockPub.setPub(null);

		return stockPub;
	}

	public List<StockRelationshipCvterm> getStockRelationshipCvterms() {
		return this.stockRelationshipCvterms;
	}

	public void setStockRelationshipCvterms(List<StockRelationshipCvterm> stockRelationshipCvterms) {
		this.stockRelationshipCvterms = stockRelationshipCvterms;
	}

	public StockRelationshipCvterm addStockRelationshipCvterm(StockRelationshipCvterm stockRelationshipCvterm) {
		getStockRelationshipCvterms().add(stockRelationshipCvterm);
		stockRelationshipCvterm.setPub(this);

		return stockRelationshipCvterm;
	}

	public StockRelationshipCvterm removeStockRelationshipCvterm(StockRelationshipCvterm stockRelationshipCvterm) {
		getStockRelationshipCvterms().remove(stockRelationshipCvterm);
		stockRelationshipCvterm.setPub(null);

		return stockRelationshipCvterm;
	}

	public List<StockRelationshipPub> getStockRelationshipPubs() {
		return this.stockRelationshipPubs;
	}

	public void setStockRelationshipPubs(List<StockRelationshipPub> stockRelationshipPubs) {
		this.stockRelationshipPubs = stockRelationshipPubs;
	}

	public StockRelationshipPub addStockRelationshipPub(StockRelationshipPub stockRelationshipPub) {
		getStockRelationshipPubs().add(stockRelationshipPub);
		stockRelationshipPub.setPub(this);

		return stockRelationshipPub;
	}

	public StockRelationshipPub removeStockRelationshipPub(StockRelationshipPub stockRelationshipPub) {
		getStockRelationshipPubs().remove(stockRelationshipPub);
		stockRelationshipPub.setPub(null);

		return stockRelationshipPub;
	}

	public List<StockpropPub> getStockpropPubs() {
		return this.stockpropPubs;
	}

	public void setStockpropPubs(List<StockpropPub> stockpropPubs) {
		this.stockpropPubs = stockpropPubs;
	}

	public StockpropPub addStockpropPub(StockpropPub stockpropPub) {
		getStockpropPubs().add(stockpropPub);
		stockpropPub.setPub(this);

		return stockpropPub;
	}

	public StockpropPub removeStockpropPub(StockpropPub stockpropPub) {
		getStockpropPubs().remove(stockpropPub);
		stockpropPub.setPub(null);

		return stockpropPub;
	}

}
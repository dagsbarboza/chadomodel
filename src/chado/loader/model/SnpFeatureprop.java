package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the snp_featureprop database table.
 * 
 */
@Entity
@Table(name="snp_featureprop")
@NamedQueries({
	@NamedQuery(name="SnpFeatureprop.findAll", query="SELECT s FROM SnpFeatureprop s"),
	@NamedQuery(name="SnpFeatureprop.findBysnpFeaturePropId", query="SELECT s FROM SnpFeatureprop s where s.snpFeaturepropId=:snpFeaturepropId")
})

public class SnpFeatureprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="snp_featureprop_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer snpFeaturepropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to SnpFeature
	@ManyToOne
	@JoinColumn(name="snp_feature_id")
	private SnpFeature snpFeature;

	public SnpFeatureprop() {
		setRank(0);
	}

	public Integer getSnpFeaturepropId() {
		return this.snpFeaturepropId;
	}

	public void setSnpFeaturepropId(Integer snpFeaturepropId) {
		this.snpFeaturepropId = snpFeaturepropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public SnpFeature getSnpFeature() {
		return this.snpFeature;
	}

	public void setSnpFeature(SnpFeature snpFeature) {
		this.snpFeature = snpFeature;
	}

}
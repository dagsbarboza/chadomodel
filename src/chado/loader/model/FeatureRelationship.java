package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the feature_relationship database table.
 * 
 */
@Entity
@Table(name="feature_relationship")
@NamedQuery(name="FeatureRelationship.findAll", query="SELECT f FROM FeatureRelationship f")
public class FeatureRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_relationship_id")
	private Integer featureRelationshipId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="object_id")
	private Feature feature1;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Feature feature2;

	//bi-directional many-to-one association to FeatureRelationshipPub
	@OneToMany(mappedBy="featureRelationship")
	private List<FeatureRelationshipPub> featureRelationshipPubs;

	//bi-directional many-to-one association to FeatureRelationshipprop
	@OneToMany(mappedBy="featureRelationship")
	private List<FeatureRelationshipprop> featureRelationshipprops;

	public FeatureRelationship() {
	}

	public Integer getFeatureRelationshipId() {
		return this.featureRelationshipId;
	}

	public void setFeatureRelationshipId(Integer featureRelationshipId) {
		this.featureRelationshipId = featureRelationshipId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Feature getFeature1() {
		return this.feature1;
	}

	public void setFeature1(Feature feature1) {
		this.feature1 = feature1;
	}

	public Feature getFeature2() {
		return this.feature2;
	}

	public void setFeature2(Feature feature2) {
		this.feature2 = feature2;
	}

	public List<FeatureRelationshipPub> getFeatureRelationshipPubs() {
		return this.featureRelationshipPubs;
	}

	public void setFeatureRelationshipPubs(List<FeatureRelationshipPub> featureRelationshipPubs) {
		this.featureRelationshipPubs = featureRelationshipPubs;
	}

	public FeatureRelationshipPub addFeatureRelationshipPub(FeatureRelationshipPub featureRelationshipPub) {
		getFeatureRelationshipPubs().add(featureRelationshipPub);
		featureRelationshipPub.setFeatureRelationship(this);

		return featureRelationshipPub;
	}

	public FeatureRelationshipPub removeFeatureRelationshipPub(FeatureRelationshipPub featureRelationshipPub) {
		getFeatureRelationshipPubs().remove(featureRelationshipPub);
		featureRelationshipPub.setFeatureRelationship(null);

		return featureRelationshipPub;
	}

	public List<FeatureRelationshipprop> getFeatureRelationshipprops() {
		return this.featureRelationshipprops;
	}

	public void setFeatureRelationshipprops(List<FeatureRelationshipprop> featureRelationshipprops) {
		this.featureRelationshipprops = featureRelationshipprops;
	}

	public FeatureRelationshipprop addFeatureRelationshipprop(FeatureRelationshipprop featureRelationshipprop) {
		getFeatureRelationshipprops().add(featureRelationshipprop);
		featureRelationshipprop.setFeatureRelationship(this);

		return featureRelationshipprop;
	}

	public FeatureRelationshipprop removeFeatureRelationshipprop(FeatureRelationshipprop featureRelationshipprop) {
		getFeatureRelationshipprops().remove(featureRelationshipprop);
		featureRelationshipprop.setFeatureRelationship(null);

		return featureRelationshipprop;
	}

}
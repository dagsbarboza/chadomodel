package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stock_cvterm database table.
 * 
 */
@Entity
@Table(name="stock_cvterm")
@NamedQuery(name="StockCvterm.findAll", query="SELECT s FROM StockCvterm s")
public class StockCvterm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_cvterm_id")
	private Integer stockCvtermId;

	@Column(name="is_not")
	private Boolean isNot;

	private Integer rank;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	//bi-directional many-to-one association to StockCvtermprop
	@OneToMany(mappedBy="stockCvterm")
	private List<StockCvtermprop> stockCvtermprops;

	public StockCvterm() {
	}

	public Integer getStockCvtermId() {
		return this.stockCvtermId;
	}

	public void setStockCvtermId(Integer stockCvtermId) {
		this.stockCvtermId = stockCvtermId;
	}

	public Boolean getIsNot() {
		return this.isNot;
	}

	public void setIsNot(Boolean isNot) {
		this.isNot = isNot;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public List<StockCvtermprop> getStockCvtermprops() {
		return this.stockCvtermprops;
	}

	public void setStockCvtermprops(List<StockCvtermprop> stockCvtermprops) {
		this.stockCvtermprops = stockCvtermprops;
	}

	public StockCvtermprop addStockCvtermprop(StockCvtermprop stockCvtermprop) {
		getStockCvtermprops().add(stockCvtermprop);
		stockCvtermprop.setStockCvterm(this);

		return stockCvtermprop;
	}

	public StockCvtermprop removeStockCvtermprop(StockCvtermprop stockCvtermprop) {
		getStockCvtermprops().remove(stockCvtermprop);
		stockCvtermprop.setStockCvterm(null);

		return stockCvtermprop;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the gff_meta database table.
 * 
 */
@Entity
@Table(name="gff_meta")
@NamedQuery(name="GffMeta.findAll", query="SELECT g FROM GffMeta g")
public class GffMeta implements Serializable {
	private static final long serialVersionUID = 1L;

	private String hostname;

	@Id
	private String name;

	private Timestamp starttime;

	public GffMeta() {
	}

	public String getHostname() {
		return this.hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

}
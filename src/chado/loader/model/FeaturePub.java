package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the feature_pub database table.
 * 
 */
@Entity
@Table(name="feature_pub")
@NamedQuery(name="FeaturePub.findAll", query="SELECT f FROM FeaturePub f")
public class FeaturePub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_pub_id")
	private Integer featurePubId;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to FeaturePubprop
	@OneToMany(mappedBy="featurePub")
	private List<FeaturePubprop> featurePubprops;

	public FeaturePub() {
	}

	public Integer getFeaturePubId() {
		return this.featurePubId;
	}

	public void setFeaturePubId(Integer featurePubId) {
		this.featurePubId = featurePubId;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public List<FeaturePubprop> getFeaturePubprops() {
		return this.featurePubprops;
	}

	public void setFeaturePubprops(List<FeaturePubprop> featurePubprops) {
		this.featurePubprops = featurePubprops;
	}

	public FeaturePubprop addFeaturePubprop(FeaturePubprop featurePubprop) {
		getFeaturePubprops().add(featurePubprop);
		featurePubprop.setFeaturePub(this);

		return featurePubprop;
	}

	public FeaturePubprop removeFeaturePubprop(FeaturePubprop featurePubprop) {
		getFeaturePubprops().remove(featurePubprop);
		featurePubprop.setFeaturePub(null);

		return featurePubprop;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the analysisfeature database table.
 * 
 */
@Entity
@NamedQuery(name="Analysisfeature.findAll", query="SELECT a FROM Analysisfeature a")
public class Analysisfeature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="analysisfeature_id")
	private Integer analysisfeatureId;

	private double identity;

	private double normscore;

	private double rawscore;

	private double significance;

	//bi-directional many-to-one association to Analysi
	@ManyToOne
	@JoinColumn(name="analysis_id")
	private Analysi analysi;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	public Analysisfeature() {
	}

	public Integer getAnalysisfeatureId() {
		return this.analysisfeatureId;
	}

	public void setAnalysisfeatureId(Integer analysisfeatureId) {
		this.analysisfeatureId = analysisfeatureId;
	}

	public double getIdentity() {
		return this.identity;
	}

	public void setIdentity(double identity) {
		this.identity = identity;
	}

	public double getNormscore() {
		return this.normscore;
	}

	public void setNormscore(double normscore) {
		this.normscore = normscore;
	}

	public double getRawscore() {
		return this.rawscore;
	}

	public void setRawscore(double rawscore) {
		this.rawscore = rawscore;
	}

	public double getSignificance() {
		return this.significance;
	}

	public void setSignificance(double significance) {
		this.significance = significance;
	}

	public Analysi getAnalysi() {
		return this.analysi;
	}

	public void setAnalysi(Analysi analysi) {
		this.analysi = analysi;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the stock_sample database table.
 * 
 */
@Entity
@Table(name = "stock_sample")
@NamedQueries({ @NamedQuery(name = "StockSample.findAll", query = "SELECT s FROM StockSample s"),
		@NamedQuery(name = "StockSample.findById", query = "SELECT s FROM StockSample s where s.stockSampleId = :stockSampleId") 
})
public class StockSample implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "stock_sample_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer stockSampleId;

	@Column(name = "hdf5_index")
	private Integer hdf5Index;

	@Column(name = "tmp_oldstock_id")
	private Integer tmpOldstockId;

	// bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name = "dbxref_id")
	private Dbxref dbxref;

	// bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name = "stock_id")
	private Stock stock;

	public StockSample() {
	}

	public Integer getStockSampleId() {
		return this.stockSampleId;
	}

	public void setStockSampleId(Integer stockSampleId) {
		this.stockSampleId = stockSampleId;
	}

	public Integer getHdf5Index() {
		return this.hdf5Index;
	}

	public void setHdf5Index(Integer hdf5Index) {
		this.hdf5Index = hdf5Index;
	}

	public Integer getTmpOldstockId() {
		return this.tmpOldstockId;
	}

	public void setTmpOldstockId(Integer tmpOldstockId) {
		this.tmpOldstockId = tmpOldstockId;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
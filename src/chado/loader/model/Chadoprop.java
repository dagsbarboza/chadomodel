package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the chadoprop database table.
 * 
 */
@Entity
@NamedQuery(name="Chadoprop.findAll", query="SELECT c FROM Chadoprop c")
public class Chadoprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="chadoprop_id")
	private Integer chadopropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	public Chadoprop() {
	}

	public Integer getChadopropId() {
		return this.chadopropId;
	}

	public void setChadopropId(Integer chadopropId) {
		this.chadopropId = chadopropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

}
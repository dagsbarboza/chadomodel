package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the gwas_trait database table.
 * 
 */
@Entity
@Table(name="gwas_trait")
@NamedQuery(name="GwasTrait.findAll", query="SELECT g FROM GwasTrait g")
public class GwasTrait implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="gwas_trait_id")
	private Long gwasTraitId;

	private String definition;

	private String name;

	//bi-directional many-to-one association to GwasRun
	@OneToMany(mappedBy="gwasTrait")
	private List<GwasRun> gwasRuns;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="phenotype_id")
	private Cvterm cvterm;

	public GwasTrait() {
	}

	public Long getGwasTraitId() {
		return this.gwasTraitId;
	}

	public void setGwasTraitId(Long gwasTraitId) {
		this.gwasTraitId = gwasTraitId;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<GwasRun> getGwasRuns() {
		return this.gwasRuns;
	}

	public void setGwasRuns(List<GwasRun> gwasRuns) {
		this.gwasRuns = gwasRuns;
	}

	public GwasRun addGwasRun(GwasRun gwasRun) {
		getGwasRuns().add(gwasRun);
		gwasRun.setGwasTrait(this);

		return gwasRun;
	}

	public GwasRun removeGwasRun(GwasRun gwasRun) {
		getGwasRuns().remove(gwasRun);
		gwasRun.setGwasTrait(null);

		return gwasRun;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

}
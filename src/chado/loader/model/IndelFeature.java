package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the indel_feature database table.
 * 
 */
@Entity
@Table(name="indel_feature")
@NamedQuery(name="IndelFeature.findAll", query="SELECT i FROM IndelFeature i")
public class IndelFeature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="indel_feature_id")
	private Integer indelFeatureId;

	//bi-directional many-to-one association to IndelFeatureloc
	@OneToMany(mappedBy="indelFeature")
	private List<IndelFeatureloc> indelFeaturelocs;

	public IndelFeature() {
	}

	public Integer getIndelFeatureId() {
		return this.indelFeatureId;
	}

	public void setIndelFeatureId(Integer indelFeatureId) {
		this.indelFeatureId = indelFeatureId;
	}

	public List<IndelFeatureloc> getIndelFeaturelocs() {
		return this.indelFeaturelocs;
	}

	public void setIndelFeaturelocs(List<IndelFeatureloc> indelFeaturelocs) {
		this.indelFeaturelocs = indelFeaturelocs;
	}

	public IndelFeatureloc addIndelFeatureloc(IndelFeatureloc indelFeatureloc) {
		getIndelFeaturelocs().add(indelFeatureloc);
		indelFeatureloc.setIndelFeature(this);

		return indelFeatureloc;
	}

	public IndelFeatureloc removeIndelFeatureloc(IndelFeatureloc indelFeatureloc) {
		getIndelFeaturelocs().remove(indelFeatureloc);
		indelFeatureloc.setIndelFeature(null);

		return indelFeatureloc;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the dbxref database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Dbxref.findAll", query="SELECT d FROM Dbxref d"),
	@NamedQuery(name="Dbxref.findAllAccession", query="SELECT d.accession FROM Dbxref d"),
	@NamedQuery(name="Dbxref.findByAccession", query="SELECT d FROM Dbxref d where d.accession= :accession"),
	@NamedQuery(name="Dbxref.findByAccessionAndDb", query="SELECT d FROM Dbxref d where d.accession= :accession and d.db=:db"),
	@NamedQuery(name="Dbxref.findByDbXrefIdId", query="SELECT d FROM Dbxref d where d.dbxrefId= :dbxrefId")
})

public class Dbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="dbxref_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbxrefId;

	private String accession;

	private String description;

	private String version;

	//bi-directional many-to-one association to Cvterm
	@OneToMany(mappedBy="dbxref")
	private List<Cvterm> cvterms;

	//bi-directional many-to-one association to CvtermDbxref
	@OneToMany(mappedBy="dbxref")
	private List<CvtermDbxref> cvtermDbxrefs;

	//bi-directional many-to-one association to Db
	@ManyToOne
	@JoinColumn(name="db_id")
	private Db db;

	//bi-directional many-to-one association to Dbxrefprop
	@OneToMany(mappedBy="dbxref")
	private List<Dbxrefprop> dbxrefprops;

	//bi-directional many-to-one association to Feature
	@OneToMany(mappedBy="dbxref")
	private List<Feature> features;

	//bi-directional many-to-one association to FeatureCvtermDbxref
	@OneToMany(mappedBy="dbxref")
	private List<FeatureCvtermDbxref> featureCvtermDbxrefs;

	//bi-directional many-to-one association to FeatureDbxref
	@OneToMany(mappedBy="dbxref")
	private List<FeatureDbxref> featureDbxrefs;

	//bi-directional many-to-one association to OrganismDbxref
	@OneToMany(mappedBy="dbxref")
	private List<OrganismDbxref> organismDbxrefs;

	//bi-directional many-to-one association to PubDbxref
	@OneToMany(mappedBy="dbxref")
	private List<PubDbxref> pubDbxrefs;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="dbxref")
	private List<Stock> stocks;

	//bi-directional many-to-one association to StockDbxref
	@OneToMany(mappedBy="dbxref")
	private List<StockDbxref> stockDbxrefs;

	//bi-directional many-to-one association to StockPhenotype
	@OneToMany(mappedBy="dbxref")
	private List<StockPhenotype> stockPhenotypes;

	//bi-directional many-to-one association to StockSample
	@OneToMany(mappedBy="dbxref")
	private List<StockSample> stockSamples;

	public Dbxref() {
	}

	public Integer getDbxrefId() {
		return this.dbxrefId;
	}

	public void setDbxrefId(Integer dbxrefId) {
		this.dbxrefId = dbxrefId;
	}

	public String getAccession() {
		return this.accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Cvterm> getCvterms() {
		return this.cvterms;
	}

	public void setCvterms(List<Cvterm> cvterms) {
		this.cvterms = cvterms;
	}

	public Cvterm addCvterm(Cvterm cvterm) {
		getCvterms().add(cvterm);
		cvterm.setDbxref(this);

		return cvterm;
	}

	public Cvterm removeCvterm(Cvterm cvterm) {
		getCvterms().remove(cvterm);
		cvterm.setDbxref(null);

		return cvterm;
	}

	public List<CvtermDbxref> getCvtermDbxrefs() {
		return this.cvtermDbxrefs;
	}

	public void setCvtermDbxrefs(List<CvtermDbxref> cvtermDbxrefs) {
		this.cvtermDbxrefs = cvtermDbxrefs;
	}

	public CvtermDbxref addCvtermDbxref(CvtermDbxref cvtermDbxref) {
		getCvtermDbxrefs().add(cvtermDbxref);
		cvtermDbxref.setDbxref(this);

		return cvtermDbxref;
	}

	public CvtermDbxref removeCvtermDbxref(CvtermDbxref cvtermDbxref) {
		getCvtermDbxrefs().remove(cvtermDbxref);
		cvtermDbxref.setDbxref(null);

		return cvtermDbxref;
	}

	public Db getDb() {
		return this.db;
	}

	public void setDb(Db db) {
		this.db = db;
	}

	public List<Dbxrefprop> getDbxrefprops() {
		return this.dbxrefprops;
	}

	public void setDbxrefprops(List<Dbxrefprop> dbxrefprops) {
		this.dbxrefprops = dbxrefprops;
	}

	public Dbxrefprop addDbxrefprop(Dbxrefprop dbxrefprop) {
		getDbxrefprops().add(dbxrefprop);
		dbxrefprop.setDbxref(this);

		return dbxrefprop;
	}

	public Dbxrefprop removeDbxrefprop(Dbxrefprop dbxrefprop) {
		getDbxrefprops().remove(dbxrefprop);
		dbxrefprop.setDbxref(null);

		return dbxrefprop;
	}

	public List<Feature> getFeatures() {
		return this.features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public Feature addFeature(Feature feature) {
		getFeatures().add(feature);
		feature.setDbxref(this);

		return feature;
	}

	public Feature removeFeature(Feature feature) {
		getFeatures().remove(feature);
		feature.setDbxref(null);

		return feature;
	}

	public List<FeatureCvtermDbxref> getFeatureCvtermDbxrefs() {
		return this.featureCvtermDbxrefs;
	}

	public void setFeatureCvtermDbxrefs(List<FeatureCvtermDbxref> featureCvtermDbxrefs) {
		this.featureCvtermDbxrefs = featureCvtermDbxrefs;
	}

	public FeatureCvtermDbxref addFeatureCvtermDbxref(FeatureCvtermDbxref featureCvtermDbxref) {
		getFeatureCvtermDbxrefs().add(featureCvtermDbxref);
		featureCvtermDbxref.setDbxref(this);

		return featureCvtermDbxref;
	}

	public FeatureCvtermDbxref removeFeatureCvtermDbxref(FeatureCvtermDbxref featureCvtermDbxref) {
		getFeatureCvtermDbxrefs().remove(featureCvtermDbxref);
		featureCvtermDbxref.setDbxref(null);

		return featureCvtermDbxref;
	}

	public List<FeatureDbxref> getFeatureDbxrefs() {
		return this.featureDbxrefs;
	}

	public void setFeatureDbxrefs(List<FeatureDbxref> featureDbxrefs) {
		this.featureDbxrefs = featureDbxrefs;
	}

	public FeatureDbxref addFeatureDbxref(FeatureDbxref featureDbxref) {
		getFeatureDbxrefs().add(featureDbxref);
		featureDbxref.setDbxref(this);

		return featureDbxref;
	}

	public FeatureDbxref removeFeatureDbxref(FeatureDbxref featureDbxref) {
		getFeatureDbxrefs().remove(featureDbxref);
		featureDbxref.setDbxref(null);

		return featureDbxref;
	}

	public List<OrganismDbxref> getOrganismDbxrefs() {
		return this.organismDbxrefs;
	}

	public void setOrganismDbxrefs(List<OrganismDbxref> organismDbxrefs) {
		this.organismDbxrefs = organismDbxrefs;
	}

	public OrganismDbxref addOrganismDbxref(OrganismDbxref organismDbxref) {
		getOrganismDbxrefs().add(organismDbxref);
		organismDbxref.setDbxref(this);

		return organismDbxref;
	}

	public OrganismDbxref removeOrganismDbxref(OrganismDbxref organismDbxref) {
		getOrganismDbxrefs().remove(organismDbxref);
		organismDbxref.setDbxref(null);

		return organismDbxref;
	}

	public List<PubDbxref> getPubDbxrefs() {
		return this.pubDbxrefs;
	}

	public void setPubDbxrefs(List<PubDbxref> pubDbxrefs) {
		this.pubDbxrefs = pubDbxrefs;
	}

	public PubDbxref addPubDbxref(PubDbxref pubDbxref) {
		getPubDbxrefs().add(pubDbxref);
		pubDbxref.setDbxref(this);

		return pubDbxref;
	}

	public PubDbxref removePubDbxref(PubDbxref pubDbxref) {
		getPubDbxrefs().remove(pubDbxref);
		pubDbxref.setDbxref(null);

		return pubDbxref;
	}

	public List<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public Stock addStock(Stock stock) {
		getStocks().add(stock);
		stock.setDbxref(this);

		return stock;
	}

	public Stock removeStock(Stock stock) {
		getStocks().remove(stock);
		stock.setDbxref(null);

		return stock;
	}

	public List<StockDbxref> getStockDbxrefs() {
		return this.stockDbxrefs;
	}

	public void setStockDbxrefs(List<StockDbxref> stockDbxrefs) {
		this.stockDbxrefs = stockDbxrefs;
	}

	public StockDbxref addStockDbxref(StockDbxref stockDbxref) {
		getStockDbxrefs().add(stockDbxref);
		stockDbxref.setDbxref(this);

		return stockDbxref;
	}

	public StockDbxref removeStockDbxref(StockDbxref stockDbxref) {
		getStockDbxrefs().remove(stockDbxref);
		stockDbxref.setDbxref(null);

		return stockDbxref;
	}

	public List<StockPhenotype> getStockPhenotypes() {
		return this.stockPhenotypes;
	}

	public void setStockPhenotypes(List<StockPhenotype> stockPhenotypes) {
		this.stockPhenotypes = stockPhenotypes;
	}

	public StockPhenotype addStockPhenotype(StockPhenotype stockPhenotype) {
		getStockPhenotypes().add(stockPhenotype);
		stockPhenotype.setDbxref(this);

		return stockPhenotype;
	}

	public StockPhenotype removeStockPhenotype(StockPhenotype stockPhenotype) {
		getStockPhenotypes().remove(stockPhenotype);
		stockPhenotype.setDbxref(null);

		return stockPhenotype;
	}

	public List<StockSample> getStockSamples() {
		return this.stockSamples;
	}

	public void setStockSamples(List<StockSample> stockSamples) {
		this.stockSamples = stockSamples;
	}

	public StockSample addStockSample(StockSample stockSample) {
		getStockSamples().add(stockSample);
		stockSample.setDbxref(this);

		return stockSample;
	}

	public StockSample removeStockSample(StockSample stockSample) {
		getStockSamples().remove(stockSample);
		stockSample.setDbxref(null);

		return stockSample;
	}

}
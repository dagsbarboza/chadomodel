package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_relationshipprop_pub database table.
 * 
 */
@Entity
@Table(name="feature_relationshipprop_pub")
@NamedQuery(name="FeatureRelationshippropPub.findAll", query="SELECT f FROM FeatureRelationshippropPub f")
public class FeatureRelationshippropPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_relationshipprop_pub_id")
	private Integer featureRelationshippropPubId;

	//bi-directional many-to-one association to FeatureRelationshipprop
	@ManyToOne
	@JoinColumn(name="feature_relationshipprop_id")
	private FeatureRelationshipprop featureRelationshipprop;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public FeatureRelationshippropPub() {
	}

	public Integer getFeatureRelationshippropPubId() {
		return this.featureRelationshippropPubId;
	}

	public void setFeatureRelationshippropPubId(Integer featureRelationshippropPubId) {
		this.featureRelationshippropPubId = featureRelationshippropPubId;
	}

	public FeatureRelationshipprop getFeatureRelationshipprop() {
		return this.featureRelationshipprop;
	}

	public void setFeatureRelationshipprop(FeatureRelationshipprop featureRelationshipprop) {
		this.featureRelationshipprop = featureRelationshipprop;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the gene database table.
 * 
 */
@Entity
@NamedQuery(name="Gene.findAll", query="SELECT g FROM Gene g")
public class Gene implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="dbxref_id")
	private Integer dbxrefId;

	@Column(name="feature_id")
	private Integer featureId;

	@Id
	@Column(name="gene_id")
	private Integer geneId;

	@Column(name="is_analysis")
	private Boolean isAnalysis;

	@Column(name="is_obsolete")
	private Boolean isObsolete;

	private String md5checksum;

	private String name;

	@Column(name="organism_id")
	private Integer organismId;

	private String residues;

	private Integer seqlen;

	private Timestamp timeaccessioned;

	private Timestamp timelastmodified;

	@Column(name="type_id")
	private Integer typeId;

	private String uniquename;

	public Gene() {
	}

	public Integer getDbxrefId() {
		return this.dbxrefId;
	}

	public void setDbxrefId(Integer dbxrefId) {
		this.dbxrefId = dbxrefId;
	}

	public Integer getFeatureId() {
		return this.featureId;
	}

	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}

	public Integer getGeneId() {
		return this.geneId;
	}

	public void setGeneId(Integer geneId) {
		this.geneId = geneId;
	}

	public Boolean getIsAnalysis() {
		return this.isAnalysis;
	}

	public void setIsAnalysis(Boolean isAnalysis) {
		this.isAnalysis = isAnalysis;
	}

	public Boolean getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Boolean isObsolete) {
		this.isObsolete = isObsolete;
	}

	public String getMd5checksum() {
		return this.md5checksum;
	}

	public void setMd5checksum(String md5checksum) {
		this.md5checksum = md5checksum;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrganismId() {
		return this.organismId;
	}

	public void setOrganismId(Integer organismId) {
		this.organismId = organismId;
	}

	public String getResidues() {
		return this.residues;
	}

	public void setResidues(String residues) {
		this.residues = residues;
	}

	public Integer getSeqlen() {
		return this.seqlen;
	}

	public void setSeqlen(Integer seqlen) {
		this.seqlen = seqlen;
	}

	public Timestamp getTimeaccessioned() {
		return this.timeaccessioned;
	}

	public void setTimeaccessioned(Timestamp timeaccessioned) {
		this.timeaccessioned = timeaccessioned;
	}

	public Timestamp getTimelastmodified() {
		return this.timelastmodified;
	}

	public void setTimelastmodified(Timestamp timelastmodified) {
		this.timelastmodified = timelastmodified;
	}

	public Integer getTypeId() {
		return this.typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getUniquename() {
		return this.uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

}
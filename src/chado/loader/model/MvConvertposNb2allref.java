package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mv_convertpos_nb2allrefs database table.
 * 
 */
@Entity
@Table(name="mv_convertpos_nb2allrefs")
@NamedQuery(name="MvConvertposNb2allref.findAll", query="SELECT m FROM MvConvertposNb2allref m")
public class MvConvertposNb2allref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mv_convertpos_nb2allrefs_id")
	private Integer mvConvertposNb2allrefsId;

	@Column(name="dj123_align_count")
	private Integer dj123AlignCount;

	@Column(name="dj123_position")
	private Integer dj123Position;

	@Column(name="dj123_refcall")
	private String dj123Refcall;

	@Column(name="from_position")
	private Integer fromPosition;

	@Column(name="from_refcall")
	private String fromRefcall;

	@Column(name="ir64_align_count")
	private Integer ir64AlignCount;

	@Column(name="ir64_position")
	private Integer ir64Position;

	@Column(name="ir64_refcall")
	private String ir64Refcall;

	@Column(name="kasalath_align_count")
	private Integer kasalathAlignCount;

	@Column(name="kasalath_position")
	private Integer kasalathPosition;

	@Column(name="kasalath_refcall")
	private String kasalathRefcall;

	@Column(name="nb_align_count")
	private Integer nbAlignCount;

	@Column(name="nb_position")
	private Integer nbPosition;

	@Column(name="nb_refcall")
	private String nbRefcall;

	@Column(name="rice9311_align_count")
	private Integer rice9311AlignCount;

	@Column(name="rice9311_position")
	private Integer rice9311Position;

	@Column(name="rice9311_refcall")
	private String rice9311Refcall;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="dj123_contig_id")
	private Feature feature1;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="from_contig_id")
	private Feature feature2;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="ir64_contig_id")
	private Feature feature3;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="kasalath_contig_id")
	private Feature feature4;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="nb_contig_id")
	private Feature feature5;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="rice9311_contig_id")
	private Feature feature6;

	//bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name="from_organism_id")
	private Organism organism;

	//bi-directional many-to-one association to SnpFeature
	@ManyToOne
	@JoinColumn(name="snp_feature_id")
	private SnpFeature snpFeature;

	public MvConvertposNb2allref() {
	}

	public Integer getMvConvertposNb2allrefsId() {
		return this.mvConvertposNb2allrefsId;
	}

	public void setMvConvertposNb2allrefsId(Integer mvConvertposNb2allrefsId) {
		this.mvConvertposNb2allrefsId = mvConvertposNb2allrefsId;
	}

	public Integer getDj123AlignCount() {
		return this.dj123AlignCount;
	}

	public void setDj123AlignCount(Integer dj123AlignCount) {
		this.dj123AlignCount = dj123AlignCount;
	}

	public Integer getDj123Position() {
		return this.dj123Position;
	}

	public void setDj123Position(Integer dj123Position) {
		this.dj123Position = dj123Position;
	}

	public String getDj123Refcall() {
		return this.dj123Refcall;
	}

	public void setDj123Refcall(String dj123Refcall) {
		this.dj123Refcall = dj123Refcall;
	}

	public Integer getFromPosition() {
		return this.fromPosition;
	}

	public void setFromPosition(Integer fromPosition) {
		this.fromPosition = fromPosition;
	}

	public String getFromRefcall() {
		return this.fromRefcall;
	}

	public void setFromRefcall(String fromRefcall) {
		this.fromRefcall = fromRefcall;
	}

	public Integer getIr64AlignCount() {
		return this.ir64AlignCount;
	}

	public void setIr64AlignCount(Integer ir64AlignCount) {
		this.ir64AlignCount = ir64AlignCount;
	}

	public Integer getIr64Position() {
		return this.ir64Position;
	}

	public void setIr64Position(Integer ir64Position) {
		this.ir64Position = ir64Position;
	}

	public String getIr64Refcall() {
		return this.ir64Refcall;
	}

	public void setIr64Refcall(String ir64Refcall) {
		this.ir64Refcall = ir64Refcall;
	}

	public Integer getKasalathAlignCount() {
		return this.kasalathAlignCount;
	}

	public void setKasalathAlignCount(Integer kasalathAlignCount) {
		this.kasalathAlignCount = kasalathAlignCount;
	}

	public Integer getKasalathPosition() {
		return this.kasalathPosition;
	}

	public void setKasalathPosition(Integer kasalathPosition) {
		this.kasalathPosition = kasalathPosition;
	}

	public String getKasalathRefcall() {
		return this.kasalathRefcall;
	}

	public void setKasalathRefcall(String kasalathRefcall) {
		this.kasalathRefcall = kasalathRefcall;
	}

	public Integer getNbAlignCount() {
		return this.nbAlignCount;
	}

	public void setNbAlignCount(Integer nbAlignCount) {
		this.nbAlignCount = nbAlignCount;
	}

	public Integer getNbPosition() {
		return this.nbPosition;
	}

	public void setNbPosition(Integer nbPosition) {
		this.nbPosition = nbPosition;
	}

	public String getNbRefcall() {
		return this.nbRefcall;
	}

	public void setNbRefcall(String nbRefcall) {
		this.nbRefcall = nbRefcall;
	}

	public Integer getRice9311AlignCount() {
		return this.rice9311AlignCount;
	}

	public void setRice9311AlignCount(Integer rice9311AlignCount) {
		this.rice9311AlignCount = rice9311AlignCount;
	}

	public Integer getRice9311Position() {
		return this.rice9311Position;
	}

	public void setRice9311Position(Integer rice9311Position) {
		this.rice9311Position = rice9311Position;
	}

	public String getRice9311Refcall() {
		return this.rice9311Refcall;
	}

	public void setRice9311Refcall(String rice9311Refcall) {
		this.rice9311Refcall = rice9311Refcall;
	}

	public Feature getFeature1() {
		return this.feature1;
	}

	public void setFeature1(Feature feature1) {
		this.feature1 = feature1;
	}

	public Feature getFeature2() {
		return this.feature2;
	}

	public void setFeature2(Feature feature2) {
		this.feature2 = feature2;
	}

	public Feature getFeature3() {
		return this.feature3;
	}

	public void setFeature3(Feature feature3) {
		this.feature3 = feature3;
	}

	public Feature getFeature4() {
		return this.feature4;
	}

	public void setFeature4(Feature feature4) {
		this.feature4 = feature4;
	}

	public Feature getFeature5() {
		return this.feature5;
	}

	public void setFeature5(Feature feature5) {
		this.feature5 = feature5;
	}

	public Feature getFeature6() {
		return this.feature6;
	}

	public void setFeature6(Feature feature6) {
		this.feature6 = feature6;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

	public SnpFeature getSnpFeature() {
		return this.snpFeature;
	}

	public void setSnpFeature(SnpFeature snpFeature) {
		this.snpFeature = snpFeature;
	}

}
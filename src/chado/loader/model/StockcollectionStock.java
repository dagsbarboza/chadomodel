package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stockcollection_stock database table.
 * 
 */
@Entity
@Table(name="stockcollection_stock")
@NamedQuery(name="StockcollectionStock.findAll", query="SELECT s FROM StockcollectionStock s")
public class StockcollectionStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stockcollection_stock_id")
	private Integer stockcollectionStockId;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	//bi-directional many-to-one association to Stockcollection
	@ManyToOne
	@JoinColumn(name="stockcollection_id")
	private Stockcollection stockcollection;

	public StockcollectionStock() {
	}

	public Integer getStockcollectionStockId() {
		return this.stockcollectionStockId;
	}

	public void setStockcollectionStockId(Integer stockcollectionStockId) {
		this.stockcollectionStockId = stockcollectionStockId;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Stockcollection getStockcollection() {
		return this.stockcollection;
	}

	public void setStockcollection(Stockcollection stockcollection) {
		this.stockcollection = stockcollection;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stock database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Stock.findAll", query="SELECT s FROM Stock s"),
	@NamedQuery(name="Stock.findById", query="SELECT s FROM Stock s where s.stockId= :stockId"),
	@NamedQuery(name="Stock.findByOrganismTypeName", query="SELECT s FROM Stock s where s.uniquename= :uniquename and s.cvterm=:cvterm and s.organism=:organism"),
	@NamedQuery(name="Stock.findByOrganism", query="SELECT s FROM Stock s where s.organism=:organism")
})

public class Stock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer stockId;

	private String description;

	@Column(name="is_obsolete")
	private Boolean isObsolete;

	private String name;

	@Column(name="stock_geolocation_id")
	private Integer stockGeolocationId;

	@Column(name="tmp_oldstock_id")
	private Integer tmpOldstockId;

	private String uniquename;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name="organism_id")
	private Organism organism;

	//bi-directional many-to-one association to StockCvterm
	@OneToMany(mappedBy="stock")
	private List<StockCvterm> stockCvterms;

	//bi-directional many-to-one association to StockDbxref
	@OneToMany(mappedBy="stock")
	private List<StockDbxref> stockDbxrefs;

	//bi-directional many-to-one association to StockGenotype
	@OneToMany(mappedBy="stock")
	private List<StockGenotype> stockGenotypes;

	//bi-directional many-to-one association to StockPhenotype
	@OneToMany(mappedBy="stock")
	private List<StockPhenotype> stockPhenotypes;

	//bi-directional many-to-one association to StockPub
	@OneToMany(mappedBy="stock")
	private List<StockPub> stockPubs;

	//bi-directional many-to-one association to StockRelationship
	@OneToMany(mappedBy="stock1")
	private List<StockRelationship> stockRelationships1;

	//bi-directional many-to-one association to StockRelationship
	@OneToMany(mappedBy="stock2")
	private List<StockRelationship> stockRelationships2;

	//bi-directional many-to-one association to StockSample
	@OneToMany(mappedBy="stock")
	private List<StockSample> stockSamples;

	//bi-directional many-to-one association to StockcollectionStock
	@OneToMany(mappedBy="stock")
	private List<StockcollectionStock> stockcollectionStocks;

	//bi-directional many-to-one association to Stockprop
	@OneToMany(mappedBy="stock")
	private List<Stockprop> stockprops;

	public Stock() {
	}

	public Integer getStockId() {
		return this.stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Boolean isObsolete) {
		this.isObsolete = isObsolete;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStockGeolocationId() {
		return this.stockGeolocationId;
	}

	public void setStockGeolocationId(Integer stockGeolocationId) {
		this.stockGeolocationId = stockGeolocationId;
	}

	public Integer getTmpOldstockId() {
		return this.tmpOldstockId;
	}

	public void setTmpOldstockId(Integer tmpOldstockId) {
		this.tmpOldstockId = tmpOldstockId;
	}

	public String getUniquename() {
		return this.uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

	public List<StockCvterm> getStockCvterms() {
		return this.stockCvterms;
	}

	public void setStockCvterms(List<StockCvterm> stockCvterms) {
		this.stockCvterms = stockCvterms;
	}

	public StockCvterm addStockCvterm(StockCvterm stockCvterm) {
		getStockCvterms().add(stockCvterm);
		stockCvterm.setStock(this);

		return stockCvterm;
	}

	public StockCvterm removeStockCvterm(StockCvterm stockCvterm) {
		getStockCvterms().remove(stockCvterm);
		stockCvterm.setStock(null);

		return stockCvterm;
	}

	public List<StockDbxref> getStockDbxrefs() {
		return this.stockDbxrefs;
	}

	public void setStockDbxrefs(List<StockDbxref> stockDbxrefs) {
		this.stockDbxrefs = stockDbxrefs;
	}

	public StockDbxref addStockDbxref(StockDbxref stockDbxref) {
		getStockDbxrefs().add(stockDbxref);
		stockDbxref.setStock(this);

		return stockDbxref;
	}

	public StockDbxref removeStockDbxref(StockDbxref stockDbxref) {
		getStockDbxrefs().remove(stockDbxref);
		stockDbxref.setStock(null);

		return stockDbxref;
	}

	public List<StockGenotype> getStockGenotypes() {
		return this.stockGenotypes;
	}

	public void setStockGenotypes(List<StockGenotype> stockGenotypes) {
		this.stockGenotypes = stockGenotypes;
	}

	public StockGenotype addStockGenotype(StockGenotype stockGenotype) {
		getStockGenotypes().add(stockGenotype);
		stockGenotype.setStock(this);

		return stockGenotype;
	}

	public StockGenotype removeStockGenotype(StockGenotype stockGenotype) {
		getStockGenotypes().remove(stockGenotype);
		stockGenotype.setStock(null);

		return stockGenotype;
	}

	public List<StockPhenotype> getStockPhenotypes() {
		return this.stockPhenotypes;
	}

	public void setStockPhenotypes(List<StockPhenotype> stockPhenotypes) {
		this.stockPhenotypes = stockPhenotypes;
	}

	public StockPhenotype addStockPhenotype(StockPhenotype stockPhenotype) {
		getStockPhenotypes().add(stockPhenotype);
		stockPhenotype.setStock(this);

		return stockPhenotype;
	}

	public StockPhenotype removeStockPhenotype(StockPhenotype stockPhenotype) {
		getStockPhenotypes().remove(stockPhenotype);
		stockPhenotype.setStock(null);

		return stockPhenotype;
	}

	public List<StockPub> getStockPubs() {
		return this.stockPubs;
	}

	public void setStockPubs(List<StockPub> stockPubs) {
		this.stockPubs = stockPubs;
	}

	public StockPub addStockPub(StockPub stockPub) {
		getStockPubs().add(stockPub);
		stockPub.setStock(this);

		return stockPub;
	}

	public StockPub removeStockPub(StockPub stockPub) {
		getStockPubs().remove(stockPub);
		stockPub.setStock(null);

		return stockPub;
	}

	public List<StockRelationship> getStockRelationships1() {
		return this.stockRelationships1;
	}

	public void setStockRelationships1(List<StockRelationship> stockRelationships1) {
		this.stockRelationships1 = stockRelationships1;
	}

	public StockRelationship addStockRelationships1(StockRelationship stockRelationships1) {
		getStockRelationships1().add(stockRelationships1);
		stockRelationships1.setStock1(this);

		return stockRelationships1;
	}

	public StockRelationship removeStockRelationships1(StockRelationship stockRelationships1) {
		getStockRelationships1().remove(stockRelationships1);
		stockRelationships1.setStock1(null);

		return stockRelationships1;
	}

	public List<StockRelationship> getStockRelationships2() {
		return this.stockRelationships2;
	}

	public void setStockRelationships2(List<StockRelationship> stockRelationships2) {
		this.stockRelationships2 = stockRelationships2;
	}

	public StockRelationship addStockRelationships2(StockRelationship stockRelationships2) {
		getStockRelationships2().add(stockRelationships2);
		stockRelationships2.setStock2(this);

		return stockRelationships2;
	}

	public StockRelationship removeStockRelationships2(StockRelationship stockRelationships2) {
		getStockRelationships2().remove(stockRelationships2);
		stockRelationships2.setStock2(null);

		return stockRelationships2;
	}

	public List<StockSample> getStockSamples() {
		return this.stockSamples;
	}

	public void setStockSamples(List<StockSample> stockSamples) {
		this.stockSamples = stockSamples;
	}

	public StockSample addStockSample(StockSample stockSample) {
		getStockSamples().add(stockSample);
		stockSample.setStock(this);

		return stockSample;
	}

	public StockSample removeStockSample(StockSample stockSample) {
		getStockSamples().remove(stockSample);
		stockSample.setStock(null);

		return stockSample;
	}

	public List<StockcollectionStock> getStockcollectionStocks() {
		return this.stockcollectionStocks;
	}

	public void setStockcollectionStocks(List<StockcollectionStock> stockcollectionStocks) {
		this.stockcollectionStocks = stockcollectionStocks;
	}

	public StockcollectionStock addStockcollectionStock(StockcollectionStock stockcollectionStock) {
		getStockcollectionStocks().add(stockcollectionStock);
		stockcollectionStock.setStock(this);

		return stockcollectionStock;
	}

	public StockcollectionStock removeStockcollectionStock(StockcollectionStock stockcollectionStock) {
		getStockcollectionStocks().remove(stockcollectionStock);
		stockcollectionStock.setStock(null);

		return stockcollectionStock;
	}

	public List<Stockprop> getStockprops() {
		return this.stockprops;
	}

	public void setStockprops(List<Stockprop> stockprops) {
		this.stockprops = stockprops;
	}

	public Stockprop addStockprop(Stockprop stockprop) {
		getStockprops().add(stockprop);
		stockprop.setStock(this);

		return stockprop;
	}

	public Stockprop removeStockprop(Stockprop stockprop) {
		getStockprops().remove(stockprop);
		stockprop.setStock(null);

		return stockprop;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the featureloc database table.
 * 
 */
@Entity
@NamedQuery(name="Featureloc.findAll", query="SELECT f FROM Featureloc f")
public class Featureloc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="featureloc_id")
	private Integer featurelocId;

	private Integer fmax;

	private Integer fmin;

	@Column(name="is_fmax_partial")
	private Boolean isFmaxPartial;

	@Column(name="is_fmin_partial")
	private Boolean isFminPartial;

	private Integer locgroup;

	private Integer phase;

	private Integer rank;

	@Column(name="residue_info")
	private String residueInfo;

	private Integer strand;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature1;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="srcfeature_id")
	private Feature feature2;

	//bi-directional many-to-one association to FeaturelocPub
	@OneToMany(mappedBy="featureloc")
	private List<FeaturelocPub> featurelocPubs;

	public Featureloc() {
	}

	public Integer getFeaturelocId() {
		return this.featurelocId;
	}

	public void setFeaturelocId(Integer featurelocId) {
		this.featurelocId = featurelocId;
	}

	public Integer getFmax() {
		return this.fmax;
	}

	public void setFmax(Integer fmax) {
		this.fmax = fmax;
	}

	public Integer getFmin() {
		return this.fmin;
	}

	public void setFmin(Integer fmin) {
		this.fmin = fmin;
	}

	public Boolean getIsFmaxPartial() {
		return this.isFmaxPartial;
	}

	public void setIsFmaxPartial(Boolean isFmaxPartial) {
		this.isFmaxPartial = isFmaxPartial;
	}

	public Boolean getIsFminPartial() {
		return this.isFminPartial;
	}

	public void setIsFminPartial(Boolean isFminPartial) {
		this.isFminPartial = isFminPartial;
	}

	public Integer getLocgroup() {
		return this.locgroup;
	}

	public void setLocgroup(Integer locgroup) {
		this.locgroup = locgroup;
	}

	public Integer getPhase() {
		return this.phase;
	}

	public void setPhase(Integer phase) {
		this.phase = phase;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getResidueInfo() {
		return this.residueInfo;
	}

	public void setResidueInfo(String residueInfo) {
		this.residueInfo = residueInfo;
	}

	public Integer getStrand() {
		return this.strand;
	}

	public void setStrand(Integer strand) {
		this.strand = strand;
	}

	public Feature getFeature1() {
		return this.feature1;
	}

	public void setFeature1(Feature feature1) {
		this.feature1 = feature1;
	}

	public Feature getFeature2() {
		return this.feature2;
	}

	public void setFeature2(Feature feature2) {
		this.feature2 = feature2;
	}

	public List<FeaturelocPub> getFeaturelocPubs() {
		return this.featurelocPubs;
	}

	public void setFeaturelocPubs(List<FeaturelocPub> featurelocPubs) {
		this.featurelocPubs = featurelocPubs;
	}

	public FeaturelocPub addFeaturelocPub(FeaturelocPub featurelocPub) {
		getFeaturelocPubs().add(featurelocPub);
		featurelocPub.setFeatureloc(this);

		return featurelocPub;
	}

	public FeaturelocPub removeFeaturelocPub(FeaturelocPub featurelocPub) {
		getFeaturelocPubs().remove(featurelocPub);
		featurelocPub.setFeatureloc(null);

		return featurelocPub;
	}

}
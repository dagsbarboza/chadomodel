package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stock_dbxrefprop database table.
 * 
 */
@Entity
@Table(name="stock_dbxrefprop")
@NamedQuery(name="StockDbxrefprop.findAll", query="SELECT s FROM StockDbxrefprop s")
public class StockDbxrefprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_dbxrefprop_id")
	private Integer stockDbxrefpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to StockDbxref
	@ManyToOne
	@JoinColumn(name="stock_dbxref_id")
	private StockDbxref stockDbxref;

	public StockDbxrefprop() {
	}

	public Integer getStockDbxrefpropId() {
		return this.stockDbxrefpropId;
	}

	public void setStockDbxrefpropId(Integer stockDbxrefpropId) {
		this.stockDbxrefpropId = stockDbxrefpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public StockDbxref getStockDbxref() {
		return this.stockDbxref;
	}

	public void setStockDbxref(StockDbxref stockDbxref) {
		this.stockDbxref = stockDbxref;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stockcollectionprop database table.
 * 
 */
@Entity
@NamedQuery(name="Stockcollectionprop.findAll", query="SELECT s FROM Stockcollectionprop s")
public class Stockcollectionprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stockcollectionprop_id")
	private Integer stockcollectionpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Stockcollection
	@ManyToOne
	@JoinColumn(name="stockcollection_id")
	private Stockcollection stockcollection;

	public Stockcollectionprop() {
	}

	public Integer getStockcollectionpropId() {
		return this.stockcollectionpropId;
	}

	public void setStockcollectionpropId(Integer stockcollectionpropId) {
		this.stockcollectionpropId = stockcollectionpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Stockcollection getStockcollection() {
		return this.stockcollection;
	}

	public void setStockcollection(Stockcollection stockcollection) {
		this.stockcollection = stockcollection;
	}

}
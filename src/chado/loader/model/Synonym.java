package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the synonym database table.
 * 
 */
@Entity
@NamedQuery(name="Synonym.findAll", query="SELECT s FROM Synonym s")
public class Synonym implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="synonym_id")
	private Integer synonymId;

	private String name;

	@Column(name="synonym_sgml")
	private String synonymSgml;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	public Synonym() {
	}

	public Integer getSynonymId() {
		return this.synonymId;
	}

	public void setSynonymId(Integer synonymId) {
		this.synonymId = synonymId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSynonymSgml() {
		return this.synonymSgml;
	}

	public void setSynonymSgml(String synonymSgml) {
		this.synonymSgml = synonymSgml;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

}
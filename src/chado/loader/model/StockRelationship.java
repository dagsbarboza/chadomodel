package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stock_relationship database table.
 * 
 */
@Entity
@Table(name="stock_relationship")
@NamedQuery(name="StockRelationship.findAll", query="SELECT s FROM StockRelationship s")
public class StockRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_relationship_id")
	private Integer stockRelationshipId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="object_id")
	private Stock stock1;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Stock stock2;

	//bi-directional many-to-one association to StockRelationshipCvterm
	@OneToMany(mappedBy="stockRelationship")
	private List<StockRelationshipCvterm> stockRelationshipCvterms;

	//bi-directional many-to-one association to StockRelationshipPub
	@OneToMany(mappedBy="stockRelationship")
	private List<StockRelationshipPub> stockRelationshipPubs;

	public StockRelationship() {
	}

	public Integer getStockRelationshipId() {
		return this.stockRelationshipId;
	}

	public void setStockRelationshipId(Integer stockRelationshipId) {
		this.stockRelationshipId = stockRelationshipId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Stock getStock1() {
		return this.stock1;
	}

	public void setStock1(Stock stock1) {
		this.stock1 = stock1;
	}

	public Stock getStock2() {
		return this.stock2;
	}

	public void setStock2(Stock stock2) {
		this.stock2 = stock2;
	}

	public List<StockRelationshipCvterm> getStockRelationshipCvterms() {
		return this.stockRelationshipCvterms;
	}

	public void setStockRelationshipCvterms(List<StockRelationshipCvterm> stockRelationshipCvterms) {
		this.stockRelationshipCvterms = stockRelationshipCvterms;
	}

	public StockRelationshipCvterm addStockRelationshipCvterm(StockRelationshipCvterm stockRelationshipCvterm) {
		getStockRelationshipCvterms().add(stockRelationshipCvterm);
		stockRelationshipCvterm.setStockRelationship(this);

		return stockRelationshipCvterm;
	}

	public StockRelationshipCvterm removeStockRelationshipCvterm(StockRelationshipCvterm stockRelationshipCvterm) {
		getStockRelationshipCvterms().remove(stockRelationshipCvterm);
		stockRelationshipCvterm.setStockRelationship(null);

		return stockRelationshipCvterm;
	}

	public List<StockRelationshipPub> getStockRelationshipPubs() {
		return this.stockRelationshipPubs;
	}

	public void setStockRelationshipPubs(List<StockRelationshipPub> stockRelationshipPubs) {
		this.stockRelationshipPubs = stockRelationshipPubs;
	}

	public StockRelationshipPub addStockRelationshipPub(StockRelationshipPub stockRelationshipPub) {
		getStockRelationshipPubs().add(stockRelationshipPub);
		stockRelationshipPub.setStockRelationship(this);

		return stockRelationshipPub;
	}

	public StockRelationshipPub removeStockRelationshipPub(StockRelationshipPub stockRelationshipPub) {
		getStockRelationshipPubs().remove(stockRelationshipPub);
		stockRelationshipPub.setStockRelationship(null);

		return stockRelationshipPub;
	}

}
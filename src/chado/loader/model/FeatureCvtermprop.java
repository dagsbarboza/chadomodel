package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_cvtermprop database table.
 * 
 */
@Entity
@Table(name="feature_cvtermprop")
@NamedQuery(name="FeatureCvtermprop.findAll", query="SELECT f FROM FeatureCvtermprop f")
public class FeatureCvtermprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_cvtermprop_id")
	private Integer featureCvtermpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to FeatureCvterm
	@ManyToOne
	@JoinColumn(name="feature_cvterm_id")
	private FeatureCvterm featureCvterm;

	public FeatureCvtermprop() {
	}

	public Integer getFeatureCvtermpropId() {
		return this.featureCvtermpropId;
	}

	public void setFeatureCvtermpropId(Integer featureCvtermpropId) {
		this.featureCvtermpropId = featureCvtermpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public FeatureCvterm getFeatureCvterm() {
		return this.featureCvterm;
	}

	public void setFeatureCvterm(FeatureCvterm featureCvterm) {
		this.featureCvterm = featureCvterm;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the feature_expression database table.
 * 
 */
@Entity
@Table(name="feature_expression")
@NamedQuery(name="FeatureExpression.findAll", query="SELECT f FROM FeatureExpression f")
public class FeatureExpression implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_expression_id")
	private Integer featureExpressionId;

	@Column(name="expression_id")
	private Integer expressionId;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to FeatureExpressionprop
	@OneToMany(mappedBy="featureExpression")
	private List<FeatureExpressionprop> featureExpressionprops;

	public FeatureExpression() {
	}

	public Integer getFeatureExpressionId() {
		return this.featureExpressionId;
	}

	public void setFeatureExpressionId(Integer featureExpressionId) {
		this.featureExpressionId = featureExpressionId;
	}

	public Integer getExpressionId() {
		return this.expressionId;
	}

	public void setExpressionId(Integer expressionId) {
		this.expressionId = expressionId;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public List<FeatureExpressionprop> getFeatureExpressionprops() {
		return this.featureExpressionprops;
	}

	public void setFeatureExpressionprops(List<FeatureExpressionprop> featureExpressionprops) {
		this.featureExpressionprops = featureExpressionprops;
	}

	public FeatureExpressionprop addFeatureExpressionprop(FeatureExpressionprop featureExpressionprop) {
		getFeatureExpressionprops().add(featureExpressionprop);
		featureExpressionprop.setFeatureExpression(this);

		return featureExpressionprop;
	}

	public FeatureExpressionprop removeFeatureExpressionprop(FeatureExpressionprop featureExpressionprop) {
		getFeatureExpressionprops().remove(featureExpressionprop);
		featureExpressionprop.setFeatureExpression(null);

		return featureExpressionprop;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stock_pub database table.
 * 
 */
@Entity
@Table(name="stock_pub")
@NamedQuery(name="StockPub.findAll", query="SELECT s FROM StockPub s")
public class StockPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_pub_id")
	private Integer stockPubId;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	public StockPub() {
	}

	public Integer getStockPubId() {
		return this.stockPubId;
	}

	public void setStockPubId(Integer stockPubId) {
		this.stockPubId = stockPubId;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
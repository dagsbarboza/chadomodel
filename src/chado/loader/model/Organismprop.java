package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the organismprop database table.
 * 
 */
@Entity
@NamedQuery(name="Organismprop.findAll", query="SELECT o FROM Organismprop o")
public class Organismprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="organismprop_id")
	private Integer organismpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name="organism_id")
	private Organism organism;

	public Organismprop() {
	}

	public Integer getOrganismpropId() {
		return this.organismpropId;
	}

	public void setOrganismpropId(Integer organismpropId) {
		this.organismpropId = organismpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pub_relationship database table.
 * 
 */
@Entity
@Table(name="pub_relationship")
@NamedQuery(name="PubRelationship.findAll", query="SELECT p FROM PubRelationship p")
public class PubRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pub_relationship_id")
	private Integer pubRelationshipId;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="object_id")
	private Pub pub1;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Pub pub2;

	public PubRelationship() {
	}

	public Integer getPubRelationshipId() {
		return this.pubRelationshipId;
	}

	public void setPubRelationshipId(Integer pubRelationshipId) {
		this.pubRelationshipId = pubRelationshipId;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Pub getPub1() {
		return this.pub1;
	}

	public void setPub1(Pub pub1) {
		this.pub1 = pub1;
	}

	public Pub getPub2() {
		return this.pub2;
	}

	public void setPub2(Pub pub2) {
		this.pub2 = pub2;
	}

}
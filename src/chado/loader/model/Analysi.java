package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the analysis database table.
 * 
 */
@Entity
@Table(name="analysis")
@NamedQuery(name="Analysi.findAll", query="SELECT a FROM Analysi a")
public class Analysi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="analysis_id")
	private Integer analysisId;

	private String algorithm;

	private String description;

	private String name;

	private String program;

	private String programversion;

	private String sourcename;

	private String sourceuri;

	private String sourceversion;

	private Timestamp timeexecuted;

	//bi-directional many-to-one association to Analysisfeature
	@OneToMany(mappedBy="analysi")
	private List<Analysisfeature> analysisfeatures;

	public Analysi() {
	}

	public Integer getAnalysisId() {
		return this.analysisId;
	}

	public void setAnalysisId(Integer analysisId) {
		this.analysisId = analysisId;
	}

	public String getAlgorithm() {
		return this.algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProgram() {
		return this.program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public String getProgramversion() {
		return this.programversion;
	}

	public void setProgramversion(String programversion) {
		this.programversion = programversion;
	}

	public String getSourcename() {
		return this.sourcename;
	}

	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	public String getSourceuri() {
		return this.sourceuri;
	}

	public void setSourceuri(String sourceuri) {
		this.sourceuri = sourceuri;
	}

	public String getSourceversion() {
		return this.sourceversion;
	}

	public void setSourceversion(String sourceversion) {
		this.sourceversion = sourceversion;
	}

	public Timestamp getTimeexecuted() {
		return this.timeexecuted;
	}

	public void setTimeexecuted(Timestamp timeexecuted) {
		this.timeexecuted = timeexecuted;
	}

	public List<Analysisfeature> getAnalysisfeatures() {
		return this.analysisfeatures;
	}

	public void setAnalysisfeatures(List<Analysisfeature> analysisfeatures) {
		this.analysisfeatures = analysisfeatures;
	}

	public Analysisfeature addAnalysisfeature(Analysisfeature analysisfeature) {
		getAnalysisfeatures().add(analysisfeature);
		analysisfeature.setAnalysi(this);

		return analysisfeature;
	}

	public Analysisfeature removeAnalysisfeature(Analysisfeature analysisfeature) {
		getAnalysisfeatures().remove(analysisfeature);
		analysisfeature.setAnalysi(null);

		return analysisfeature;
	}

}
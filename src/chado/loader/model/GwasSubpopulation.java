package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the gwas_subpopulation database table.
 * 
 */
@Entity
@Table(name="gwas_subpopulation")
@NamedQuery(name="GwasSubpopulation.findAll", query="SELECT g FROM GwasSubpopulation g")
public class GwasSubpopulation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="gwas_subpopulation_id")
	private Integer gwasSubpopulationId;

	private String name;

	//bi-directional many-to-one association to GwasRun
	@OneToMany(mappedBy="gwasSubpopulation")
	private List<GwasRun> gwasRuns;

	public GwasSubpopulation() {
	}

	public Integer getGwasSubpopulationId() {
		return this.gwasSubpopulationId;
	}

	public void setGwasSubpopulationId(Integer gwasSubpopulationId) {
		this.gwasSubpopulationId = gwasSubpopulationId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<GwasRun> getGwasRuns() {
		return this.gwasRuns;
	}

	public void setGwasRuns(List<GwasRun> gwasRuns) {
		this.gwasRuns = gwasRuns;
	}

	public GwasRun addGwasRun(GwasRun gwasRun) {
		getGwasRuns().add(gwasRun);
		gwasRun.setGwasSubpopulation(this);

		return gwasRun;
	}

	public GwasRun removeGwasRun(GwasRun gwasRun) {
		getGwasRuns().remove(gwasRun);
		gwasRun.setGwasSubpopulation(null);

		return gwasRun;
	}

}
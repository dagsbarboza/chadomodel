package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_cvterm_pub database table.
 * 
 */
@Entity
@Table(name="feature_cvterm_pub")
@NamedQuery(name="FeatureCvtermPub.findAll", query="SELECT f FROM FeatureCvtermPub f")
public class FeatureCvtermPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_cvterm_pub_id")
	private Integer featureCvtermPubId;

	//bi-directional many-to-one association to FeatureCvterm
	@ManyToOne
	@JoinColumn(name="feature_cvterm_id")
	private FeatureCvterm featureCvterm;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public FeatureCvtermPub() {
	}

	public Integer getFeatureCvtermPubId() {
		return this.featureCvtermPubId;
	}

	public void setFeatureCvtermPubId(Integer featureCvtermPubId) {
		this.featureCvtermPubId = featureCvtermPubId;
	}

	public FeatureCvterm getFeatureCvterm() {
		return this.featureCvterm;
	}

	public void setFeatureCvterm(FeatureCvterm featureCvterm) {
		this.featureCvterm = featureCvterm;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the organism_dbxref database table.
 * 
 */
@Entity
@Table(name="organism_dbxref")
@NamedQuery(name="OrganismDbxref.findAll", query="SELECT o FROM OrganismDbxref o")
public class OrganismDbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="organism_dbxref_id")
	private Integer organismDbxrefId;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name="organism_id")
	private Organism organism;

	public OrganismDbxref() {
	}

	public Integer getOrganismDbxrefId() {
		return this.organismDbxrefId;
	}

	public void setOrganismDbxrefId(Integer organismDbxrefId) {
		this.organismDbxrefId = organismDbxrefId;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

}
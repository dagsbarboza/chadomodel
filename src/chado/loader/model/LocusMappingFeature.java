package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the locus_mapping_feature database table.
 * 
 */
@Entity
@Table(name="locus_mapping_feature")
@NamedQuery(name="LocusMappingFeature.findAll", query="SELECT l FROM LocusMappingFeature l")
public class LocusMappingFeature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="db_id")
	private Long dbId;

	@Column(name="feature_id")
	private Long featureId;

	@Id
	@Column(name="locus_mapping_feature_id")
	private Long locusMappingFeatureId;

	@Column(name="locus_mapping_id")
	private Long locusMappingId;

	public LocusMappingFeature() {
	}

	public Long getDbId() {
		return this.dbId;
	}

	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}

	public Long getFeatureId() {
		return this.featureId;
	}

	public void setFeatureId(Long featureId) {
		this.featureId = featureId;
	}

	public Long getLocusMappingFeatureId() {
		return this.locusMappingFeatureId;
	}

	public void setLocusMappingFeatureId(Long locusMappingFeatureId) {
		this.locusMappingFeatureId = locusMappingFeatureId;
	}

	public Long getLocusMappingId() {
		return this.locusMappingId;
	}

	public void setLocusMappingId(Long locusMappingId) {
		this.locusMappingId = locusMappingId;
	}

}
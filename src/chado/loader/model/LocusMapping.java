package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the locus_mapping database table.
 * 
 */
@Entity
@Table(name="locus_mapping")
@NamedQuery(name="LocusMapping.findAll", query="SELECT l FROM LocusMapping l")
public class LocusMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fgenesh;

	@Id
	@Column(name="locus_mapping_id")
	private Long locusMappingId;

	private String msu7;

	private String name;

	@Column(name="rap_predicted")
	private String rapPredicted;

	@Column(name="rap_representative")
	private String rapRepresentative;

	public LocusMapping() {
	}

	public String getFgenesh() {
		return this.fgenesh;
	}

	public void setFgenesh(String fgenesh) {
		this.fgenesh = fgenesh;
	}

	public Long getLocusMappingId() {
		return this.locusMappingId;
	}

	public void setLocusMappingId(Long locusMappingId) {
		this.locusMappingId = locusMappingId;
	}

	public String getMsu7() {
		return this.msu7;
	}

	public void setMsu7(String msu7) {
		this.msu7 = msu7;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRapPredicted() {
		return this.rapPredicted;
	}

	public void setRapPredicted(String rapPredicted) {
		this.rapPredicted = rapPredicted;
	}

	public String getRapRepresentative() {
		return this.rapRepresentative;
	}

	public void setRapRepresentative(String rapRepresentative) {
		this.rapRepresentative = rapRepresentative;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the feature_relationshipprop database table.
 * 
 */
@Entity
@Table(name="feature_relationshipprop")
@NamedQuery(name="FeatureRelationshipprop.findAll", query="SELECT f FROM FeatureRelationshipprop f")
public class FeatureRelationshipprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_relationshipprop_id")
	private Integer featureRelationshippropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to FeatureRelationship
	@ManyToOne
	@JoinColumn(name="feature_relationship_id")
	private FeatureRelationship featureRelationship;

	//bi-directional many-to-one association to FeatureRelationshippropPub
	@OneToMany(mappedBy="featureRelationshipprop")
	private List<FeatureRelationshippropPub> featureRelationshippropPubs;

	public FeatureRelationshipprop() {
	}

	public Integer getFeatureRelationshippropId() {
		return this.featureRelationshippropId;
	}

	public void setFeatureRelationshippropId(Integer featureRelationshippropId) {
		this.featureRelationshippropId = featureRelationshippropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public FeatureRelationship getFeatureRelationship() {
		return this.featureRelationship;
	}

	public void setFeatureRelationship(FeatureRelationship featureRelationship) {
		this.featureRelationship = featureRelationship;
	}

	public List<FeatureRelationshippropPub> getFeatureRelationshippropPubs() {
		return this.featureRelationshippropPubs;
	}

	public void setFeatureRelationshippropPubs(List<FeatureRelationshippropPub> featureRelationshippropPubs) {
		this.featureRelationshippropPubs = featureRelationshippropPubs;
	}

	public FeatureRelationshippropPub addFeatureRelationshippropPub(FeatureRelationshippropPub featureRelationshippropPub) {
		getFeatureRelationshippropPubs().add(featureRelationshippropPub);
		featureRelationshippropPub.setFeatureRelationshipprop(this);

		return featureRelationshippropPub;
	}

	public FeatureRelationshippropPub removeFeatureRelationshippropPub(FeatureRelationshippropPub featureRelationshippropPub) {
		getFeatureRelationshippropPubs().remove(featureRelationshippropPub);
		featureRelationshippropPub.setFeatureRelationshipprop(null);

		return featureRelationshippropPub;
	}

}
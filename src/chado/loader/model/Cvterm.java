package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cvterm database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Cvterm.findAll", query="SELECT c FROM Cvterm c"),
	@NamedQuery(name="Cvterm.findAllNames", query="SELECT c.name FROM Cvterm c order by c.name"),
	@NamedQuery(name="Cvterm.findById", query="SELECT c FROM Cvterm c where c.cvtermId =:cvtermId "),
	@NamedQuery(name="Cvterm.findByName", query="SELECT c FROM Cvterm c where c.name=:name "),
	@NamedQuery(name="Cvterm.findFeatureType", query="select ct from Cvterm ct, Cv cv_ where ct.name=:ctName and ct.cv=cv_ and cv_.name=:cvName")
	
})
public class Cvterm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvterm_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer cvtermId;

	private String definition;

	@Column(name="is_obsolete")
	private Integer isObsolete;

	@Column(name="is_relationshiptype")
	private Integer isRelationshiptype;

	private String name;

	//bi-directional many-to-one association to Chadoprop
	@OneToMany(mappedBy="cvterm")
	private List<Chadoprop> chadoprops;

	//bi-directional many-to-one association to Cvprop
	@OneToMany(mappedBy="cvterm")
	private List<Cvprop> cvprops;

	//bi-directional many-to-one association to Cv
	@ManyToOne
	@JoinColumn(name="cv_id")
	private Cv cv;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to CvtermDbxref
	@OneToMany(mappedBy="cvterm")
	private List<CvtermDbxref> cvtermDbxrefs;

	//bi-directional many-to-one association to CvtermRelationship
	@OneToMany(mappedBy="cvterm1")
	private List<CvtermRelationship> cvtermRelationships1;

	//bi-directional many-to-one association to CvtermRelationship
	@OneToMany(mappedBy="cvterm2")
	private List<CvtermRelationship> cvtermRelationships2;

	//bi-directional many-to-one association to CvtermRelationship
	@OneToMany(mappedBy="cvterm3")
	private List<CvtermRelationship> cvtermRelationships3;

	//bi-directional many-to-one association to Cvtermpath
	@OneToMany(mappedBy="cvterm1")
	private List<Cvtermpath> cvtermpaths1;

	//bi-directional many-to-one association to Cvtermpath
	@OneToMany(mappedBy="cvterm2")
	private List<Cvtermpath> cvtermpaths2;

	//bi-directional many-to-one association to Cvtermpath
	@OneToMany(mappedBy="cvterm3")
	private List<Cvtermpath> cvtermpaths3;

	//bi-directional many-to-one association to Cvtermprop
	@OneToMany(mappedBy="cvterm1")
	private List<Cvtermprop> cvtermprops1;

	//bi-directional many-to-one association to Cvtermprop
	@OneToMany(mappedBy="cvterm2")
	private List<Cvtermprop> cvtermprops2;

	//bi-directional many-to-one association to Cvtermsynonym
	@OneToMany(mappedBy="cvterm1")
	private List<Cvtermsynonym> cvtermsynonyms1;

	//bi-directional many-to-one association to Cvtermsynonym
	@OneToMany(mappedBy="cvterm2")
	private List<Cvtermsynonym> cvtermsynonyms2;

	//bi-directional many-to-one association to Dbxrefprop
	@OneToMany(mappedBy="cvterm")
	private List<Dbxrefprop> dbxrefprops;

	//bi-directional many-to-one association to Feature
	@OneToMany(mappedBy="cvterm")
	private List<Feature> features;

	//bi-directional many-to-one association to FeatureCvterm
	@OneToMany(mappedBy="cvterm")
	private List<FeatureCvterm> featureCvterms;

	//bi-directional many-to-one association to FeatureCvtermprop
	@OneToMany(mappedBy="cvterm")
	private List<FeatureCvtermprop> featureCvtermprops;

	//bi-directional many-to-one association to FeatureExpressionprop
	@OneToMany(mappedBy="cvterm")
	private List<FeatureExpressionprop> featureExpressionprops;

	//bi-directional many-to-one association to FeatureGenotype
	@OneToMany(mappedBy="cvterm")
	private List<FeatureGenotype> featureGenotypes;

	//bi-directional many-to-one association to FeaturePubprop
	@OneToMany(mappedBy="cvterm")
	private List<FeaturePubprop> featurePubprops;

	//bi-directional many-to-one association to FeatureRelationship
	@OneToMany(mappedBy="cvterm")
	private List<FeatureRelationship> featureRelationships;

	//bi-directional many-to-one association to FeatureRelationshipprop
	@OneToMany(mappedBy="cvterm")
	private List<FeatureRelationshipprop> featureRelationshipprops;

	//bi-directional many-to-one association to Featureprop
	@OneToMany(mappedBy="cvterm")
	private List<Featureprop> featureprops;

	//bi-directional many-to-one association to GwasTrait
	@OneToMany(mappedBy="cvterm")
	private List<GwasTrait> gwasTraits;

	//bi-directional many-to-one association to Organismprop
	@OneToMany(mappedBy="cvterm")
	private List<Organismprop> organismprops;

	//bi-directional many-to-one association to Platform
	@OneToMany(mappedBy="cvterm")
	private List<Platform> platforms;

	//bi-directional many-to-one association to Pub
	@OneToMany(mappedBy="cvterm")
	private List<Pub> pubs;

	//bi-directional many-to-one association to PubRelationship
	@OneToMany(mappedBy="cvterm")
	private List<PubRelationship> pubRelationships;

	//bi-directional many-to-one association to Pubprop
	@OneToMany(mappedBy="cvterm")
	private List<Pubprop> pubprops;

	//bi-directional many-to-one association to SnpFeatureprop
	@OneToMany(mappedBy="cvterm")
	private List<SnpFeatureprop> snpFeatureprops;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="cvterm")
	private List<Stock> stocks;

	//bi-directional many-to-one association to StockCvterm
	@OneToMany(mappedBy="cvterm")
	private List<StockCvterm> stockCvterms;

	//bi-directional many-to-one association to StockCvtermprop
	@OneToMany(mappedBy="cvterm")
	private List<StockCvtermprop> stockCvtermprops;

	//bi-directional many-to-one association to StockDbxrefprop
	@OneToMany(mappedBy="cvterm")
	private List<StockDbxrefprop> stockDbxrefprops;

	//bi-directional many-to-one association to StockPhenotype
	@OneToMany(mappedBy="cvterm1")
	private List<StockPhenotype> stockPhenotypes1;

	//bi-directional many-to-one association to StockPhenotype
	@OneToMany(mappedBy="cvterm2")
	private List<StockPhenotype> stockPhenotypes2;

	//bi-directional many-to-one association to StockRelationship
	@OneToMany(mappedBy="cvterm")
	private List<StockRelationship> stockRelationships;

	//bi-directional many-to-one association to StockRelationshipCvterm
	@OneToMany(mappedBy="cvterm")
	private List<StockRelationshipCvterm> stockRelationshipCvterms;

	//bi-directional many-to-one association to Stockcollection
	@OneToMany(mappedBy="cvterm")
	private List<Stockcollection> stockcollections;

	//bi-directional many-to-one association to Stockcollectionprop
	@OneToMany(mappedBy="cvterm")
	private List<Stockcollectionprop> stockcollectionprops;

	//bi-directional many-to-one association to Stockprop
	@OneToMany(mappedBy="cvterm")
	private List<Stockprop> stockprops;

	//bi-directional many-to-one association to Synonym
	@OneToMany(mappedBy="cvterm")
	private List<Synonym> synonyms;

	//bi-directional many-to-one association to VariantVariantset
	@OneToMany(mappedBy="cvterm")
	private List<VariantVariantset> variantVariantsets;

	//bi-directional many-to-one association to Variantset
	@OneToMany(mappedBy="cvterm")
	private List<Variantset> variantsets;

	public Cvterm() {
	}

	public Integer getCvtermId() {
		return this.cvtermId;
	}

	public void setCvtermId(Integer cvtermId) {
		this.cvtermId = cvtermId;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Integer getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Integer isObsolete) {
		this.isObsolete = isObsolete;
	}

	public Integer getIsRelationshiptype() {
		return this.isRelationshiptype;
	}

	public void setIsRelationshiptype(Integer isRelationshiptype) {
		this.isRelationshiptype = isRelationshiptype;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Chadoprop> getChadoprops() {
		return this.chadoprops;
	}

	public void setChadoprops(List<Chadoprop> chadoprops) {
		this.chadoprops = chadoprops;
	}

	public Chadoprop addChadoprop(Chadoprop chadoprop) {
		getChadoprops().add(chadoprop);
		chadoprop.setCvterm(this);

		return chadoprop;
	}

	public Chadoprop removeChadoprop(Chadoprop chadoprop) {
		getChadoprops().remove(chadoprop);
		chadoprop.setCvterm(null);

		return chadoprop;
	}

	public List<Cvprop> getCvprops() {
		return this.cvprops;
	}

	public void setCvprops(List<Cvprop> cvprops) {
		this.cvprops = cvprops;
	}

	public Cvprop addCvprop(Cvprop cvprop) {
		getCvprops().add(cvprop);
		cvprop.setCvterm(this);

		return cvprop;
	}

	public Cvprop removeCvprop(Cvprop cvprop) {
		getCvprops().remove(cvprop);
		cvprop.setCvterm(null);

		return cvprop;
	}

	public Cv getCv() {
		return this.cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public List<CvtermDbxref> getCvtermDbxrefs() {
		return this.cvtermDbxrefs;
	}

	public void setCvtermDbxrefs(List<CvtermDbxref> cvtermDbxrefs) {
		this.cvtermDbxrefs = cvtermDbxrefs;
	}

	public CvtermDbxref addCvtermDbxref(CvtermDbxref cvtermDbxref) {
		getCvtermDbxrefs().add(cvtermDbxref);
		cvtermDbxref.setCvterm(this);

		return cvtermDbxref;
	}

	public CvtermDbxref removeCvtermDbxref(CvtermDbxref cvtermDbxref) {
		getCvtermDbxrefs().remove(cvtermDbxref);
		cvtermDbxref.setCvterm(null);

		return cvtermDbxref;
	}

	public List<CvtermRelationship> getCvtermRelationships1() {
		return this.cvtermRelationships1;
	}

	public void setCvtermRelationships1(List<CvtermRelationship> cvtermRelationships1) {
		this.cvtermRelationships1 = cvtermRelationships1;
	}

	public CvtermRelationship addCvtermRelationships1(CvtermRelationship cvtermRelationships1) {
		getCvtermRelationships1().add(cvtermRelationships1);
		cvtermRelationships1.setCvterm1(this);

		return cvtermRelationships1;
	}

	public CvtermRelationship removeCvtermRelationships1(CvtermRelationship cvtermRelationships1) {
		getCvtermRelationships1().remove(cvtermRelationships1);
		cvtermRelationships1.setCvterm1(null);

		return cvtermRelationships1;
	}

	public List<CvtermRelationship> getCvtermRelationships2() {
		return this.cvtermRelationships2;
	}

	public void setCvtermRelationships2(List<CvtermRelationship> cvtermRelationships2) {
		this.cvtermRelationships2 = cvtermRelationships2;
	}

	public CvtermRelationship addCvtermRelationships2(CvtermRelationship cvtermRelationships2) {
		getCvtermRelationships2().add(cvtermRelationships2);
		cvtermRelationships2.setCvterm2(this);

		return cvtermRelationships2;
	}

	public CvtermRelationship removeCvtermRelationships2(CvtermRelationship cvtermRelationships2) {
		getCvtermRelationships2().remove(cvtermRelationships2);
		cvtermRelationships2.setCvterm2(null);

		return cvtermRelationships2;
	}

	public List<CvtermRelationship> getCvtermRelationships3() {
		return this.cvtermRelationships3;
	}

	public void setCvtermRelationships3(List<CvtermRelationship> cvtermRelationships3) {
		this.cvtermRelationships3 = cvtermRelationships3;
	}

	public CvtermRelationship addCvtermRelationships3(CvtermRelationship cvtermRelationships3) {
		getCvtermRelationships3().add(cvtermRelationships3);
		cvtermRelationships3.setCvterm3(this);

		return cvtermRelationships3;
	}

	public CvtermRelationship removeCvtermRelationships3(CvtermRelationship cvtermRelationships3) {
		getCvtermRelationships3().remove(cvtermRelationships3);
		cvtermRelationships3.setCvterm3(null);

		return cvtermRelationships3;
	}

	public List<Cvtermpath> getCvtermpaths1() {
		return this.cvtermpaths1;
	}

	public void setCvtermpaths1(List<Cvtermpath> cvtermpaths1) {
		this.cvtermpaths1 = cvtermpaths1;
	}

	public Cvtermpath addCvtermpaths1(Cvtermpath cvtermpaths1) {
		getCvtermpaths1().add(cvtermpaths1);
		cvtermpaths1.setCvterm1(this);

		return cvtermpaths1;
	}

	public Cvtermpath removeCvtermpaths1(Cvtermpath cvtermpaths1) {
		getCvtermpaths1().remove(cvtermpaths1);
		cvtermpaths1.setCvterm1(null);

		return cvtermpaths1;
	}

	public List<Cvtermpath> getCvtermpaths2() {
		return this.cvtermpaths2;
	}

	public void setCvtermpaths2(List<Cvtermpath> cvtermpaths2) {
		this.cvtermpaths2 = cvtermpaths2;
	}

	public Cvtermpath addCvtermpaths2(Cvtermpath cvtermpaths2) {
		getCvtermpaths2().add(cvtermpaths2);
		cvtermpaths2.setCvterm2(this);

		return cvtermpaths2;
	}

	public Cvtermpath removeCvtermpaths2(Cvtermpath cvtermpaths2) {
		getCvtermpaths2().remove(cvtermpaths2);
		cvtermpaths2.setCvterm2(null);

		return cvtermpaths2;
	}

	public List<Cvtermpath> getCvtermpaths3() {
		return this.cvtermpaths3;
	}

	public void setCvtermpaths3(List<Cvtermpath> cvtermpaths3) {
		this.cvtermpaths3 = cvtermpaths3;
	}

	public Cvtermpath addCvtermpaths3(Cvtermpath cvtermpaths3) {
		getCvtermpaths3().add(cvtermpaths3);
		cvtermpaths3.setCvterm3(this);

		return cvtermpaths3;
	}

	public Cvtermpath removeCvtermpaths3(Cvtermpath cvtermpaths3) {
		getCvtermpaths3().remove(cvtermpaths3);
		cvtermpaths3.setCvterm3(null);

		return cvtermpaths3;
	}

	public List<Cvtermprop> getCvtermprops1() {
		return this.cvtermprops1;
	}

	public void setCvtermprops1(List<Cvtermprop> cvtermprops1) {
		this.cvtermprops1 = cvtermprops1;
	}

	public Cvtermprop addCvtermprops1(Cvtermprop cvtermprops1) {
		getCvtermprops1().add(cvtermprops1);
		cvtermprops1.setCvterm1(this);

		return cvtermprops1;
	}

	public Cvtermprop removeCvtermprops1(Cvtermprop cvtermprops1) {
		getCvtermprops1().remove(cvtermprops1);
		cvtermprops1.setCvterm1(null);

		return cvtermprops1;
	}

	public List<Cvtermprop> getCvtermprops2() {
		return this.cvtermprops2;
	}

	public void setCvtermprops2(List<Cvtermprop> cvtermprops2) {
		this.cvtermprops2 = cvtermprops2;
	}

	public Cvtermprop addCvtermprops2(Cvtermprop cvtermprops2) {
		getCvtermprops2().add(cvtermprops2);
		cvtermprops2.setCvterm2(this);

		return cvtermprops2;
	}

	public Cvtermprop removeCvtermprops2(Cvtermprop cvtermprops2) {
		getCvtermprops2().remove(cvtermprops2);
		cvtermprops2.setCvterm2(null);

		return cvtermprops2;
	}

	public List<Cvtermsynonym> getCvtermsynonyms1() {
		return this.cvtermsynonyms1;
	}

	public void setCvtermsynonyms1(List<Cvtermsynonym> cvtermsynonyms1) {
		this.cvtermsynonyms1 = cvtermsynonyms1;
	}

	public Cvtermsynonym addCvtermsynonyms1(Cvtermsynonym cvtermsynonyms1) {
		getCvtermsynonyms1().add(cvtermsynonyms1);
		cvtermsynonyms1.setCvterm1(this);

		return cvtermsynonyms1;
	}

	public Cvtermsynonym removeCvtermsynonyms1(Cvtermsynonym cvtermsynonyms1) {
		getCvtermsynonyms1().remove(cvtermsynonyms1);
		cvtermsynonyms1.setCvterm1(null);

		return cvtermsynonyms1;
	}

	public List<Cvtermsynonym> getCvtermsynonyms2() {
		return this.cvtermsynonyms2;
	}

	public void setCvtermsynonyms2(List<Cvtermsynonym> cvtermsynonyms2) {
		this.cvtermsynonyms2 = cvtermsynonyms2;
	}

	public Cvtermsynonym addCvtermsynonyms2(Cvtermsynonym cvtermsynonyms2) {
		getCvtermsynonyms2().add(cvtermsynonyms2);
		cvtermsynonyms2.setCvterm2(this);

		return cvtermsynonyms2;
	}

	public Cvtermsynonym removeCvtermsynonyms2(Cvtermsynonym cvtermsynonyms2) {
		getCvtermsynonyms2().remove(cvtermsynonyms2);
		cvtermsynonyms2.setCvterm2(null);

		return cvtermsynonyms2;
	}

	public List<Dbxrefprop> getDbxrefprops() {
		return this.dbxrefprops;
	}

	public void setDbxrefprops(List<Dbxrefprop> dbxrefprops) {
		this.dbxrefprops = dbxrefprops;
	}

	public Dbxrefprop addDbxrefprop(Dbxrefprop dbxrefprop) {
		getDbxrefprops().add(dbxrefprop);
		dbxrefprop.setCvterm(this);

		return dbxrefprop;
	}

	public Dbxrefprop removeDbxrefprop(Dbxrefprop dbxrefprop) {
		getDbxrefprops().remove(dbxrefprop);
		dbxrefprop.setCvterm(null);

		return dbxrefprop;
	}

	public List<Feature> getFeatures() {
		return this.features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public Feature addFeature(Feature feature) {
		getFeatures().add(feature);
		feature.setCvterm(this);

		return feature;
	}

	public Feature removeFeature(Feature feature) {
		getFeatures().remove(feature);
		feature.setCvterm(null);

		return feature;
	}

	public List<FeatureCvterm> getFeatureCvterms() {
		return this.featureCvterms;
	}

	public void setFeatureCvterms(List<FeatureCvterm> featureCvterms) {
		this.featureCvterms = featureCvterms;
	}

	public FeatureCvterm addFeatureCvterm(FeatureCvterm featureCvterm) {
		getFeatureCvterms().add(featureCvterm);
		featureCvterm.setCvterm(this);

		return featureCvterm;
	}

	public FeatureCvterm removeFeatureCvterm(FeatureCvterm featureCvterm) {
		getFeatureCvterms().remove(featureCvterm);
		featureCvterm.setCvterm(null);

		return featureCvterm;
	}

	public List<FeatureCvtermprop> getFeatureCvtermprops() {
		return this.featureCvtermprops;
	}

	public void setFeatureCvtermprops(List<FeatureCvtermprop> featureCvtermprops) {
		this.featureCvtermprops = featureCvtermprops;
	}

	public FeatureCvtermprop addFeatureCvtermprop(FeatureCvtermprop featureCvtermprop) {
		getFeatureCvtermprops().add(featureCvtermprop);
		featureCvtermprop.setCvterm(this);

		return featureCvtermprop;
	}

	public FeatureCvtermprop removeFeatureCvtermprop(FeatureCvtermprop featureCvtermprop) {
		getFeatureCvtermprops().remove(featureCvtermprop);
		featureCvtermprop.setCvterm(null);

		return featureCvtermprop;
	}

	public List<FeatureExpressionprop> getFeatureExpressionprops() {
		return this.featureExpressionprops;
	}

	public void setFeatureExpressionprops(List<FeatureExpressionprop> featureExpressionprops) {
		this.featureExpressionprops = featureExpressionprops;
	}

	public FeatureExpressionprop addFeatureExpressionprop(FeatureExpressionprop featureExpressionprop) {
		getFeatureExpressionprops().add(featureExpressionprop);
		featureExpressionprop.setCvterm(this);

		return featureExpressionprop;
	}

	public FeatureExpressionprop removeFeatureExpressionprop(FeatureExpressionprop featureExpressionprop) {
		getFeatureExpressionprops().remove(featureExpressionprop);
		featureExpressionprop.setCvterm(null);

		return featureExpressionprop;
	}

	public List<FeatureGenotype> getFeatureGenotypes() {
		return this.featureGenotypes;
	}

	public void setFeatureGenotypes(List<FeatureGenotype> featureGenotypes) {
		this.featureGenotypes = featureGenotypes;
	}

	public FeatureGenotype addFeatureGenotype(FeatureGenotype featureGenotype) {
		getFeatureGenotypes().add(featureGenotype);
		featureGenotype.setCvterm(this);

		return featureGenotype;
	}

	public FeatureGenotype removeFeatureGenotype(FeatureGenotype featureGenotype) {
		getFeatureGenotypes().remove(featureGenotype);
		featureGenotype.setCvterm(null);

		return featureGenotype;
	}

	public List<FeaturePubprop> getFeaturePubprops() {
		return this.featurePubprops;
	}

	public void setFeaturePubprops(List<FeaturePubprop> featurePubprops) {
		this.featurePubprops = featurePubprops;
	}

	public FeaturePubprop addFeaturePubprop(FeaturePubprop featurePubprop) {
		getFeaturePubprops().add(featurePubprop);
		featurePubprop.setCvterm(this);

		return featurePubprop;
	}

	public FeaturePubprop removeFeaturePubprop(FeaturePubprop featurePubprop) {
		getFeaturePubprops().remove(featurePubprop);
		featurePubprop.setCvterm(null);

		return featurePubprop;
	}

	public List<FeatureRelationship> getFeatureRelationships() {
		return this.featureRelationships;
	}

	public void setFeatureRelationships(List<FeatureRelationship> featureRelationships) {
		this.featureRelationships = featureRelationships;
	}

	public FeatureRelationship addFeatureRelationship(FeatureRelationship featureRelationship) {
		getFeatureRelationships().add(featureRelationship);
		featureRelationship.setCvterm(this);

		return featureRelationship;
	}

	public FeatureRelationship removeFeatureRelationship(FeatureRelationship featureRelationship) {
		getFeatureRelationships().remove(featureRelationship);
		featureRelationship.setCvterm(null);

		return featureRelationship;
	}

	public List<FeatureRelationshipprop> getFeatureRelationshipprops() {
		return this.featureRelationshipprops;
	}

	public void setFeatureRelationshipprops(List<FeatureRelationshipprop> featureRelationshipprops) {
		this.featureRelationshipprops = featureRelationshipprops;
	}

	public FeatureRelationshipprop addFeatureRelationshipprop(FeatureRelationshipprop featureRelationshipprop) {
		getFeatureRelationshipprops().add(featureRelationshipprop);
		featureRelationshipprop.setCvterm(this);

		return featureRelationshipprop;
	}

	public FeatureRelationshipprop removeFeatureRelationshipprop(FeatureRelationshipprop featureRelationshipprop) {
		getFeatureRelationshipprops().remove(featureRelationshipprop);
		featureRelationshipprop.setCvterm(null);

		return featureRelationshipprop;
	}

	public List<Featureprop> getFeatureprops() {
		return this.featureprops;
	}

	public void setFeatureprops(List<Featureprop> featureprops) {
		this.featureprops = featureprops;
	}

	public Featureprop addFeatureprop(Featureprop featureprop) {
		getFeatureprops().add(featureprop);
		featureprop.setCvterm(this);

		return featureprop;
	}

	public Featureprop removeFeatureprop(Featureprop featureprop) {
		getFeatureprops().remove(featureprop);
		featureprop.setCvterm(null);

		return featureprop;
	}

	public List<GwasTrait> getGwasTraits() {
		return this.gwasTraits;
	}

	public void setGwasTraits(List<GwasTrait> gwasTraits) {
		this.gwasTraits = gwasTraits;
	}

	public GwasTrait addGwasTrait(GwasTrait gwasTrait) {
		getGwasTraits().add(gwasTrait);
		gwasTrait.setCvterm(this);

		return gwasTrait;
	}

	public GwasTrait removeGwasTrait(GwasTrait gwasTrait) {
		getGwasTraits().remove(gwasTrait);
		gwasTrait.setCvterm(null);

		return gwasTrait;
	}

	public List<Organismprop> getOrganismprops() {
		return this.organismprops;
	}

	public void setOrganismprops(List<Organismprop> organismprops) {
		this.organismprops = organismprops;
	}

	public Organismprop addOrganismprop(Organismprop organismprop) {
		getOrganismprops().add(organismprop);
		organismprop.setCvterm(this);

		return organismprop;
	}

	public Organismprop removeOrganismprop(Organismprop organismprop) {
		getOrganismprops().remove(organismprop);
		organismprop.setCvterm(null);

		return organismprop;
	}

	public List<Platform> getPlatforms() {
		return this.platforms;
	}

	public void setPlatforms(List<Platform> platforms) {
		this.platforms = platforms;
	}

	public Platform addPlatform(Platform platform) {
		getPlatforms().add(platform);
		platform.setCvterm(this);

		return platform;
	}

	public Platform removePlatform(Platform platform) {
		getPlatforms().remove(platform);
		platform.setCvterm(null);

		return platform;
	}

	public List<Pub> getPubs() {
		return this.pubs;
	}

	public void setPubs(List<Pub> pubs) {
		this.pubs = pubs;
	}

	public Pub addPub(Pub pub) {
		getPubs().add(pub);
		pub.setCvterm(this);

		return pub;
	}

	public Pub removePub(Pub pub) {
		getPubs().remove(pub);
		pub.setCvterm(null);

		return pub;
	}

	public List<PubRelationship> getPubRelationships() {
		return this.pubRelationships;
	}

	public void setPubRelationships(List<PubRelationship> pubRelationships) {
		this.pubRelationships = pubRelationships;
	}

	public PubRelationship addPubRelationship(PubRelationship pubRelationship) {
		getPubRelationships().add(pubRelationship);
		pubRelationship.setCvterm(this);

		return pubRelationship;
	}

	public PubRelationship removePubRelationship(PubRelationship pubRelationship) {
		getPubRelationships().remove(pubRelationship);
		pubRelationship.setCvterm(null);

		return pubRelationship;
	}

	public List<Pubprop> getPubprops() {
		return this.pubprops;
	}

	public void setPubprops(List<Pubprop> pubprops) {
		this.pubprops = pubprops;
	}

	public Pubprop addPubprop(Pubprop pubprop) {
		getPubprops().add(pubprop);
		pubprop.setCvterm(this);

		return pubprop;
	}

	public Pubprop removePubprop(Pubprop pubprop) {
		getPubprops().remove(pubprop);
		pubprop.setCvterm(null);

		return pubprop;
	}

	public List<SnpFeatureprop> getSnpFeatureprops() {
		return this.snpFeatureprops;
	}

	public void setSnpFeatureprops(List<SnpFeatureprop> snpFeatureprops) {
		this.snpFeatureprops = snpFeatureprops;
	}

	public SnpFeatureprop addSnpFeatureprop(SnpFeatureprop snpFeatureprop) {
		getSnpFeatureprops().add(snpFeatureprop);
		snpFeatureprop.setCvterm(this);

		return snpFeatureprop;
	}

	public SnpFeatureprop removeSnpFeatureprop(SnpFeatureprop snpFeatureprop) {
		getSnpFeatureprops().remove(snpFeatureprop);
		snpFeatureprop.setCvterm(null);

		return snpFeatureprop;
	}

	public List<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public Stock addStock(Stock stock) {
		getStocks().add(stock);
		stock.setCvterm(this);

		return stock;
	}

	public Stock removeStock(Stock stock) {
		getStocks().remove(stock);
		stock.setCvterm(null);

		return stock;
	}

	public List<StockCvterm> getStockCvterms() {
		return this.stockCvterms;
	}

	public void setStockCvterms(List<StockCvterm> stockCvterms) {
		this.stockCvterms = stockCvterms;
	}

	public StockCvterm addStockCvterm(StockCvterm stockCvterm) {
		getStockCvterms().add(stockCvterm);
		stockCvterm.setCvterm(this);

		return stockCvterm;
	}

	public StockCvterm removeStockCvterm(StockCvterm stockCvterm) {
		getStockCvterms().remove(stockCvterm);
		stockCvterm.setCvterm(null);

		return stockCvterm;
	}

	public List<StockCvtermprop> getStockCvtermprops() {
		return this.stockCvtermprops;
	}

	public void setStockCvtermprops(List<StockCvtermprop> stockCvtermprops) {
		this.stockCvtermprops = stockCvtermprops;
	}

	public StockCvtermprop addStockCvtermprop(StockCvtermprop stockCvtermprop) {
		getStockCvtermprops().add(stockCvtermprop);
		stockCvtermprop.setCvterm(this);

		return stockCvtermprop;
	}

	public StockCvtermprop removeStockCvtermprop(StockCvtermprop stockCvtermprop) {
		getStockCvtermprops().remove(stockCvtermprop);
		stockCvtermprop.setCvterm(null);

		return stockCvtermprop;
	}

	public List<StockDbxrefprop> getStockDbxrefprops() {
		return this.stockDbxrefprops;
	}

	public void setStockDbxrefprops(List<StockDbxrefprop> stockDbxrefprops) {
		this.stockDbxrefprops = stockDbxrefprops;
	}

	public StockDbxrefprop addStockDbxrefprop(StockDbxrefprop stockDbxrefprop) {
		getStockDbxrefprops().add(stockDbxrefprop);
		stockDbxrefprop.setCvterm(this);

		return stockDbxrefprop;
	}

	public StockDbxrefprop removeStockDbxrefprop(StockDbxrefprop stockDbxrefprop) {
		getStockDbxrefprops().remove(stockDbxrefprop);
		stockDbxrefprop.setCvterm(null);

		return stockDbxrefprop;
	}

	public List<StockPhenotype> getStockPhenotypes1() {
		return this.stockPhenotypes1;
	}

	public void setStockPhenotypes1(List<StockPhenotype> stockPhenotypes1) {
		this.stockPhenotypes1 = stockPhenotypes1;
	}

	public StockPhenotype addStockPhenotypes1(StockPhenotype stockPhenotypes1) {
		getStockPhenotypes1().add(stockPhenotypes1);
		stockPhenotypes1.setCvterm1(this);

		return stockPhenotypes1;
	}

	public StockPhenotype removeStockPhenotypes1(StockPhenotype stockPhenotypes1) {
		getStockPhenotypes1().remove(stockPhenotypes1);
		stockPhenotypes1.setCvterm1(null);

		return stockPhenotypes1;
	}

	public List<StockPhenotype> getStockPhenotypes2() {
		return this.stockPhenotypes2;
	}

	public void setStockPhenotypes2(List<StockPhenotype> stockPhenotypes2) {
		this.stockPhenotypes2 = stockPhenotypes2;
	}

	public StockPhenotype addStockPhenotypes2(StockPhenotype stockPhenotypes2) {
		getStockPhenotypes2().add(stockPhenotypes2);
		stockPhenotypes2.setCvterm2(this);

		return stockPhenotypes2;
	}

	public StockPhenotype removeStockPhenotypes2(StockPhenotype stockPhenotypes2) {
		getStockPhenotypes2().remove(stockPhenotypes2);
		stockPhenotypes2.setCvterm2(null);

		return stockPhenotypes2;
	}

	public List<StockRelationship> getStockRelationships() {
		return this.stockRelationships;
	}

	public void setStockRelationships(List<StockRelationship> stockRelationships) {
		this.stockRelationships = stockRelationships;
	}

	public StockRelationship addStockRelationship(StockRelationship stockRelationship) {
		getStockRelationships().add(stockRelationship);
		stockRelationship.setCvterm(this);

		return stockRelationship;
	}

	public StockRelationship removeStockRelationship(StockRelationship stockRelationship) {
		getStockRelationships().remove(stockRelationship);
		stockRelationship.setCvterm(null);

		return stockRelationship;
	}

	public List<StockRelationshipCvterm> getStockRelationshipCvterms() {
		return this.stockRelationshipCvterms;
	}

	public void setStockRelationshipCvterms(List<StockRelationshipCvterm> stockRelationshipCvterms) {
		this.stockRelationshipCvterms = stockRelationshipCvterms;
	}

	public StockRelationshipCvterm addStockRelationshipCvterm(StockRelationshipCvterm stockRelationshipCvterm) {
		getStockRelationshipCvterms().add(stockRelationshipCvterm);
		stockRelationshipCvterm.setCvterm(this);

		return stockRelationshipCvterm;
	}

	public StockRelationshipCvterm removeStockRelationshipCvterm(StockRelationshipCvterm stockRelationshipCvterm) {
		getStockRelationshipCvterms().remove(stockRelationshipCvterm);
		stockRelationshipCvterm.setCvterm(null);

		return stockRelationshipCvterm;
	}

	public List<Stockcollection> getStockcollections() {
		return this.stockcollections;
	}

	public void setStockcollections(List<Stockcollection> stockcollections) {
		this.stockcollections = stockcollections;
	}

	public Stockcollection addStockcollection(Stockcollection stockcollection) {
		getStockcollections().add(stockcollection);
		stockcollection.setCvterm(this);

		return stockcollection;
	}

	public Stockcollection removeStockcollection(Stockcollection stockcollection) {
		getStockcollections().remove(stockcollection);
		stockcollection.setCvterm(null);

		return stockcollection;
	}

	public List<Stockcollectionprop> getStockcollectionprops() {
		return this.stockcollectionprops;
	}

	public void setStockcollectionprops(List<Stockcollectionprop> stockcollectionprops) {
		this.stockcollectionprops = stockcollectionprops;
	}

	public Stockcollectionprop addStockcollectionprop(Stockcollectionprop stockcollectionprop) {
		getStockcollectionprops().add(stockcollectionprop);
		stockcollectionprop.setCvterm(this);

		return stockcollectionprop;
	}

	public Stockcollectionprop removeStockcollectionprop(Stockcollectionprop stockcollectionprop) {
		getStockcollectionprops().remove(stockcollectionprop);
		stockcollectionprop.setCvterm(null);

		return stockcollectionprop;
	}

	public List<Stockprop> getStockprops() {
		return this.stockprops;
	}

	public void setStockprops(List<Stockprop> stockprops) {
		this.stockprops = stockprops;
	}

	public Stockprop addStockprop(Stockprop stockprop) {
		getStockprops().add(stockprop);
		stockprop.setCvterm(this);

		return stockprop;
	}

	public Stockprop removeStockprop(Stockprop stockprop) {
		getStockprops().remove(stockprop);
		stockprop.setCvterm(null);

		return stockprop;
	}

	public List<Synonym> getSynonyms() {
		return this.synonyms;
	}

	public void setSynonyms(List<Synonym> synonyms) {
		this.synonyms = synonyms;
	}

	public Synonym addSynonym(Synonym synonym) {
		getSynonyms().add(synonym);
		synonym.setCvterm(this);

		return synonym;
	}

	public Synonym removeSynonym(Synonym synonym) {
		getSynonyms().remove(synonym);
		synonym.setCvterm(null);

		return synonym;
	}

	public List<VariantVariantset> getVariantVariantsets() {
		return this.variantVariantsets;
	}

	public void setVariantVariantsets(List<VariantVariantset> variantVariantsets) {
		this.variantVariantsets = variantVariantsets;
	}

	public VariantVariantset addVariantVariantset(VariantVariantset variantVariantset) {
		getVariantVariantsets().add(variantVariantset);
		variantVariantset.setCvterm(this);

		return variantVariantset;
	}

	public VariantVariantset removeVariantVariantset(VariantVariantset variantVariantset) {
		getVariantVariantsets().remove(variantVariantset);
		variantVariantset.setCvterm(null);

		return variantVariantset;
	}

	public List<Variantset> getVariantsets() {
		return this.variantsets;
	}

	public void setVariantsets(List<Variantset> variantsets) {
		this.variantsets = variantsets;
	}

	public Variantset addVariantset(Variantset variantset) {
		getVariantsets().add(variantset);
		variantset.setCvterm(this);

		return variantset;
	}

	public Variantset removeVariantset(Variantset variantset) {
		getVariantsets().remove(variantset);
		variantset.setCvterm(null);

		return variantset;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the dbxrefprop database table.
 * 
 */
@Entity
@NamedQuery(name="Dbxrefprop.findAll", query="SELECT d FROM Dbxrefprop d")
public class Dbxrefprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="dbxrefprop_id")
	private Integer dbxrefpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	public Dbxrefprop() {
	}

	public Integer getDbxrefpropId() {
		return this.dbxrefpropId;
	}

	public void setDbxrefpropId(Integer dbxrefpropId) {
		this.dbxrefpropId = dbxrefpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

}
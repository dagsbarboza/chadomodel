package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the gff_sort_tmp database table.
 * 
 */
@Entity
@Table(name="gff_sort_tmp")
@NamedQuery(name="GffSortTmp.findAll", query="SELECT g FROM GffSortTmp g")
public class GffSortTmp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="row_id")
	private Integer rowId;

	private String gffline;

	private String id;

	private String parent;

	private String refseq;

	public GffSortTmp() {
	}

	public Integer getRowId() {
		return this.rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getGffline() {
		return this.gffline;
	}

	public void setGffline(String gffline) {
		this.gffline = gffline;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParent() {
		return this.parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getRefseq() {
		return this.refseq;
	}

	public void setRefseq(String refseq) {
		this.refseq = refseq;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pubprop database table.
 * 
 */
@Entity
@NamedQuery(name="Pubprop.findAll", query="SELECT p FROM Pubprop p")
public class Pubprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pubprop_id")
	private Integer pubpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public Pubprop() {
	}

	public Integer getPubpropId() {
		return this.pubpropId;
	}

	public void setPubpropId(Integer pubpropId) {
		this.pubpropId = pubpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
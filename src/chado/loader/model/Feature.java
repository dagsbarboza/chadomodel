package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the feature database table.
 * 
 */

@Entity
@NamedQueries({ 
		@NamedQuery(name = "Feature.findAll", query = "SELECT f FROM Feature f"),
		@NamedQuery(name = "Feature.findFeatureId", query = "SELECT f FROM Feature f where f.featureId=:featureId"),
		@NamedQuery(name = "Feature.findByNameOrganismType", query = "SELECT f FROM Feature f where f.name=:name and f.cvterm=:cvterm and f.organism=:organism")

})
public class Feature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "feature_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer featureId;

	@Column(name = "is_analysis")
	private Boolean isAnalysis;

	@Column(name = "is_obsolete")
	private Boolean isObsolete;

	private String md5checksum;

	private String name;

	private String residues;

	private Integer seqlen;

	private Timestamp timeaccessioned;

	private Timestamp timelastmodified;

	private String uniquename;

	// bi-directional many-to-one association to Analysisfeature
	@OneToMany(mappedBy = "feature")
	private List<Analysisfeature> analysisfeatures;

	// bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name = "type_id")
	private Cvterm cvterm;

	// bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name = "dbxref_id")
	private Dbxref dbxref;

	// bi-directional many-to-one association to Organism
	@ManyToOne
	@JoinColumn(name = "organism_id")
	private Organism organism;

	// bi-directional many-to-one association to FeatureCvterm
	@OneToMany(mappedBy = "feature")
	private List<FeatureCvterm> featureCvterms;

	// bi-directional many-to-one association to FeatureDbxref
	@OneToMany(mappedBy = "feature")
	private List<FeatureDbxref> featureDbxrefs;

	// bi-directional many-to-one association to FeatureExpression
	@OneToMany(mappedBy = "feature")
	private List<FeatureExpression> featureExpressions;

	// bi-directional many-to-one association to FeatureGenotype
	@OneToMany(mappedBy = "feature1")
	private List<FeatureGenotype> featureGenotypes1;

	// bi-directional many-to-one association to FeatureGenotype
	@OneToMany(mappedBy = "feature2")
	private List<FeatureGenotype> featureGenotypes2;

	// bi-directional many-to-one association to FeaturePhenotype
	@OneToMany(mappedBy = "feature")
	private List<FeaturePhenotype> featurePhenotypes;

	// bi-directional many-to-one association to FeaturePub
	@OneToMany(mappedBy = "feature")
	private List<FeaturePub> featurePubs;

	// bi-directional many-to-one association to FeatureRelationship
	@OneToMany(mappedBy = "feature1")
	private List<FeatureRelationship> featureRelationships1;

	// bi-directional many-to-one association to FeatureRelationship
	@OneToMany(mappedBy = "feature2")
	private List<FeatureRelationship> featureRelationships2;

	// bi-directional many-to-one association to Featureloc
	@OneToMany(mappedBy = "feature1")
	private List<Featureloc> featurelocs1;

	// bi-directional many-to-one association to Featureloc
	@OneToMany(mappedBy = "feature2")
	private List<Featureloc> featurelocs2;

	// bi-directional many-to-one association to Featureprop
	@OneToMany(mappedBy = "feature")
	private List<Featureprop> featureprops;

	// bi-directional many-to-one association to IndelFeatureloc
	@OneToMany(mappedBy = "feature")
	private List<IndelFeatureloc> indelFeaturelocs;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "feature1")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs1;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "feature2")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs2;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "feature3")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs3;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "feature4")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs4;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "feature5")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs5;

	// bi-directional many-to-one association to MvConvertposNb2allref
	@OneToMany(mappedBy = "feature6")
	private List<MvConvertposNb2allref> mvConvertposNb2allrefs6;

	// bi-directional many-to-one association to SnpFeatureloc
	@OneToMany(mappedBy = "feature")
	private List<SnpFeatureloc> snpFeaturelocs;

	public Feature() {
		Date date = new Date();

		setIsAnalysis(false);
		setIsObsolete(false);

		setTimeaccessioned(new Timestamp(date.getTime()));
		setTimelastmodified(new Timestamp(date.getTime()));
	}

	public Integer getFeatureId() {
		return this.featureId;
	}

	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}

	public Boolean getIsAnalysis() {
		return this.isAnalysis;
	}

	public void setIsAnalysis(Boolean isAnalysis) {
		this.isAnalysis = isAnalysis;
	}

	public Boolean getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Boolean isObsolete) {
		this.isObsolete = isObsolete;
	}

	public String getMd5checksum() {
		return this.md5checksum;
	}

	public void setMd5checksum(String md5checksum) {
		this.md5checksum = md5checksum;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResidues() {
		return this.residues;
	}

	public void setResidues(String residues) {
		this.residues = residues;
	}

	public Integer getSeqlen() {
		return this.seqlen;
	}

	public void setSeqlen(Integer seqlen) {
		this.seqlen = seqlen;
	}

	public Timestamp getTimeaccessioned() {
		return this.timeaccessioned;
	}

	public void setTimeaccessioned(Timestamp timeaccessioned) {
		this.timeaccessioned = timeaccessioned;
	}

	public Timestamp getTimelastmodified() {
		return this.timelastmodified;
	}

	public void setTimelastmodified(Timestamp timelastmodified) {
		this.timelastmodified = timelastmodified;
	}

	public String getUniquename() {
		return this.uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

	public List<Analysisfeature> getAnalysisfeatures() {
		return this.analysisfeatures;
	}

	public void setAnalysisfeatures(List<Analysisfeature> analysisfeatures) {
		this.analysisfeatures = analysisfeatures;
	}

	public Analysisfeature addAnalysisfeature(Analysisfeature analysisfeature) {
		getAnalysisfeatures().add(analysisfeature);
		analysisfeature.setFeature(this);

		return analysisfeature;
	}

	public Analysisfeature removeAnalysisfeature(Analysisfeature analysisfeature) {
		getAnalysisfeatures().remove(analysisfeature);
		analysisfeature.setFeature(null);

		return analysisfeature;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Organism getOrganism() {
		return this.organism;
	}

	public void setOrganism(Organism organism) {
		this.organism = organism;
	}

	public List<FeatureCvterm> getFeatureCvterms() {
		return this.featureCvterms;
	}

	public void setFeatureCvterms(List<FeatureCvterm> featureCvterms) {
		this.featureCvterms = featureCvterms;
	}

	public FeatureCvterm addFeatureCvterm(FeatureCvterm featureCvterm) {
		getFeatureCvterms().add(featureCvterm);
		featureCvterm.setFeature(this);

		return featureCvterm;
	}

	public FeatureCvterm removeFeatureCvterm(FeatureCvterm featureCvterm) {
		getFeatureCvterms().remove(featureCvterm);
		featureCvterm.setFeature(null);

		return featureCvterm;
	}

	public List<FeatureDbxref> getFeatureDbxrefs() {
		return this.featureDbxrefs;
	}

	public void setFeatureDbxrefs(List<FeatureDbxref> featureDbxrefs) {
		this.featureDbxrefs = featureDbxrefs;
	}

	public FeatureDbxref addFeatureDbxref(FeatureDbxref featureDbxref) {
		getFeatureDbxrefs().add(featureDbxref);
		featureDbxref.setFeature(this);

		return featureDbxref;
	}

	public FeatureDbxref removeFeatureDbxref(FeatureDbxref featureDbxref) {
		getFeatureDbxrefs().remove(featureDbxref);
		featureDbxref.setFeature(null);

		return featureDbxref;
	}

	public List<FeatureExpression> getFeatureExpressions() {
		return this.featureExpressions;
	}

	public void setFeatureExpressions(List<FeatureExpression> featureExpressions) {
		this.featureExpressions = featureExpressions;
	}

	public FeatureExpression addFeatureExpression(FeatureExpression featureExpression) {
		getFeatureExpressions().add(featureExpression);
		featureExpression.setFeature(this);

		return featureExpression;
	}

	public FeatureExpression removeFeatureExpression(FeatureExpression featureExpression) {
		getFeatureExpressions().remove(featureExpression);
		featureExpression.setFeature(null);

		return featureExpression;
	}

	public List<FeatureGenotype> getFeatureGenotypes1() {
		return this.featureGenotypes1;
	}

	public void setFeatureGenotypes1(List<FeatureGenotype> featureGenotypes1) {
		this.featureGenotypes1 = featureGenotypes1;
	}

	public FeatureGenotype addFeatureGenotypes1(FeatureGenotype featureGenotypes1) {
		getFeatureGenotypes1().add(featureGenotypes1);
		featureGenotypes1.setFeature1(this);

		return featureGenotypes1;
	}

	public FeatureGenotype removeFeatureGenotypes1(FeatureGenotype featureGenotypes1) {
		getFeatureGenotypes1().remove(featureGenotypes1);
		featureGenotypes1.setFeature1(null);

		return featureGenotypes1;
	}

	public List<FeatureGenotype> getFeatureGenotypes2() {
		return this.featureGenotypes2;
	}

	public void setFeatureGenotypes2(List<FeatureGenotype> featureGenotypes2) {
		this.featureGenotypes2 = featureGenotypes2;
	}

	public FeatureGenotype addFeatureGenotypes2(FeatureGenotype featureGenotypes2) {
		getFeatureGenotypes2().add(featureGenotypes2);
		featureGenotypes2.setFeature2(this);

		return featureGenotypes2;
	}

	public FeatureGenotype removeFeatureGenotypes2(FeatureGenotype featureGenotypes2) {
		getFeatureGenotypes2().remove(featureGenotypes2);
		featureGenotypes2.setFeature2(null);

		return featureGenotypes2;
	}

	public List<FeaturePhenotype> getFeaturePhenotypes() {
		return this.featurePhenotypes;
	}

	public void setFeaturePhenotypes(List<FeaturePhenotype> featurePhenotypes) {
		this.featurePhenotypes = featurePhenotypes;
	}

	public FeaturePhenotype addFeaturePhenotype(FeaturePhenotype featurePhenotype) {
		getFeaturePhenotypes().add(featurePhenotype);
		featurePhenotype.setFeature(this);

		return featurePhenotype;
	}

	public FeaturePhenotype removeFeaturePhenotype(FeaturePhenotype featurePhenotype) {
		getFeaturePhenotypes().remove(featurePhenotype);
		featurePhenotype.setFeature(null);

		return featurePhenotype;
	}

	public List<FeaturePub> getFeaturePubs() {
		return this.featurePubs;
	}

	public void setFeaturePubs(List<FeaturePub> featurePubs) {
		this.featurePubs = featurePubs;
	}

	public FeaturePub addFeaturePub(FeaturePub featurePub) {
		getFeaturePubs().add(featurePub);
		featurePub.setFeature(this);

		return featurePub;
	}

	public FeaturePub removeFeaturePub(FeaturePub featurePub) {
		getFeaturePubs().remove(featurePub);
		featurePub.setFeature(null);

		return featurePub;
	}

	public List<FeatureRelationship> getFeatureRelationships1() {
		return this.featureRelationships1;
	}

	public void setFeatureRelationships1(List<FeatureRelationship> featureRelationships1) {
		this.featureRelationships1 = featureRelationships1;
	}

	public FeatureRelationship addFeatureRelationships1(FeatureRelationship featureRelationships1) {
		getFeatureRelationships1().add(featureRelationships1);
		featureRelationships1.setFeature1(this);

		return featureRelationships1;
	}

	public FeatureRelationship removeFeatureRelationships1(FeatureRelationship featureRelationships1) {
		getFeatureRelationships1().remove(featureRelationships1);
		featureRelationships1.setFeature1(null);

		return featureRelationships1;
	}

	public List<FeatureRelationship> getFeatureRelationships2() {
		return this.featureRelationships2;
	}

	public void setFeatureRelationships2(List<FeatureRelationship> featureRelationships2) {
		this.featureRelationships2 = featureRelationships2;
	}

	public FeatureRelationship addFeatureRelationships2(FeatureRelationship featureRelationships2) {
		getFeatureRelationships2().add(featureRelationships2);
		featureRelationships2.setFeature2(this);

		return featureRelationships2;
	}

	public FeatureRelationship removeFeatureRelationships2(FeatureRelationship featureRelationships2) {
		getFeatureRelationships2().remove(featureRelationships2);
		featureRelationships2.setFeature2(null);

		return featureRelationships2;
	}

	public List<Featureloc> getFeaturelocs1() {
		return this.featurelocs1;
	}

	public void setFeaturelocs1(List<Featureloc> featurelocs1) {
		this.featurelocs1 = featurelocs1;
	}

	public Featureloc addFeaturelocs1(Featureloc featurelocs1) {
		getFeaturelocs1().add(featurelocs1);
		featurelocs1.setFeature1(this);

		return featurelocs1;
	}

	public Featureloc removeFeaturelocs1(Featureloc featurelocs1) {
		getFeaturelocs1().remove(featurelocs1);
		featurelocs1.setFeature1(null);

		return featurelocs1;
	}

	public List<Featureloc> getFeaturelocs2() {
		return this.featurelocs2;
	}

	public void setFeaturelocs2(List<Featureloc> featurelocs2) {
		this.featurelocs2 = featurelocs2;
	}

	public Featureloc addFeaturelocs2(Featureloc featurelocs2) {
		getFeaturelocs2().add(featurelocs2);
		featurelocs2.setFeature2(this);

		return featurelocs2;
	}

	public Featureloc removeFeaturelocs2(Featureloc featurelocs2) {
		getFeaturelocs2().remove(featurelocs2);
		featurelocs2.setFeature2(null);

		return featurelocs2;
	}

	public List<Featureprop> getFeatureprops() {
		return this.featureprops;
	}

	public void setFeatureprops(List<Featureprop> featureprops) {
		this.featureprops = featureprops;
	}

	public Featureprop addFeatureprop(Featureprop featureprop) {
		getFeatureprops().add(featureprop);
		featureprop.setFeature(this);

		return featureprop;
	}

	public Featureprop removeFeatureprop(Featureprop featureprop) {
		getFeatureprops().remove(featureprop);
		featureprop.setFeature(null);

		return featureprop;
	}

	public List<IndelFeatureloc> getIndelFeaturelocs() {
		return this.indelFeaturelocs;
	}

	public void setIndelFeaturelocs(List<IndelFeatureloc> indelFeaturelocs) {
		this.indelFeaturelocs = indelFeaturelocs;
	}

	public IndelFeatureloc addIndelFeatureloc(IndelFeatureloc indelFeatureloc) {
		getIndelFeaturelocs().add(indelFeatureloc);
		indelFeatureloc.setFeature(this);

		return indelFeatureloc;
	}

	public IndelFeatureloc removeIndelFeatureloc(IndelFeatureloc indelFeatureloc) {
		getIndelFeaturelocs().remove(indelFeatureloc);
		indelFeatureloc.setFeature(null);

		return indelFeatureloc;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs1() {
		return this.mvConvertposNb2allrefs1;
	}

	public void setMvConvertposNb2allrefs1(List<MvConvertposNb2allref> mvConvertposNb2allrefs1) {
		this.mvConvertposNb2allrefs1 = mvConvertposNb2allrefs1;
	}

	public MvConvertposNb2allref addMvConvertposNb2allrefs1(MvConvertposNb2allref mvConvertposNb2allrefs1) {
		getMvConvertposNb2allrefs1().add(mvConvertposNb2allrefs1);
		mvConvertposNb2allrefs1.setFeature1(this);

		return mvConvertposNb2allrefs1;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allrefs1(MvConvertposNb2allref mvConvertposNb2allrefs1) {
		getMvConvertposNb2allrefs1().remove(mvConvertposNb2allrefs1);
		mvConvertposNb2allrefs1.setFeature1(null);

		return mvConvertposNb2allrefs1;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs2() {
		return this.mvConvertposNb2allrefs2;
	}

	public void setMvConvertposNb2allrefs2(List<MvConvertposNb2allref> mvConvertposNb2allrefs2) {
		this.mvConvertposNb2allrefs2 = mvConvertposNb2allrefs2;
	}

	public MvConvertposNb2allref addMvConvertposNb2allrefs2(MvConvertposNb2allref mvConvertposNb2allrefs2) {
		getMvConvertposNb2allrefs2().add(mvConvertposNb2allrefs2);
		mvConvertposNb2allrefs2.setFeature2(this);

		return mvConvertposNb2allrefs2;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allrefs2(MvConvertposNb2allref mvConvertposNb2allrefs2) {
		getMvConvertposNb2allrefs2().remove(mvConvertposNb2allrefs2);
		mvConvertposNb2allrefs2.setFeature2(null);

		return mvConvertposNb2allrefs2;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs3() {
		return this.mvConvertposNb2allrefs3;
	}

	public void setMvConvertposNb2allrefs3(List<MvConvertposNb2allref> mvConvertposNb2allrefs3) {
		this.mvConvertposNb2allrefs3 = mvConvertposNb2allrefs3;
	}

	public MvConvertposNb2allref addMvConvertposNb2allrefs3(MvConvertposNb2allref mvConvertposNb2allrefs3) {
		getMvConvertposNb2allrefs3().add(mvConvertposNb2allrefs3);
		mvConvertposNb2allrefs3.setFeature3(this);

		return mvConvertposNb2allrefs3;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allrefs3(MvConvertposNb2allref mvConvertposNb2allrefs3) {
		getMvConvertposNb2allrefs3().remove(mvConvertposNb2allrefs3);
		mvConvertposNb2allrefs3.setFeature3(null);

		return mvConvertposNb2allrefs3;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs4() {
		return this.mvConvertposNb2allrefs4;
	}

	public void setMvConvertposNb2allrefs4(List<MvConvertposNb2allref> mvConvertposNb2allrefs4) {
		this.mvConvertposNb2allrefs4 = mvConvertposNb2allrefs4;
	}

	public MvConvertposNb2allref addMvConvertposNb2allrefs4(MvConvertposNb2allref mvConvertposNb2allrefs4) {
		getMvConvertposNb2allrefs4().add(mvConvertposNb2allrefs4);
		mvConvertposNb2allrefs4.setFeature4(this);

		return mvConvertposNb2allrefs4;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allrefs4(MvConvertposNb2allref mvConvertposNb2allrefs4) {
		getMvConvertposNb2allrefs4().remove(mvConvertposNb2allrefs4);
		mvConvertposNb2allrefs4.setFeature4(null);

		return mvConvertposNb2allrefs4;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs5() {
		return this.mvConvertposNb2allrefs5;
	}

	public void setMvConvertposNb2allrefs5(List<MvConvertposNb2allref> mvConvertposNb2allrefs5) {
		this.mvConvertposNb2allrefs5 = mvConvertposNb2allrefs5;
	}

	public MvConvertposNb2allref addMvConvertposNb2allrefs5(MvConvertposNb2allref mvConvertposNb2allrefs5) {
		getMvConvertposNb2allrefs5().add(mvConvertposNb2allrefs5);
		mvConvertposNb2allrefs5.setFeature5(this);

		return mvConvertposNb2allrefs5;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allrefs5(MvConvertposNb2allref mvConvertposNb2allrefs5) {
		getMvConvertposNb2allrefs5().remove(mvConvertposNb2allrefs5);
		mvConvertposNb2allrefs5.setFeature5(null);

		return mvConvertposNb2allrefs5;
	}

	public List<MvConvertposNb2allref> getMvConvertposNb2allrefs6() {
		return this.mvConvertposNb2allrefs6;
	}

	public void setMvConvertposNb2allrefs6(List<MvConvertposNb2allref> mvConvertposNb2allrefs6) {
		this.mvConvertposNb2allrefs6 = mvConvertposNb2allrefs6;
	}

	public MvConvertposNb2allref addMvConvertposNb2allrefs6(MvConvertposNb2allref mvConvertposNb2allrefs6) {
		getMvConvertposNb2allrefs6().add(mvConvertposNb2allrefs6);
		mvConvertposNb2allrefs6.setFeature6(this);

		return mvConvertposNb2allrefs6;
	}

	public MvConvertposNb2allref removeMvConvertposNb2allrefs6(MvConvertposNb2allref mvConvertposNb2allrefs6) {
		getMvConvertposNb2allrefs6().remove(mvConvertposNb2allrefs6);
		mvConvertposNb2allrefs6.setFeature6(null);

		return mvConvertposNb2allrefs6;
	}

	public List<SnpFeatureloc> getSnpFeaturelocs() {
		return this.snpFeaturelocs;
	}

	public void setSnpFeaturelocs(List<SnpFeatureloc> snpFeaturelocs) {
		this.snpFeaturelocs = snpFeaturelocs;
	}

	public SnpFeatureloc addSnpFeatureloc(SnpFeatureloc snpFeatureloc) {
		getSnpFeaturelocs().add(snpFeatureloc);
		snpFeatureloc.setFeature(this);

		return snpFeatureloc;
	}

	public SnpFeatureloc removeSnpFeatureloc(SnpFeatureloc snpFeatureloc) {
		getSnpFeaturelocs().remove(snpFeatureloc);
		snpFeatureloc.setFeature(null);

		return snpFeatureloc;
	}

}
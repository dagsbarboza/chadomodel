package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stockprop_pub database table.
 * 
 */
@Entity
@Table(name="stockprop_pub")
@NamedQuery(name="StockpropPub.findAll", query="SELECT s FROM StockpropPub s")
public class StockpropPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stockprop_pub_id")
	private Integer stockpropPubId;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to Stockprop
	@ManyToOne
	@JoinColumn(name="stockprop_id")
	private Stockprop stockprop;

	public StockpropPub() {
	}

	public Integer getStockpropPubId() {
		return this.stockpropPubId;
	}

	public void setStockpropPubId(Integer stockpropPubId) {
		this.stockpropPubId = stockpropPubId;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public Stockprop getStockprop() {
		return this.stockprop;
	}

	public void setStockprop(Stockprop stockprop) {
		this.stockprop = stockprop;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the materialized_view database table.
 * 
 */
@Entity
@Table(name="materialized_view")
@NamedQuery(name="MaterializedView.findAll", query="SELECT m FROM MaterializedView m")
public class MaterializedView implements Serializable {
	private static final long serialVersionUID = 1L;

	private String indexed;

	@Column(name="last_update")
	private Timestamp lastUpdate;

	@Id
	@Column(name="materialized_view_id")
	private Integer materializedViewId;

	@Column(name="mv_schema")
	private String mvSchema;

	@Column(name="mv_specs")
	private String mvSpecs;

	@Column(name="mv_table")
	private String mvTable;

	private String name;

	private String query;

	@Column(name="refresh_time")
	private Integer refreshTime;

	@Column(name="special_index")
	private String specialIndex;

	public MaterializedView() {
	}

	public String getIndexed() {
		return this.indexed;
	}

	public void setIndexed(String indexed) {
		this.indexed = indexed;
	}

	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Integer getMaterializedViewId() {
		return this.materializedViewId;
	}

	public void setMaterializedViewId(Integer materializedViewId) {
		this.materializedViewId = materializedViewId;
	}

	public String getMvSchema() {
		return this.mvSchema;
	}

	public void setMvSchema(String mvSchema) {
		this.mvSchema = mvSchema;
	}

	public String getMvSpecs() {
		return this.mvSpecs;
	}

	public void setMvSpecs(String mvSpecs) {
		this.mvSpecs = mvSpecs;
	}

	public String getMvTable() {
		return this.mvTable;
	}

	public void setMvTable(String mvTable) {
		this.mvTable = mvTable;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuery() {
		return this.query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Integer getRefreshTime() {
		return this.refreshTime;
	}

	public void setRefreshTime(Integer refreshTime) {
		this.refreshTime = refreshTime;
	}

	public String getSpecialIndex() {
		return this.specialIndex;
	}

	public void setSpecialIndex(String specialIndex) {
		this.specialIndex = specialIndex;
	}

}
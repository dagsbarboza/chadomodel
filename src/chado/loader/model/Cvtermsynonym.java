package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvtermsynonym database table.
 * 
 */
@Entity
@NamedQuery(name="Cvtermsynonym.findAll", query="SELECT c FROM Cvtermsynonym c")
public class Cvtermsynonym implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvtermsynonym_id")
	private Integer cvtermsynonymId;

	private String synonym;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm1;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm2;

	public Cvtermsynonym() {
	}

	public Integer getCvtermsynonymId() {
		return this.cvtermsynonymId;
	}

	public void setCvtermsynonymId(Integer cvtermsynonymId) {
		this.cvtermsynonymId = cvtermsynonymId;
	}

	public String getSynonym() {
		return this.synonym;
	}

	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}

	public Cvterm getCvterm1() {
		return this.cvterm1;
	}

	public void setCvterm1(Cvterm cvterm1) {
		this.cvterm1 = cvterm1;
	}

	public Cvterm getCvterm2() {
		return this.cvterm2;
	}

	public void setCvterm2(Cvterm cvterm2) {
		this.cvterm2 = cvterm2;
	}

}
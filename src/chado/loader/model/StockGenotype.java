package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stock_genotype database table.
 * 
 */
@Entity
@Table(name="stock_genotype")
@NamedQuery(name="StockGenotype.findAll", query="SELECT s FROM StockGenotype s")
public class StockGenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_genotype_id")
	private Integer stockGenotypeId;

	@Column(name="genotype_id")
	private Integer genotypeId;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	public StockGenotype() {
	}

	public Integer getStockGenotypeId() {
		return this.stockGenotypeId;
	}

	public void setStockGenotypeId(Integer stockGenotypeId) {
		this.stockGenotypeId = stockGenotypeId;
	}

	public Integer getGenotypeId() {
		return this.genotypeId;
	}

	public void setGenotypeId(Integer genotypeId) {
		this.genotypeId = genotypeId;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
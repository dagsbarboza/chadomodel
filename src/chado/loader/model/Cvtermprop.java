package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvtermprop database table.
 * 
 */
@Entity
@NamedQuery(name="Cvtermprop.findAll", query="SELECT c FROM Cvtermprop c")
public class Cvtermprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvtermprop_id")
	private Integer cvtermpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm1;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm2;

	public Cvtermprop() {
	}

	public Integer getCvtermpropId() {
		return this.cvtermpropId;
	}

	public void setCvtermpropId(Integer cvtermpropId) {
		this.cvtermpropId = cvtermpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm1() {
		return this.cvterm1;
	}

	public void setCvterm1(Cvterm cvterm1) {
		this.cvterm1 = cvterm1;
	}

	public Cvterm getCvterm2() {
		return this.cvterm2;
	}

	public void setCvterm2(Cvterm cvterm2) {
		this.cvterm2 = cvterm2;
	}

}
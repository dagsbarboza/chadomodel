package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_synonym database table.
 * 
 */
@Entity
@Table(name="feature_synonym")
@NamedQuery(name="FeatureSynonym.findAll", query="SELECT f FROM FeatureSynonym f")
public class FeatureSynonym implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="feature_id")
	private Integer featureId;

	@Id
	@Column(name="feature_synonym_id")
	private Integer featureSynonymId;

	@Column(name="is_current")
	private Boolean isCurrent;

	@Column(name="is_internal")
	private Boolean isInternal;

	@Column(name="pub_id")
	private Integer pubId;

	@Column(name="synonym_id")
	private Integer synonymId;

	public FeatureSynonym() {
	}

	public Integer getFeatureId() {
		return this.featureId;
	}

	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}

	public Integer getFeatureSynonymId() {
		return this.featureSynonymId;
	}

	public void setFeatureSynonymId(Integer featureSynonymId) {
		this.featureSynonymId = featureSynonymId;
	}

	public Boolean getIsCurrent() {
		return this.isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Boolean getIsInternal() {
		return this.isInternal;
	}

	public void setIsInternal(Boolean isInternal) {
		this.isInternal = isInternal;
	}

	public Integer getPubId() {
		return this.pubId;
	}

	public void setPubId(Integer pubId) {
		this.pubId = pubId;
	}

	public Integer getSynonymId() {
		return this.synonymId;
	}

	public void setSynonymId(Integer synonymId) {
		this.synonymId = synonymId;
	}

}
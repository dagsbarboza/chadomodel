package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stock_relationship_cvterm database table.
 * 
 */
@Entity
@Table(name="stock_relationship_cvterm")
@NamedQuery(name="StockRelationshipCvterm.findAll", query="SELECT s FROM StockRelationshipCvterm s")
public class StockRelationshipCvterm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_relationship_cvterm_id")
	private Integer stockRelationshipCvtermId;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to StockRelationship
	@ManyToOne
	@JoinColumn(name="stock_relationship_id")
	private StockRelationship stockRelationship;

	public StockRelationshipCvterm() {
	}

	public Integer getStockRelationshipCvtermId() {
		return this.stockRelationshipCvtermId;
	}

	public void setStockRelationshipCvtermId(Integer stockRelationshipCvtermId) {
		this.stockRelationshipCvtermId = stockRelationshipCvtermId;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public StockRelationship getStockRelationship() {
		return this.stockRelationship;
	}

	public void setStockRelationship(StockRelationship stockRelationship) {
		this.stockRelationship = stockRelationship;
	}

}
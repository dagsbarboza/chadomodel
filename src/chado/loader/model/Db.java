package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.NamedNativeQueries;

import java.util.List;

/**
 * The persistent class for the db database table.
 * 
 */
@Entity
@Table(name = "Db")
@NamedQueries({
	@NamedQuery(name="Db.findById", query="SELECT d FROM Db d where d.dbId =:dbId"),
	@NamedQuery(name="Db.findAll", query="SELECT d FROM Db d"),
	@NamedQuery(name="Db.findAllName", query="SELECT d.name FROM Db d order by d.name"),
	@NamedQuery(name="Db.findByName", query="SELECT d FROM Db d where d.name =:name"),
	@NamedQuery(name="Db.findNameByName", query="SELECT d.name FROM Db d where d.name =:name")
})
public class Db implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="db_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbId;

	private String description;

	private String name;

	private String url;

	private String urlprefix;

	//bi-directional many-to-one association to Dbxref
	@OneToMany(mappedBy="db")
	private List<Dbxref> dbxrefs;

	//bi-directional many-to-one association to Platform
	@OneToMany(mappedBy="db")
	private List<Platform> platforms;

	public Db() {
	}

	public Integer getDbId() {
		return this.dbId;
	}

	public void setDbId(Integer dbId) {
		this.dbId = dbId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlprefix() {
		return this.urlprefix;
	}

	public void setUrlprefix(String urlprefix) {
		this.urlprefix = urlprefix;
	}

	public List<Dbxref> getDbxrefs() {
		return this.dbxrefs;
	}

	public void setDbxrefs(List<Dbxref> dbxrefs) {
		this.dbxrefs = dbxrefs;
	}

	public Dbxref addDbxref(Dbxref dbxref) {
		getDbxrefs().add(dbxref);
		dbxref.setDb(this);

		return dbxref;
	}

	public Dbxref removeDbxref(Dbxref dbxref) {
		getDbxrefs().remove(dbxref);
		dbxref.setDb(null);

		return dbxref;
	}

	public List<Platform> getPlatforms() {
		return this.platforms;
	}

	public void setPlatforms(List<Platform> platforms) {
		this.platforms = platforms;
	}

	public Platform addPlatform(Platform platform) {
		getPlatforms().add(platform);
		platform.setDb(this);

		return platform;
	}

	public Platform removePlatform(Platform platform) {
		getPlatforms().remove(platform);
		platform.setDb(null);

		return platform;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvtermpath database table.
 * 
 */
@Entity
@NamedQuery(name="Cvtermpath.findAll", query="SELECT c FROM Cvtermpath c")
public class Cvtermpath implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvtermpath_id")
	private Integer cvtermpathId;

	private Integer pathdistance;

	//bi-directional many-to-one association to Cv
	@ManyToOne
	@JoinColumn(name="cv_id")
	private Cv cv;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="object_id")
	private Cvterm cvterm1;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Cvterm cvterm2;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm3;

	public Cvtermpath() {
	}

	public Integer getCvtermpathId() {
		return this.cvtermpathId;
	}

	public void setCvtermpathId(Integer cvtermpathId) {
		this.cvtermpathId = cvtermpathId;
	}

	public Integer getPathdistance() {
		return this.pathdistance;
	}

	public void setPathdistance(Integer pathdistance) {
		this.pathdistance = pathdistance;
	}

	public Cv getCv() {
		return this.cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}

	public Cvterm getCvterm1() {
		return this.cvterm1;
	}

	public void setCvterm1(Cvterm cvterm1) {
		this.cvterm1 = cvterm1;
	}

	public Cvterm getCvterm2() {
		return this.cvterm2;
	}

	public void setCvterm2(Cvterm cvterm2) {
		this.cvterm2 = cvterm2;
	}

	public Cvterm getCvterm3() {
		return this.cvterm3;
	}

	public void setCvterm3(Cvterm cvterm3) {
		this.cvterm3 = cvterm3;
	}

}
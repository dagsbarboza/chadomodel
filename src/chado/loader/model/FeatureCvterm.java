package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the feature_cvterm database table.
 * 
 */
@Entity
@Table(name="feature_cvterm")
@NamedQuery(name="FeatureCvterm.findAll", query="SELECT f FROM FeatureCvterm f")
public class FeatureCvterm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_cvterm_id")
	private Integer featureCvtermId;

	@Column(name="is_not")
	private Boolean isNot;

	private Integer rank;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="cvterm_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to FeatureCvtermDbxref
	@OneToMany(mappedBy="featureCvterm")
	private List<FeatureCvtermDbxref> featureCvtermDbxrefs;

	//bi-directional many-to-one association to FeatureCvtermPub
	@OneToMany(mappedBy="featureCvterm")
	private List<FeatureCvtermPub> featureCvtermPubs;

	//bi-directional many-to-one association to FeatureCvtermprop
	@OneToMany(mappedBy="featureCvterm")
	private List<FeatureCvtermprop> featureCvtermprops;

	public FeatureCvterm() {
	}

	public Integer getFeatureCvtermId() {
		return this.featureCvtermId;
	}

	public void setFeatureCvtermId(Integer featureCvtermId) {
		this.featureCvtermId = featureCvtermId;
	}

	public Boolean getIsNot() {
		return this.isNot;
	}

	public void setIsNot(Boolean isNot) {
		this.isNot = isNot;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public List<FeatureCvtermDbxref> getFeatureCvtermDbxrefs() {
		return this.featureCvtermDbxrefs;
	}

	public void setFeatureCvtermDbxrefs(List<FeatureCvtermDbxref> featureCvtermDbxrefs) {
		this.featureCvtermDbxrefs = featureCvtermDbxrefs;
	}

	public FeatureCvtermDbxref addFeatureCvtermDbxref(FeatureCvtermDbxref featureCvtermDbxref) {
		getFeatureCvtermDbxrefs().add(featureCvtermDbxref);
		featureCvtermDbxref.setFeatureCvterm(this);

		return featureCvtermDbxref;
	}

	public FeatureCvtermDbxref removeFeatureCvtermDbxref(FeatureCvtermDbxref featureCvtermDbxref) {
		getFeatureCvtermDbxrefs().remove(featureCvtermDbxref);
		featureCvtermDbxref.setFeatureCvterm(null);

		return featureCvtermDbxref;
	}

	public List<FeatureCvtermPub> getFeatureCvtermPubs() {
		return this.featureCvtermPubs;
	}

	public void setFeatureCvtermPubs(List<FeatureCvtermPub> featureCvtermPubs) {
		this.featureCvtermPubs = featureCvtermPubs;
	}

	public FeatureCvtermPub addFeatureCvtermPub(FeatureCvtermPub featureCvtermPub) {
		getFeatureCvtermPubs().add(featureCvtermPub);
		featureCvtermPub.setFeatureCvterm(this);

		return featureCvtermPub;
	}

	public FeatureCvtermPub removeFeatureCvtermPub(FeatureCvtermPub featureCvtermPub) {
		getFeatureCvtermPubs().remove(featureCvtermPub);
		featureCvtermPub.setFeatureCvterm(null);

		return featureCvtermPub;
	}

	public List<FeatureCvtermprop> getFeatureCvtermprops() {
		return this.featureCvtermprops;
	}

	public void setFeatureCvtermprops(List<FeatureCvtermprop> featureCvtermprops) {
		this.featureCvtermprops = featureCvtermprops;
	}

	public FeatureCvtermprop addFeatureCvtermprop(FeatureCvtermprop featureCvtermprop) {
		getFeatureCvtermprops().add(featureCvtermprop);
		featureCvtermprop.setFeatureCvterm(this);

		return featureCvtermprop;
	}

	public FeatureCvtermprop removeFeatureCvtermprop(FeatureCvtermprop featureCvtermprop) {
		getFeatureCvtermprops().remove(featureCvtermprop);
		featureCvtermprop.setFeatureCvterm(null);

		return featureCvtermprop;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the platform database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Platform.findAll", query="SELECT p FROM Platform p"),
	@NamedQuery(name="Platform.findByPlatformId", query="SELECT p FROM Platform p where p.platformId=:platformId")
})

public class Platform implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="platform_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer platformId;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="genotyping_method_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Db
	@ManyToOne
	@JoinColumn(name="db_id")
	private Db db;

	//bi-directional many-to-one association to Variantset
	@ManyToOne
	@JoinColumn(name="variantset_id")
	private Variantset variantset;

	public Platform() {
	}

	public Integer getPlatformId() {
		return this.platformId;
	}

	public void setPlatformId(Integer platformId) {
		this.platformId = platformId;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Db getDb() {
		return this.db;
	}

	public void setDb(Db db) {
		this.db = db;
	}

	public Variantset getVariantset() {
		return this.variantset;
	}

	public void setVariantset(Variantset variantset) {
		this.variantset = variantset;
	}

}
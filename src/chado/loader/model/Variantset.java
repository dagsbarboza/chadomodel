package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the variantset database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Variantset.findAll", query="SELECT v FROM Variantset v"),
	@NamedQuery(name="Variantset.findVarientSetName", query="SELECT v.name FROM Variantset v"),
	@NamedQuery(name="Variantset.findById", query="SELECT v FROM Variantset v where v.variantsetId= :variantsetId"),
	@NamedQuery(name="Variantset.findByName", query="SELECT v FROM Variantset v  where v.name= :name")
})

public class Variantset implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="variantset_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer variantsetId;

	private String description;

	private String name;

	//bi-directional many-to-one association to Platform
	@OneToMany(mappedBy="variantset")
	private List<Platform> platforms;

	//bi-directional many-to-one association to SnpFeature
	@OneToMany(mappedBy="variantset")
	private List<SnpFeature> snpFeatures;

	//bi-directional many-to-one association to VariantVariantset
	@OneToMany(mappedBy="variantset")
	private List<VariantVariantset> variantVariantsets;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="variant_type_id")
	private Cvterm cvterm;

	public Variantset() {
	}

	public Integer getVariantsetId() {
		return this.variantsetId;
	}

	public void setVariantsetId(Integer variantsetId) {
		this.variantsetId = variantsetId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Platform> getPlatforms() {
		return this.platforms;
	}

	public void setPlatforms(List<Platform> platforms) {
		this.platforms = platforms;
	}

	public Platform addPlatform(Platform platform) {
		getPlatforms().add(platform);
		platform.setVariantset(this);

		return platform;
	}

	public Platform removePlatform(Platform platform) {
		getPlatforms().remove(platform);
		platform.setVariantset(null);

		return platform;
	}

	public List<SnpFeature> getSnpFeatures() {
		return this.snpFeatures;
	}

	public void setSnpFeatures(List<SnpFeature> snpFeatures) {
		this.snpFeatures = snpFeatures;
	}

	public SnpFeature addSnpFeature(SnpFeature snpFeature) {
		getSnpFeatures().add(snpFeature);
		snpFeature.setVariantset(this);

		return snpFeature;
	}

	public SnpFeature removeSnpFeature(SnpFeature snpFeature) {
		getSnpFeatures().remove(snpFeature);
		snpFeature.setVariantset(null);

		return snpFeature;
	}

	public List<VariantVariantset> getVariantVariantsets() {
		return this.variantVariantsets;
	}

	public void setVariantVariantsets(List<VariantVariantset> variantVariantsets) {
		this.variantVariantsets = variantVariantsets;
	}

	public VariantVariantset addVariantVariantset(VariantVariantset variantVariantset) {
		getVariantVariantsets().add(variantVariantset);
		variantVariantset.setVariantset(this);

		return variantVariantset;
	}

	public VariantVariantset removeVariantVariantset(VariantVariantset variantVariantset) {
		getVariantVariantsets().remove(variantVariantset);
		variantVariantset.setVariantset(null);

		return variantVariantset;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stockcollection database table.
 * 
 */
@Entity
@NamedQuery(name="Stockcollection.findAll", query="SELECT s FROM Stockcollection s")
public class Stockcollection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stockcollection_id")
	private Integer stockcollectionId;

	@Column(name="contact_id")
	private Integer contactId;

	private String name;

	private String uniquename;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to StockcollectionStock
	@OneToMany(mappedBy="stockcollection")
	private List<StockcollectionStock> stockcollectionStocks;

	//bi-directional many-to-one association to Stockcollectionprop
	@OneToMany(mappedBy="stockcollection")
	private List<Stockcollectionprop> stockcollectionprops;

	public Stockcollection() {
	}

	public Integer getStockcollectionId() {
		return this.stockcollectionId;
	}

	public void setStockcollectionId(Integer stockcollectionId) {
		this.stockcollectionId = stockcollectionId;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUniquename() {
		return this.uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public List<StockcollectionStock> getStockcollectionStocks() {
		return this.stockcollectionStocks;
	}

	public void setStockcollectionStocks(List<StockcollectionStock> stockcollectionStocks) {
		this.stockcollectionStocks = stockcollectionStocks;
	}

	public StockcollectionStock addStockcollectionStock(StockcollectionStock stockcollectionStock) {
		getStockcollectionStocks().add(stockcollectionStock);
		stockcollectionStock.setStockcollection(this);

		return stockcollectionStock;
	}

	public StockcollectionStock removeStockcollectionStock(StockcollectionStock stockcollectionStock) {
		getStockcollectionStocks().remove(stockcollectionStock);
		stockcollectionStock.setStockcollection(null);

		return stockcollectionStock;
	}

	public List<Stockcollectionprop> getStockcollectionprops() {
		return this.stockcollectionprops;
	}

	public void setStockcollectionprops(List<Stockcollectionprop> stockcollectionprops) {
		this.stockcollectionprops = stockcollectionprops;
	}

	public Stockcollectionprop addStockcollectionprop(Stockcollectionprop stockcollectionprop) {
		getStockcollectionprops().add(stockcollectionprop);
		stockcollectionprop.setStockcollection(this);

		return stockcollectionprop;
	}

	public Stockcollectionprop removeStockcollectionprop(Stockcollectionprop stockcollectionprop) {
		getStockcollectionprops().remove(stockcollectionprop);
		stockcollectionprop.setStockcollection(null);

		return stockcollectionprop;
	}

}
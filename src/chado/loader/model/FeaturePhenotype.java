package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feature_phenotype database table.
 * 
 */
@Entity
@Table(name="feature_phenotype")
@NamedQuery(name="FeaturePhenotype.findAll", query="SELECT f FROM FeaturePhenotype f")
public class FeaturePhenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="feature_phenotype_id")
	private Integer featurePhenotypeId;

	@Column(name="phenotype_id")
	private Integer phenotypeId;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	public FeaturePhenotype() {
	}

	public Integer getFeaturePhenotypeId() {
		return this.featurePhenotypeId;
	}

	public void setFeaturePhenotypeId(Integer featurePhenotypeId) {
		this.featurePhenotypeId = featurePhenotypeId;
	}

	public Integer getPhenotypeId() {
		return this.phenotypeId;
	}

	public void setPhenotypeId(Integer phenotypeId) {
		this.phenotypeId = phenotypeId;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pub_dbxref database table.
 * 
 */
@Entity
@Table(name="pub_dbxref")
@NamedQuery(name="PubDbxref.findAll", query="SELECT p FROM PubDbxref p")
public class PubDbxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pub_dbxref_id")
	private Integer pubDbxrefId;

	@Column(name="is_current")
	private Boolean isCurrent;

	//bi-directional many-to-one association to Dbxref
	@ManyToOne
	@JoinColumn(name="dbxref_id")
	private Dbxref dbxref;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public PubDbxref() {
	}

	public Integer getPubDbxrefId() {
		return this.pubDbxrefId;
	}

	public void setPubDbxrefId(Integer pubDbxrefId) {
		this.pubDbxrefId = pubDbxrefId;
	}

	public Boolean getIsCurrent() {
		return this.isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Dbxref getDbxref() {
		return this.dbxref;
	}

	public void setDbxref(Dbxref dbxref) {
		this.dbxref = dbxref;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
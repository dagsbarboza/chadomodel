package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the featureprop database table.
 * 
 */
@Entity
@NamedQuery(name="Featureprop.findAll", query="SELECT f FROM Featureprop f")
public class Featureprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="featureprop_id")
	private Integer featurepropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to Feature
	@ManyToOne
	@JoinColumn(name="feature_id")
	private Feature feature;

	//bi-directional many-to-one association to FeaturepropPub
	@OneToMany(mappedBy="featureprop")
	private List<FeaturepropPub> featurepropPubs;

	public Featureprop() {
	}

	public Integer getFeaturepropId() {
		return this.featurepropId;
	}

	public void setFeaturepropId(Integer featurepropId) {
		this.featurepropId = featurepropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public Feature getFeature() {
		return this.feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public List<FeaturepropPub> getFeaturepropPubs() {
		return this.featurepropPubs;
	}

	public void setFeaturepropPubs(List<FeaturepropPub> featurepropPubs) {
		this.featurepropPubs = featurepropPubs;
	}

	public FeaturepropPub addFeaturepropPub(FeaturepropPub featurepropPub) {
		getFeaturepropPubs().add(featurepropPub);
		featurepropPub.setFeatureprop(this);

		return featurepropPub;
	}

	public FeaturepropPub removeFeaturepropPub(FeaturepropPub featurepropPub) {
		getFeaturepropPubs().remove(featurepropPub);
		featurepropPub.setFeatureprop(null);

		return featurepropPub;
	}

}
package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the featureloc_pub database table.
 * 
 */
@Entity
@Table(name="featureloc_pub")
@NamedQuery(name="FeaturelocPub.findAll", query="SELECT f FROM FeaturelocPub f")
public class FeaturelocPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="featureloc_pub_id")
	private Integer featurelocPubId;

	//bi-directional many-to-one association to Featureloc
	@ManyToOne
	@JoinColumn(name="featureloc_id")
	private Featureloc featureloc;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	public FeaturelocPub() {
	}

	public Integer getFeaturelocPubId() {
		return this.featurelocPubId;
	}

	public void setFeaturelocPubId(Integer featurelocPubId) {
		this.featurelocPubId = featurelocPubId;
	}

	public Featureloc getFeatureloc() {
		return this.featureloc;
	}

	public void setFeatureloc(Featureloc featureloc) {
		this.featureloc = featureloc;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

}
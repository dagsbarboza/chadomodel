package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stock_relationship_pub database table.
 * 
 */
@Entity
@Table(name="stock_relationship_pub")
@NamedQuery(name="StockRelationshipPub.findAll", query="SELECT s FROM StockRelationshipPub s")
public class StockRelationshipPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_relationship_pub_id")
	private Integer stockRelationshipPubId;

	//bi-directional many-to-one association to Pub
	@ManyToOne
	@JoinColumn(name="pub_id")
	private Pub pub;

	//bi-directional many-to-one association to StockRelationship
	@ManyToOne
	@JoinColumn(name="stock_relationship_id")
	private StockRelationship stockRelationship;

	public StockRelationshipPub() {
	}

	public Integer getStockRelationshipPubId() {
		return this.stockRelationshipPubId;
	}

	public void setStockRelationshipPubId(Integer stockRelationshipPubId) {
		this.stockRelationshipPubId = stockRelationshipPubId;
	}

	public Pub getPub() {
		return this.pub;
	}

	public void setPub(Pub pub) {
		this.pub = pub;
	}

	public StockRelationship getStockRelationship() {
		return this.stockRelationship;
	}

	public void setStockRelationship(StockRelationship stockRelationship) {
		this.stockRelationship = stockRelationship;
	}

}
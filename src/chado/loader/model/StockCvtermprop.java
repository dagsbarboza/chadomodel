package chado.loader.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stock_cvtermprop database table.
 * 
 */
@Entity
@Table(name="stock_cvtermprop")
@NamedQuery(name="StockCvtermprop.findAll", query="SELECT s FROM StockCvtermprop s")
public class StockCvtermprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="stock_cvtermprop_id")
	private Integer stockCvtermpropId;

	private Integer rank;

	private String value;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	//bi-directional many-to-one association to StockCvterm
	@ManyToOne
	@JoinColumn(name="stock_cvterm_id")
	private StockCvterm stockCvterm;

	public StockCvtermprop() {
	}

	public Integer getStockCvtermpropId() {
		return this.stockCvtermpropId;
	}

	public void setStockCvtermpropId(Integer stockCvtermpropId) {
		this.stockCvtermpropId = stockCvtermpropId;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

	public StockCvterm getStockCvterm() {
		return this.stockCvterm;
	}

	public void setStockCvterm(StockCvterm stockCvterm) {
		this.stockCvterm = stockCvterm;
	}

}
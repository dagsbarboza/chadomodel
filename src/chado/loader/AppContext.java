package chado.loader;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AppContext {
	private static String PERSISTENT_UNIT = "ChadoModel";
	private static EntityManager em;
	private static EntityManagerFactory current_emf;

	public static void createEntityManager() {
		if (current_emf != null) {
			current_emf.close();
			current_emf = null;
		}

		current_emf = Persistence.createEntityManagerFactory(PERSISTENT_UNIT);
		em = current_emf.createEntityManager();
	}

	public static EntityManager getEntityManager() {
		return em;
	}

	public static boolean createEntityManager(Map properties) {
		try {
			if (current_emf != null)
				if (current_emf.isOpen())
					current_emf.close();

			current_emf = null;
			em = null;

			current_emf = Persistence.createEntityManagerFactory(PERSISTENT_UNIT, properties);
			em = current_emf.createEntityManager();
		} catch (Exception e) {
			return false;
		}

		return true;

	}

	public static boolean testEntityManager(Map properties) {
		try {
			Persistence.createEntityManagerFactory(PERSISTENT_UNIT, properties).createEntityManager();
		} catch (Exception e) {
			return false;
		}

		return true;

	}

	public static void revertEntityManager() {
		em = current_emf.createEntityManager();
	}

}

package chado.loader;

import javax.persistence.EntityManagerFactory;

import chado.loader.model.Db;
import chado.loader.model.Dbxref;
import chado.loader.model.Feature;
import chado.loader.model.Organism;
import chado.loader.model.SnpFeatureloc;
import chado.loader.model.Stock;
import chado.loader.model.StockSample;
import chado.loader.model.Variantset;
import chado.loader.service.CvTermService;
import chado.loader.service.DbService;

public class GenotypeLoader {

	private static final String PERSISTENCE_UNIT_NAME = "ChadoModel";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {

		AppContext.createEntityManager();
		// OrganismService organismService = new OrganismService();
		DbService dbService = new DbService();
		CvTermService cvTermService = new CvTermService();

		// List<Cvterm> result = cvTermService.findCvtermyName("whole plant");

		Db db = dbService.getDbById(2);
		// DbRepositoryImpl test = new DbRepositoryImpl();
		//System.out.println("NAME: " + db.getName());

		/*-- load organism (or use chado loader bin/gmod_add_organism.pl)
		*insert into organism(genus,species,common_name)
		*values('Sorghum','bicolor','sorghum bicolor');
		*/

		// System.out.println(db.getName());

		Organism organism = new Organism();
		organism.setGenus("Sorghum");
		organism.setSpecies("bicolor");
		organism.setCommonName("sorghum bicolor");

		// organismService.insertRecord(organism);

		/*
		 * -- load sample accessions from raw_files
		 * 
		 * insert into dbxref(db_id, accession, version) select 504 db_id, name
		 * accession, '1' as version from raw_files.samples;
		 * 
		 */

		Dbxref dbxref = new Dbxref();
		dbxref.setAccession("");
		dbxref.setVersion("1");

		/*-- if varieties used in accessions are not yet defined, load into stock
		--select * from cvterm where name='whole plant';  --     cvterm_id=42248
		--select * from organism;
		insert into stock(organism_id, name, uniquename, type_id, is_obsolete) 
		select 1  sorghum bicolor  organsim_id , name, name, 42248  whole plant  type_id, false is_obsolete 
		from raw_files.samples;*/

		Stock stock = new Stock();
		stock.setOrganism(organism);
		stock.setName("");
		stock.setUniquename("");
		// stock.setCvterm(result.get(0));
		stock.setIsObsolete(false);

		/*-- populate stock_sample
		-- hdf5_index is the index number (0-based) of the sample in the hdf5 snp matrix 
		-- (not requuired if genotype is loaded into snp_genotype) 
		insert into stock_sample(stock_id, dbxref_id, hdf5_index)
		select s.stock_id, dx.dbxref_id, row_number() over(order by s.stock_id)-1 hdf5_index
		from dbxref dx, stock s
		where s.name=dx.accession and dx.db_id=504
		order by s.stock_id;*
		*/

		StockSample ssample = new StockSample();
		ssample.setStock(stock);
		ssample.setDbxref(dbxref);
		ssample.setHdf5Index(0);

		/*-- define variant set
		--select * from cvterm where name='SNP'; -- 855
		insert into variantset(name,description, variant_type_id) values('refset','S.bicolor reference set',855); --1*/

		Variantset variantSet = new Variantset();
		variantSet.setName("");
		variantSet.setDescription("");
		// variantSet.setVariantsetId(result.get(0).getCvtermId());

		/*-- define snp_feature_ids for the snps
		-- select count(*) from raw_files.snp_position; --516734
		-- offset snp_feature_id if snp_feature has entries already
		insert into snp_feature
		select generate_series(1,516734) snp_feature_id, 1 variantset_id;*/

		/*-- load snp_featureloc
		--select ct,cvterm_id from cvterm ct, cv where ct.name='chromosome' and ct.cv_id=cv.cv_id and cv.name='sequence'; --502
		--select organism_id from organsim where common_name='sorghum bicolor' -- 1 
		insert into snp_featureloc(snp_feature_id, organism_id,srcfeature_id,position,refcall)
		select row_number() over(order by f.feature_id, sp.position) snp_featureid, 1 organism_id,f.feature_id srcfeature_id, sp.position-1 as position, sp.ref_call refcall 
		from raw_files.snp_position sp, feature f
		where f.name=sp.contig and f.organism_id=1  sorghum bicolor organism_id  
		and f.type_id=502  type chromosome   
		order by f.feature_id, sp.position; --516734*/

		SnpFeatureloc snpFeatureLoc = new SnpFeatureloc();
		snpFeatureLoc.setOrganism(organism);
		snpFeatureLoc.setFeature(new Feature());

		// System.out.println("ID: " + d.getDbId());
	}
}
